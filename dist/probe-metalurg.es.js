import { inject, openBlock, createElementBlock, createElementVNode, normalizeStyle, renderSlot, ref, onMounted, unref, toDisplayString, createVNode, Transition, withCtx, createBlock, createCommentVNode, normalizeClass, createStaticVNode, createTextVNode, markRaw, resolveDynamicComponent, isRef } from "vue";
const _hoisted_1$a = /* @__PURE__ */ createElementVNode("div", { class: "modal-dialog-scrim fixed left-0 top-0 h-full w-full" }, null, -1);
const _sfc_main$b = {
  __name: "ModalDialog",
  props: {
    width: {
      required: true,
      type: Number
    }
  },
  setup(__props) {
    inject("translate");
    inject("media");
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("div", null, [
        _hoisted_1$a,
        createElementVNode("div", {
          style: normalizeStyle({ width: __props.width + "px" }),
          class: "modal-dialog absolute left-1/2 top-1/2 flex -translate-x-1/2 -translate-y-1/2 space-x-1 p-5"
        }, [
          renderSlot(_ctx.$slots, "default")
        ], 4)
      ]);
    };
  }
};
function isObject$1(obj) {
  return obj !== null && typeof obj === "object" && "constructor" in obj && obj.constructor === Object;
}
function extend$1(target = {}, src = {}) {
  Object.keys(src).forEach((key) => {
    if (typeof target[key] === "undefined")
      target[key] = src[key];
    else if (isObject$1(src[key]) && isObject$1(target[key]) && Object.keys(src[key]).length > 0) {
      extend$1(target[key], src[key]);
    }
  });
}
const ssrDocument = {
  body: {},
  addEventListener() {
  },
  removeEventListener() {
  },
  activeElement: {
    blur() {
    },
    nodeName: ""
  },
  querySelector() {
    return null;
  },
  querySelectorAll() {
    return [];
  },
  getElementById() {
    return null;
  },
  createEvent() {
    return {
      initEvent() {
      }
    };
  },
  createElement() {
    return {
      children: [],
      childNodes: [],
      style: {},
      setAttribute() {
      },
      getElementsByTagName() {
        return [];
      }
    };
  },
  createElementNS() {
    return {};
  },
  importNode() {
    return null;
  },
  location: {
    hash: "",
    host: "",
    hostname: "",
    href: "",
    origin: "",
    pathname: "",
    protocol: "",
    search: ""
  }
};
function getDocument() {
  const doc = typeof document !== "undefined" ? document : {};
  extend$1(doc, ssrDocument);
  return doc;
}
const ssrWindow = {
  document: ssrDocument,
  navigator: {
    userAgent: ""
  },
  location: {
    hash: "",
    host: "",
    hostname: "",
    href: "",
    origin: "",
    pathname: "",
    protocol: "",
    search: ""
  },
  history: {
    replaceState() {
    },
    pushState() {
    },
    go() {
    },
    back() {
    }
  },
  CustomEvent: function CustomEvent() {
    return this;
  },
  addEventListener() {
  },
  removeEventListener() {
  },
  getComputedStyle() {
    return {
      getPropertyValue() {
        return "";
      }
    };
  },
  Image() {
  },
  Date() {
  },
  screen: {},
  setTimeout() {
  },
  clearTimeout() {
  },
  matchMedia() {
    return {};
  },
  requestAnimationFrame(callback) {
    if (typeof setTimeout === "undefined") {
      callback();
      return null;
    }
    return setTimeout(callback, 0);
  },
  cancelAnimationFrame(id) {
    if (typeof setTimeout === "undefined") {
      return;
    }
    clearTimeout(id);
  }
};
function getWindow() {
  const win = typeof window !== "undefined" ? window : {};
  extend$1(win, ssrWindow);
  return win;
}
function makeReactive(obj) {
  const proto = obj.__proto__;
  Object.defineProperty(obj, "__proto__", {
    get() {
      return proto;
    },
    set(value) {
      proto.__proto__ = value;
    }
  });
}
class Dom7 extends Array {
  constructor(items) {
    if (typeof items === "number") {
      super(items);
    } else {
      super(...items || []);
      makeReactive(this);
    }
  }
}
function arrayFlat(arr = []) {
  const res = [];
  arr.forEach((el) => {
    if (Array.isArray(el)) {
      res.push(...arrayFlat(el));
    } else {
      res.push(el);
    }
  });
  return res;
}
function arrayFilter(arr, callback) {
  return Array.prototype.filter.call(arr, callback);
}
function arrayUnique(arr) {
  const uniqueArray = [];
  for (let i = 0; i < arr.length; i += 1) {
    if (uniqueArray.indexOf(arr[i]) === -1)
      uniqueArray.push(arr[i]);
  }
  return uniqueArray;
}
function qsa(selector, context) {
  if (typeof selector !== "string") {
    return [selector];
  }
  const a = [];
  const res = context.querySelectorAll(selector);
  for (let i = 0; i < res.length; i += 1) {
    a.push(res[i]);
  }
  return a;
}
function $(selector, context) {
  const window2 = getWindow();
  const document2 = getDocument();
  let arr = [];
  if (!context && selector instanceof Dom7) {
    return selector;
  }
  if (!selector) {
    return new Dom7(arr);
  }
  if (typeof selector === "string") {
    const html2 = selector.trim();
    if (html2.indexOf("<") >= 0 && html2.indexOf(">") >= 0) {
      let toCreate = "div";
      if (html2.indexOf("<li") === 0)
        toCreate = "ul";
      if (html2.indexOf("<tr") === 0)
        toCreate = "tbody";
      if (html2.indexOf("<td") === 0 || html2.indexOf("<th") === 0)
        toCreate = "tr";
      if (html2.indexOf("<tbody") === 0)
        toCreate = "table";
      if (html2.indexOf("<option") === 0)
        toCreate = "select";
      const tempParent = document2.createElement(toCreate);
      tempParent.innerHTML = html2;
      for (let i = 0; i < tempParent.childNodes.length; i += 1) {
        arr.push(tempParent.childNodes[i]);
      }
    } else {
      arr = qsa(selector.trim(), context || document2);
    }
  } else if (selector.nodeType || selector === window2 || selector === document2) {
    arr.push(selector);
  } else if (Array.isArray(selector)) {
    if (selector instanceof Dom7)
      return selector;
    arr = selector;
  }
  return new Dom7(arrayUnique(arr));
}
$.fn = Dom7.prototype;
function addClass(...classes2) {
  const classNames = arrayFlat(classes2.map((c) => c.split(" ")));
  this.forEach((el) => {
    el.classList.add(...classNames);
  });
  return this;
}
function removeClass(...classes2) {
  const classNames = arrayFlat(classes2.map((c) => c.split(" ")));
  this.forEach((el) => {
    el.classList.remove(...classNames);
  });
  return this;
}
function toggleClass(...classes2) {
  const classNames = arrayFlat(classes2.map((c) => c.split(" ")));
  this.forEach((el) => {
    classNames.forEach((className) => {
      el.classList.toggle(className);
    });
  });
}
function hasClass(...classes2) {
  const classNames = arrayFlat(classes2.map((c) => c.split(" ")));
  return arrayFilter(this, (el) => {
    return classNames.filter((className) => el.classList.contains(className)).length > 0;
  }).length > 0;
}
function attr(attrs, value) {
  if (arguments.length === 1 && typeof attrs === "string") {
    if (this[0])
      return this[0].getAttribute(attrs);
    return void 0;
  }
  for (let i = 0; i < this.length; i += 1) {
    if (arguments.length === 2) {
      this[i].setAttribute(attrs, value);
    } else {
      for (const attrName in attrs) {
        this[i][attrName] = attrs[attrName];
        this[i].setAttribute(attrName, attrs[attrName]);
      }
    }
  }
  return this;
}
function removeAttr(attr2) {
  for (let i = 0; i < this.length; i += 1) {
    this[i].removeAttribute(attr2);
  }
  return this;
}
function transform(transform2) {
  for (let i = 0; i < this.length; i += 1) {
    this[i].style.transform = transform2;
  }
  return this;
}
function transition$1(duration) {
  for (let i = 0; i < this.length; i += 1) {
    this[i].style.transitionDuration = typeof duration !== "string" ? `${duration}ms` : duration;
  }
  return this;
}
function on(...args) {
  let [eventType, targetSelector, listener, capture] = args;
  if (typeof args[1] === "function") {
    [eventType, listener, capture] = args;
    targetSelector = void 0;
  }
  if (!capture)
    capture = false;
  function handleLiveEvent(e) {
    const target = e.target;
    if (!target)
      return;
    const eventData = e.target.dom7EventData || [];
    if (eventData.indexOf(e) < 0) {
      eventData.unshift(e);
    }
    if ($(target).is(targetSelector))
      listener.apply(target, eventData);
    else {
      const parents2 = $(target).parents();
      for (let k = 0; k < parents2.length; k += 1) {
        if ($(parents2[k]).is(targetSelector))
          listener.apply(parents2[k], eventData);
      }
    }
  }
  function handleEvent(e) {
    const eventData = e && e.target ? e.target.dom7EventData || [] : [];
    if (eventData.indexOf(e) < 0) {
      eventData.unshift(e);
    }
    listener.apply(this, eventData);
  }
  const events2 = eventType.split(" ");
  let j;
  for (let i = 0; i < this.length; i += 1) {
    const el = this[i];
    if (!targetSelector) {
      for (j = 0; j < events2.length; j += 1) {
        const event = events2[j];
        if (!el.dom7Listeners)
          el.dom7Listeners = {};
        if (!el.dom7Listeners[event])
          el.dom7Listeners[event] = [];
        el.dom7Listeners[event].push({
          listener,
          proxyListener: handleEvent
        });
        el.addEventListener(event, handleEvent, capture);
      }
    } else {
      for (j = 0; j < events2.length; j += 1) {
        const event = events2[j];
        if (!el.dom7LiveListeners)
          el.dom7LiveListeners = {};
        if (!el.dom7LiveListeners[event])
          el.dom7LiveListeners[event] = [];
        el.dom7LiveListeners[event].push({
          listener,
          proxyListener: handleLiveEvent
        });
        el.addEventListener(event, handleLiveEvent, capture);
      }
    }
  }
  return this;
}
function off(...args) {
  let [eventType, targetSelector, listener, capture] = args;
  if (typeof args[1] === "function") {
    [eventType, listener, capture] = args;
    targetSelector = void 0;
  }
  if (!capture)
    capture = false;
  const events2 = eventType.split(" ");
  for (let i = 0; i < events2.length; i += 1) {
    const event = events2[i];
    for (let j = 0; j < this.length; j += 1) {
      const el = this[j];
      let handlers;
      if (!targetSelector && el.dom7Listeners) {
        handlers = el.dom7Listeners[event];
      } else if (targetSelector && el.dom7LiveListeners) {
        handlers = el.dom7LiveListeners[event];
      }
      if (handlers && handlers.length) {
        for (let k = handlers.length - 1; k >= 0; k -= 1) {
          const handler = handlers[k];
          if (listener && handler.listener === listener) {
            el.removeEventListener(event, handler.proxyListener, capture);
            handlers.splice(k, 1);
          } else if (listener && handler.listener && handler.listener.dom7proxy && handler.listener.dom7proxy === listener) {
            el.removeEventListener(event, handler.proxyListener, capture);
            handlers.splice(k, 1);
          } else if (!listener) {
            el.removeEventListener(event, handler.proxyListener, capture);
            handlers.splice(k, 1);
          }
        }
      }
    }
  }
  return this;
}
function trigger(...args) {
  const window2 = getWindow();
  const events2 = args[0].split(" ");
  const eventData = args[1];
  for (let i = 0; i < events2.length; i += 1) {
    const event = events2[i];
    for (let j = 0; j < this.length; j += 1) {
      const el = this[j];
      if (window2.CustomEvent) {
        const evt = new window2.CustomEvent(event, {
          detail: eventData,
          bubbles: true,
          cancelable: true
        });
        el.dom7EventData = args.filter((data, dataIndex) => dataIndex > 0);
        el.dispatchEvent(evt);
        el.dom7EventData = [];
        delete el.dom7EventData;
      }
    }
  }
  return this;
}
function transitionEnd$1(callback) {
  const dom = this;
  function fireCallBack(e) {
    if (e.target !== this)
      return;
    callback.call(this, e);
    dom.off("transitionend", fireCallBack);
  }
  if (callback) {
    dom.on("transitionend", fireCallBack);
  }
  return this;
}
function outerWidth(includeMargins) {
  if (this.length > 0) {
    if (includeMargins) {
      const styles2 = this.styles();
      return this[0].offsetWidth + parseFloat(styles2.getPropertyValue("margin-right")) + parseFloat(styles2.getPropertyValue("margin-left"));
    }
    return this[0].offsetWidth;
  }
  return null;
}
function outerHeight(includeMargins) {
  if (this.length > 0) {
    if (includeMargins) {
      const styles2 = this.styles();
      return this[0].offsetHeight + parseFloat(styles2.getPropertyValue("margin-top")) + parseFloat(styles2.getPropertyValue("margin-bottom"));
    }
    return this[0].offsetHeight;
  }
  return null;
}
function offset() {
  if (this.length > 0) {
    const window2 = getWindow();
    const document2 = getDocument();
    const el = this[0];
    const box = el.getBoundingClientRect();
    const body = document2.body;
    const clientTop = el.clientTop || body.clientTop || 0;
    const clientLeft = el.clientLeft || body.clientLeft || 0;
    const scrollTop = el === window2 ? window2.scrollY : el.scrollTop;
    const scrollLeft = el === window2 ? window2.scrollX : el.scrollLeft;
    return {
      top: box.top + scrollTop - clientTop,
      left: box.left + scrollLeft - clientLeft
    };
  }
  return null;
}
function styles() {
  const window2 = getWindow();
  if (this[0])
    return window2.getComputedStyle(this[0], null);
  return {};
}
function css(props, value) {
  const window2 = getWindow();
  let i;
  if (arguments.length === 1) {
    if (typeof props === "string") {
      if (this[0])
        return window2.getComputedStyle(this[0], null).getPropertyValue(props);
    } else {
      for (i = 0; i < this.length; i += 1) {
        for (const prop in props) {
          this[i].style[prop] = props[prop];
        }
      }
      return this;
    }
  }
  if (arguments.length === 2 && typeof props === "string") {
    for (i = 0; i < this.length; i += 1) {
      this[i].style[props] = value;
    }
    return this;
  }
  return this;
}
function each(callback) {
  if (!callback)
    return this;
  this.forEach((el, index2) => {
    callback.apply(el, [el, index2]);
  });
  return this;
}
function filter(callback) {
  const result = arrayFilter(this, callback);
  return $(result);
}
function html(html2) {
  if (typeof html2 === "undefined") {
    return this[0] ? this[0].innerHTML : null;
  }
  for (let i = 0; i < this.length; i += 1) {
    this[i].innerHTML = html2;
  }
  return this;
}
function text(text2) {
  if (typeof text2 === "undefined") {
    return this[0] ? this[0].textContent.trim() : null;
  }
  for (let i = 0; i < this.length; i += 1) {
    this[i].textContent = text2;
  }
  return this;
}
function is(selector) {
  const window2 = getWindow();
  const document2 = getDocument();
  const el = this[0];
  let compareWith;
  let i;
  if (!el || typeof selector === "undefined")
    return false;
  if (typeof selector === "string") {
    if (el.matches)
      return el.matches(selector);
    if (el.webkitMatchesSelector)
      return el.webkitMatchesSelector(selector);
    if (el.msMatchesSelector)
      return el.msMatchesSelector(selector);
    compareWith = $(selector);
    for (i = 0; i < compareWith.length; i += 1) {
      if (compareWith[i] === el)
        return true;
    }
    return false;
  }
  if (selector === document2) {
    return el === document2;
  }
  if (selector === window2) {
    return el === window2;
  }
  if (selector.nodeType || selector instanceof Dom7) {
    compareWith = selector.nodeType ? [selector] : selector;
    for (i = 0; i < compareWith.length; i += 1) {
      if (compareWith[i] === el)
        return true;
    }
    return false;
  }
  return false;
}
function index() {
  let child = this[0];
  let i;
  if (child) {
    i = 0;
    while ((child = child.previousSibling) !== null) {
      if (child.nodeType === 1)
        i += 1;
    }
    return i;
  }
  return void 0;
}
function eq(index2) {
  if (typeof index2 === "undefined")
    return this;
  const length = this.length;
  if (index2 > length - 1) {
    return $([]);
  }
  if (index2 < 0) {
    const returnIndex = length + index2;
    if (returnIndex < 0)
      return $([]);
    return $([this[returnIndex]]);
  }
  return $([this[index2]]);
}
function append(...els) {
  let newChild;
  const document2 = getDocument();
  for (let k = 0; k < els.length; k += 1) {
    newChild = els[k];
    for (let i = 0; i < this.length; i += 1) {
      if (typeof newChild === "string") {
        const tempDiv = document2.createElement("div");
        tempDiv.innerHTML = newChild;
        while (tempDiv.firstChild) {
          this[i].appendChild(tempDiv.firstChild);
        }
      } else if (newChild instanceof Dom7) {
        for (let j = 0; j < newChild.length; j += 1) {
          this[i].appendChild(newChild[j]);
        }
      } else {
        this[i].appendChild(newChild);
      }
    }
  }
  return this;
}
function prepend(newChild) {
  const document2 = getDocument();
  let i;
  let j;
  for (i = 0; i < this.length; i += 1) {
    if (typeof newChild === "string") {
      const tempDiv = document2.createElement("div");
      tempDiv.innerHTML = newChild;
      for (j = tempDiv.childNodes.length - 1; j >= 0; j -= 1) {
        this[i].insertBefore(tempDiv.childNodes[j], this[i].childNodes[0]);
      }
    } else if (newChild instanceof Dom7) {
      for (j = 0; j < newChild.length; j += 1) {
        this[i].insertBefore(newChild[j], this[i].childNodes[0]);
      }
    } else {
      this[i].insertBefore(newChild, this[i].childNodes[0]);
    }
  }
  return this;
}
function next(selector) {
  if (this.length > 0) {
    if (selector) {
      if (this[0].nextElementSibling && $(this[0].nextElementSibling).is(selector)) {
        return $([this[0].nextElementSibling]);
      }
      return $([]);
    }
    if (this[0].nextElementSibling)
      return $([this[0].nextElementSibling]);
    return $([]);
  }
  return $([]);
}
function nextAll(selector) {
  const nextEls = [];
  let el = this[0];
  if (!el)
    return $([]);
  while (el.nextElementSibling) {
    const next2 = el.nextElementSibling;
    if (selector) {
      if ($(next2).is(selector))
        nextEls.push(next2);
    } else
      nextEls.push(next2);
    el = next2;
  }
  return $(nextEls);
}
function prev(selector) {
  if (this.length > 0) {
    const el = this[0];
    if (selector) {
      if (el.previousElementSibling && $(el.previousElementSibling).is(selector)) {
        return $([el.previousElementSibling]);
      }
      return $([]);
    }
    if (el.previousElementSibling)
      return $([el.previousElementSibling]);
    return $([]);
  }
  return $([]);
}
function prevAll(selector) {
  const prevEls = [];
  let el = this[0];
  if (!el)
    return $([]);
  while (el.previousElementSibling) {
    const prev2 = el.previousElementSibling;
    if (selector) {
      if ($(prev2).is(selector))
        prevEls.push(prev2);
    } else
      prevEls.push(prev2);
    el = prev2;
  }
  return $(prevEls);
}
function parent(selector) {
  const parents2 = [];
  for (let i = 0; i < this.length; i += 1) {
    if (this[i].parentNode !== null) {
      if (selector) {
        if ($(this[i].parentNode).is(selector))
          parents2.push(this[i].parentNode);
      } else {
        parents2.push(this[i].parentNode);
      }
    }
  }
  return $(parents2);
}
function parents(selector) {
  const parents2 = [];
  for (let i = 0; i < this.length; i += 1) {
    let parent2 = this[i].parentNode;
    while (parent2) {
      if (selector) {
        if ($(parent2).is(selector))
          parents2.push(parent2);
      } else {
        parents2.push(parent2);
      }
      parent2 = parent2.parentNode;
    }
  }
  return $(parents2);
}
function closest(selector) {
  let closest2 = this;
  if (typeof selector === "undefined") {
    return $([]);
  }
  if (!closest2.is(selector)) {
    closest2 = closest2.parents(selector).eq(0);
  }
  return closest2;
}
function find(selector) {
  const foundElements = [];
  for (let i = 0; i < this.length; i += 1) {
    const found = this[i].querySelectorAll(selector);
    for (let j = 0; j < found.length; j += 1) {
      foundElements.push(found[j]);
    }
  }
  return $(foundElements);
}
function children(selector) {
  const children2 = [];
  for (let i = 0; i < this.length; i += 1) {
    const childNodes = this[i].children;
    for (let j = 0; j < childNodes.length; j += 1) {
      if (!selector || $(childNodes[j]).is(selector)) {
        children2.push(childNodes[j]);
      }
    }
  }
  return $(children2);
}
function remove() {
  for (let i = 0; i < this.length; i += 1) {
    if (this[i].parentNode)
      this[i].parentNode.removeChild(this[i]);
  }
  return this;
}
const Methods = {
  addClass,
  removeClass,
  hasClass,
  toggleClass,
  attr,
  removeAttr,
  transform,
  transition: transition$1,
  on,
  off,
  trigger,
  transitionEnd: transitionEnd$1,
  outerWidth,
  outerHeight,
  styles,
  offset,
  css,
  each,
  html,
  text,
  is,
  index,
  eq,
  append,
  prepend,
  next,
  nextAll,
  prev,
  prevAll,
  parent,
  parents,
  closest,
  find,
  children,
  filter,
  remove
};
Object.keys(Methods).forEach((methodName) => {
  Object.defineProperty($.fn, methodName, {
    value: Methods[methodName],
    writable: true
  });
});
function deleteProps(obj) {
  const object = obj;
  Object.keys(object).forEach((key) => {
    try {
      object[key] = null;
    } catch (e) {
    }
    try {
      delete object[key];
    } catch (e) {
    }
  });
}
function nextTick(callback, delay) {
  if (delay === void 0) {
    delay = 0;
  }
  return setTimeout(callback, delay);
}
function now() {
  return Date.now();
}
function getComputedStyle$1(el) {
  const window2 = getWindow();
  let style;
  if (window2.getComputedStyle) {
    style = window2.getComputedStyle(el, null);
  }
  if (!style && el.currentStyle) {
    style = el.currentStyle;
  }
  if (!style) {
    style = el.style;
  }
  return style;
}
function getTranslate(el, axis) {
  if (axis === void 0) {
    axis = "x";
  }
  const window2 = getWindow();
  let matrix;
  let curTransform;
  let transformMatrix;
  const curStyle = getComputedStyle$1(el);
  if (window2.WebKitCSSMatrix) {
    curTransform = curStyle.transform || curStyle.webkitTransform;
    if (curTransform.split(",").length > 6) {
      curTransform = curTransform.split(", ").map((a) => a.replace(",", ".")).join(", ");
    }
    transformMatrix = new window2.WebKitCSSMatrix(curTransform === "none" ? "" : curTransform);
  } else {
    transformMatrix = curStyle.MozTransform || curStyle.OTransform || curStyle.MsTransform || curStyle.msTransform || curStyle.transform || curStyle.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,");
    matrix = transformMatrix.toString().split(",");
  }
  if (axis === "x") {
    if (window2.WebKitCSSMatrix)
      curTransform = transformMatrix.m41;
    else if (matrix.length === 16)
      curTransform = parseFloat(matrix[12]);
    else
      curTransform = parseFloat(matrix[4]);
  }
  if (axis === "y") {
    if (window2.WebKitCSSMatrix)
      curTransform = transformMatrix.m42;
    else if (matrix.length === 16)
      curTransform = parseFloat(matrix[13]);
    else
      curTransform = parseFloat(matrix[5]);
  }
  return curTransform || 0;
}
function isObject(o) {
  return typeof o === "object" && o !== null && o.constructor && Object.prototype.toString.call(o).slice(8, -1) === "Object";
}
function isNode(node) {
  if (typeof window !== "undefined" && typeof window.HTMLElement !== "undefined") {
    return node instanceof HTMLElement;
  }
  return node && (node.nodeType === 1 || node.nodeType === 11);
}
function extend() {
  const to = Object(arguments.length <= 0 ? void 0 : arguments[0]);
  const noExtend = ["__proto__", "constructor", "prototype"];
  for (let i = 1; i < arguments.length; i += 1) {
    const nextSource = i < 0 || arguments.length <= i ? void 0 : arguments[i];
    if (nextSource !== void 0 && nextSource !== null && !isNode(nextSource)) {
      const keysArray = Object.keys(Object(nextSource)).filter((key) => noExtend.indexOf(key) < 0);
      for (let nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex += 1) {
        const nextKey = keysArray[nextIndex];
        const desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
        if (desc !== void 0 && desc.enumerable) {
          if (isObject(to[nextKey]) && isObject(nextSource[nextKey])) {
            if (nextSource[nextKey].__swiper__) {
              to[nextKey] = nextSource[nextKey];
            } else {
              extend(to[nextKey], nextSource[nextKey]);
            }
          } else if (!isObject(to[nextKey]) && isObject(nextSource[nextKey])) {
            to[nextKey] = {};
            if (nextSource[nextKey].__swiper__) {
              to[nextKey] = nextSource[nextKey];
            } else {
              extend(to[nextKey], nextSource[nextKey]);
            }
          } else {
            to[nextKey] = nextSource[nextKey];
          }
        }
      }
    }
  }
  return to;
}
function setCSSProperty(el, varName, varValue) {
  el.style.setProperty(varName, varValue);
}
function animateCSSModeScroll(_ref) {
  let {
    swiper,
    targetPosition,
    side
  } = _ref;
  const window2 = getWindow();
  const startPosition = -swiper.translate;
  let startTime = null;
  let time;
  const duration = swiper.params.speed;
  swiper.wrapperEl.style.scrollSnapType = "none";
  window2.cancelAnimationFrame(swiper.cssModeFrameID);
  const dir = targetPosition > startPosition ? "next" : "prev";
  const isOutOfBound = (current, target) => {
    return dir === "next" && current >= target || dir === "prev" && current <= target;
  };
  const animate = () => {
    time = new Date().getTime();
    if (startTime === null) {
      startTime = time;
    }
    const progress = Math.max(Math.min((time - startTime) / duration, 1), 0);
    const easeProgress = 0.5 - Math.cos(progress * Math.PI) / 2;
    let currentPosition = startPosition + easeProgress * (targetPosition - startPosition);
    if (isOutOfBound(currentPosition, targetPosition)) {
      currentPosition = targetPosition;
    }
    swiper.wrapperEl.scrollTo({
      [side]: currentPosition
    });
    if (isOutOfBound(currentPosition, targetPosition)) {
      swiper.wrapperEl.style.overflow = "hidden";
      swiper.wrapperEl.style.scrollSnapType = "";
      setTimeout(() => {
        swiper.wrapperEl.style.overflow = "";
        swiper.wrapperEl.scrollTo({
          [side]: currentPosition
        });
      });
      window2.cancelAnimationFrame(swiper.cssModeFrameID);
      return;
    }
    swiper.cssModeFrameID = window2.requestAnimationFrame(animate);
  };
  animate();
}
let support;
function calcSupport() {
  const window2 = getWindow();
  const document2 = getDocument();
  return {
    smoothScroll: document2.documentElement && "scrollBehavior" in document2.documentElement.style,
    touch: !!("ontouchstart" in window2 || window2.DocumentTouch && document2 instanceof window2.DocumentTouch),
    passiveListener: function checkPassiveListener() {
      let supportsPassive = false;
      try {
        const opts = Object.defineProperty({}, "passive", {
          get() {
            supportsPassive = true;
          }
        });
        window2.addEventListener("testPassiveListener", null, opts);
      } catch (e) {
      }
      return supportsPassive;
    }(),
    gestures: function checkGestures() {
      return "ongesturestart" in window2;
    }()
  };
}
function getSupport() {
  if (!support) {
    support = calcSupport();
  }
  return support;
}
let deviceCached;
function calcDevice(_temp) {
  let {
    userAgent
  } = _temp === void 0 ? {} : _temp;
  const support2 = getSupport();
  const window2 = getWindow();
  const platform = window2.navigator.platform;
  const ua = userAgent || window2.navigator.userAgent;
  const device = {
    ios: false,
    android: false
  };
  const screenWidth = window2.screen.width;
  const screenHeight = window2.screen.height;
  const android = ua.match(/(Android);?[\s\/]+([\d.]+)?/);
  let ipad = ua.match(/(iPad).*OS\s([\d_]+)/);
  const ipod = ua.match(/(iPod)(.*OS\s([\d_]+))?/);
  const iphone = !ipad && ua.match(/(iPhone\sOS|iOS)\s([\d_]+)/);
  const windows = platform === "Win32";
  let macos = platform === "MacIntel";
  const iPadScreens = ["1024x1366", "1366x1024", "834x1194", "1194x834", "834x1112", "1112x834", "768x1024", "1024x768", "820x1180", "1180x820", "810x1080", "1080x810"];
  if (!ipad && macos && support2.touch && iPadScreens.indexOf(`${screenWidth}x${screenHeight}`) >= 0) {
    ipad = ua.match(/(Version)\/([\d.]+)/);
    if (!ipad)
      ipad = [0, 1, "13_0_0"];
    macos = false;
  }
  if (android && !windows) {
    device.os = "android";
    device.android = true;
  }
  if (ipad || iphone || ipod) {
    device.os = "ios";
    device.ios = true;
  }
  return device;
}
function getDevice(overrides) {
  if (overrides === void 0) {
    overrides = {};
  }
  if (!deviceCached) {
    deviceCached = calcDevice(overrides);
  }
  return deviceCached;
}
let browser;
function calcBrowser() {
  const window2 = getWindow();
  function isSafari() {
    const ua = window2.navigator.userAgent.toLowerCase();
    return ua.indexOf("safari") >= 0 && ua.indexOf("chrome") < 0 && ua.indexOf("android") < 0;
  }
  return {
    isSafari: isSafari(),
    isWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(window2.navigator.userAgent)
  };
}
function getBrowser() {
  if (!browser) {
    browser = calcBrowser();
  }
  return browser;
}
function Resize(_ref) {
  let {
    swiper,
    on: on2,
    emit
  } = _ref;
  const window2 = getWindow();
  let observer = null;
  let animationFrame = null;
  const resizeHandler = () => {
    if (!swiper || swiper.destroyed || !swiper.initialized)
      return;
    emit("beforeResize");
    emit("resize");
  };
  const createObserver = () => {
    if (!swiper || swiper.destroyed || !swiper.initialized)
      return;
    observer = new ResizeObserver((entries) => {
      animationFrame = window2.requestAnimationFrame(() => {
        const {
          width,
          height
        } = swiper;
        let newWidth = width;
        let newHeight = height;
        entries.forEach((_ref2) => {
          let {
            contentBoxSize,
            contentRect,
            target
          } = _ref2;
          if (target && target !== swiper.el)
            return;
          newWidth = contentRect ? contentRect.width : (contentBoxSize[0] || contentBoxSize).inlineSize;
          newHeight = contentRect ? contentRect.height : (contentBoxSize[0] || contentBoxSize).blockSize;
        });
        if (newWidth !== width || newHeight !== height) {
          resizeHandler();
        }
      });
    });
    observer.observe(swiper.el);
  };
  const removeObserver = () => {
    if (animationFrame) {
      window2.cancelAnimationFrame(animationFrame);
    }
    if (observer && observer.unobserve && swiper.el) {
      observer.unobserve(swiper.el);
      observer = null;
    }
  };
  const orientationChangeHandler = () => {
    if (!swiper || swiper.destroyed || !swiper.initialized)
      return;
    emit("orientationchange");
  };
  on2("init", () => {
    if (swiper.params.resizeObserver && typeof window2.ResizeObserver !== "undefined") {
      createObserver();
      return;
    }
    window2.addEventListener("resize", resizeHandler);
    window2.addEventListener("orientationchange", orientationChangeHandler);
  });
  on2("destroy", () => {
    removeObserver();
    window2.removeEventListener("resize", resizeHandler);
    window2.removeEventListener("orientationchange", orientationChangeHandler);
  });
}
function Observer(_ref) {
  let {
    swiper,
    extendParams,
    on: on2,
    emit
  } = _ref;
  const observers = [];
  const window2 = getWindow();
  const attach = function(target, options) {
    if (options === void 0) {
      options = {};
    }
    const ObserverFunc = window2.MutationObserver || window2.WebkitMutationObserver;
    const observer = new ObserverFunc((mutations) => {
      if (mutations.length === 1) {
        emit("observerUpdate", mutations[0]);
        return;
      }
      const observerUpdate = function observerUpdate2() {
        emit("observerUpdate", mutations[0]);
      };
      if (window2.requestAnimationFrame) {
        window2.requestAnimationFrame(observerUpdate);
      } else {
        window2.setTimeout(observerUpdate, 0);
      }
    });
    observer.observe(target, {
      attributes: typeof options.attributes === "undefined" ? true : options.attributes,
      childList: typeof options.childList === "undefined" ? true : options.childList,
      characterData: typeof options.characterData === "undefined" ? true : options.characterData
    });
    observers.push(observer);
  };
  const init = () => {
    if (!swiper.params.observer)
      return;
    if (swiper.params.observeParents) {
      const containerParents = swiper.$el.parents();
      for (let i = 0; i < containerParents.length; i += 1) {
        attach(containerParents[i]);
      }
    }
    attach(swiper.$el[0], {
      childList: swiper.params.observeSlideChildren
    });
    attach(swiper.$wrapperEl[0], {
      attributes: false
    });
  };
  const destroy = () => {
    observers.forEach((observer) => {
      observer.disconnect();
    });
    observers.splice(0, observers.length);
  };
  extendParams({
    observer: false,
    observeParents: false,
    observeSlideChildren: false
  });
  on2("init", init);
  on2("destroy", destroy);
}
var eventsEmitter = {
  on(events2, handler, priority) {
    const self = this;
    if (typeof handler !== "function")
      return self;
    const method = priority ? "unshift" : "push";
    events2.split(" ").forEach((event) => {
      if (!self.eventsListeners[event])
        self.eventsListeners[event] = [];
      self.eventsListeners[event][method](handler);
    });
    return self;
  },
  once(events2, handler, priority) {
    const self = this;
    if (typeof handler !== "function")
      return self;
    function onceHandler() {
      self.off(events2, onceHandler);
      if (onceHandler.__emitterProxy) {
        delete onceHandler.__emitterProxy;
      }
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }
      handler.apply(self, args);
    }
    onceHandler.__emitterProxy = handler;
    return self.on(events2, onceHandler, priority);
  },
  onAny(handler, priority) {
    const self = this;
    if (typeof handler !== "function")
      return self;
    const method = priority ? "unshift" : "push";
    if (self.eventsAnyListeners.indexOf(handler) < 0) {
      self.eventsAnyListeners[method](handler);
    }
    return self;
  },
  offAny(handler) {
    const self = this;
    if (!self.eventsAnyListeners)
      return self;
    const index2 = self.eventsAnyListeners.indexOf(handler);
    if (index2 >= 0) {
      self.eventsAnyListeners.splice(index2, 1);
    }
    return self;
  },
  off(events2, handler) {
    const self = this;
    if (!self.eventsListeners)
      return self;
    events2.split(" ").forEach((event) => {
      if (typeof handler === "undefined") {
        self.eventsListeners[event] = [];
      } else if (self.eventsListeners[event]) {
        self.eventsListeners[event].forEach((eventHandler, index2) => {
          if (eventHandler === handler || eventHandler.__emitterProxy && eventHandler.__emitterProxy === handler) {
            self.eventsListeners[event].splice(index2, 1);
          }
        });
      }
    });
    return self;
  },
  emit() {
    const self = this;
    if (!self.eventsListeners)
      return self;
    let events2;
    let data;
    let context;
    for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      args[_key2] = arguments[_key2];
    }
    if (typeof args[0] === "string" || Array.isArray(args[0])) {
      events2 = args[0];
      data = args.slice(1, args.length);
      context = self;
    } else {
      events2 = args[0].events;
      data = args[0].data;
      context = args[0].context || self;
    }
    data.unshift(context);
    const eventsArray = Array.isArray(events2) ? events2 : events2.split(" ");
    eventsArray.forEach((event) => {
      if (self.eventsAnyListeners && self.eventsAnyListeners.length) {
        self.eventsAnyListeners.forEach((eventHandler) => {
          eventHandler.apply(context, [event, ...data]);
        });
      }
      if (self.eventsListeners && self.eventsListeners[event]) {
        self.eventsListeners[event].forEach((eventHandler) => {
          eventHandler.apply(context, data);
        });
      }
    });
    return self;
  }
};
function updateSize() {
  const swiper = this;
  let width;
  let height;
  const $el = swiper.$el;
  if (typeof swiper.params.width !== "undefined" && swiper.params.width !== null) {
    width = swiper.params.width;
  } else {
    width = $el[0].clientWidth;
  }
  if (typeof swiper.params.height !== "undefined" && swiper.params.height !== null) {
    height = swiper.params.height;
  } else {
    height = $el[0].clientHeight;
  }
  if (width === 0 && swiper.isHorizontal() || height === 0 && swiper.isVertical()) {
    return;
  }
  width = width - parseInt($el.css("padding-left") || 0, 10) - parseInt($el.css("padding-right") || 0, 10);
  height = height - parseInt($el.css("padding-top") || 0, 10) - parseInt($el.css("padding-bottom") || 0, 10);
  if (Number.isNaN(width))
    width = 0;
  if (Number.isNaN(height))
    height = 0;
  Object.assign(swiper, {
    width,
    height,
    size: swiper.isHorizontal() ? width : height
  });
}
function updateSlides() {
  const swiper = this;
  function getDirectionLabel(property) {
    if (swiper.isHorizontal()) {
      return property;
    }
    return {
      "width": "height",
      "margin-top": "margin-left",
      "margin-bottom ": "margin-right",
      "margin-left": "margin-top",
      "margin-right": "margin-bottom",
      "padding-left": "padding-top",
      "padding-right": "padding-bottom",
      "marginRight": "marginBottom"
    }[property];
  }
  function getDirectionPropertyValue(node, label) {
    return parseFloat(node.getPropertyValue(getDirectionLabel(label)) || 0);
  }
  const params = swiper.params;
  const {
    $wrapperEl,
    size: swiperSize,
    rtlTranslate: rtl,
    wrongRTL
  } = swiper;
  const isVirtual = swiper.virtual && params.virtual.enabled;
  const previousSlidesLength = isVirtual ? swiper.virtual.slides.length : swiper.slides.length;
  const slides = $wrapperEl.children(`.${swiper.params.slideClass}`);
  const slidesLength = isVirtual ? swiper.virtual.slides.length : slides.length;
  let snapGrid = [];
  const slidesGrid = [];
  const slidesSizesGrid = [];
  let offsetBefore = params.slidesOffsetBefore;
  if (typeof offsetBefore === "function") {
    offsetBefore = params.slidesOffsetBefore.call(swiper);
  }
  let offsetAfter = params.slidesOffsetAfter;
  if (typeof offsetAfter === "function") {
    offsetAfter = params.slidesOffsetAfter.call(swiper);
  }
  const previousSnapGridLength = swiper.snapGrid.length;
  const previousSlidesGridLength = swiper.slidesGrid.length;
  let spaceBetween = params.spaceBetween;
  let slidePosition = -offsetBefore;
  let prevSlideSize = 0;
  let index2 = 0;
  if (typeof swiperSize === "undefined") {
    return;
  }
  if (typeof spaceBetween === "string" && spaceBetween.indexOf("%") >= 0) {
    spaceBetween = parseFloat(spaceBetween.replace("%", "")) / 100 * swiperSize;
  }
  swiper.virtualSize = -spaceBetween;
  if (rtl)
    slides.css({
      marginLeft: "",
      marginBottom: "",
      marginTop: ""
    });
  else
    slides.css({
      marginRight: "",
      marginBottom: "",
      marginTop: ""
    });
  if (params.centeredSlides && params.cssMode) {
    setCSSProperty(swiper.wrapperEl, "--swiper-centered-offset-before", "");
    setCSSProperty(swiper.wrapperEl, "--swiper-centered-offset-after", "");
  }
  const gridEnabled = params.grid && params.grid.rows > 1 && swiper.grid;
  if (gridEnabled) {
    swiper.grid.initSlides(slidesLength);
  }
  let slideSize;
  const shouldResetSlideSize = params.slidesPerView === "auto" && params.breakpoints && Object.keys(params.breakpoints).filter((key) => {
    return typeof params.breakpoints[key].slidesPerView !== "undefined";
  }).length > 0;
  for (let i = 0; i < slidesLength; i += 1) {
    slideSize = 0;
    const slide2 = slides.eq(i);
    if (gridEnabled) {
      swiper.grid.updateSlide(i, slide2, slidesLength, getDirectionLabel);
    }
    if (slide2.css("display") === "none")
      continue;
    if (params.slidesPerView === "auto") {
      if (shouldResetSlideSize) {
        slides[i].style[getDirectionLabel("width")] = ``;
      }
      const slideStyles = getComputedStyle(slide2[0]);
      const currentTransform = slide2[0].style.transform;
      const currentWebKitTransform = slide2[0].style.webkitTransform;
      if (currentTransform) {
        slide2[0].style.transform = "none";
      }
      if (currentWebKitTransform) {
        slide2[0].style.webkitTransform = "none";
      }
      if (params.roundLengths) {
        slideSize = swiper.isHorizontal() ? slide2.outerWidth(true) : slide2.outerHeight(true);
      } else {
        const width = getDirectionPropertyValue(slideStyles, "width");
        const paddingLeft = getDirectionPropertyValue(slideStyles, "padding-left");
        const paddingRight = getDirectionPropertyValue(slideStyles, "padding-right");
        const marginLeft = getDirectionPropertyValue(slideStyles, "margin-left");
        const marginRight = getDirectionPropertyValue(slideStyles, "margin-right");
        const boxSizing = slideStyles.getPropertyValue("box-sizing");
        if (boxSizing && boxSizing === "border-box") {
          slideSize = width + marginLeft + marginRight;
        } else {
          const {
            clientWidth,
            offsetWidth
          } = slide2[0];
          slideSize = width + paddingLeft + paddingRight + marginLeft + marginRight + (offsetWidth - clientWidth);
        }
      }
      if (currentTransform) {
        slide2[0].style.transform = currentTransform;
      }
      if (currentWebKitTransform) {
        slide2[0].style.webkitTransform = currentWebKitTransform;
      }
      if (params.roundLengths)
        slideSize = Math.floor(slideSize);
    } else {
      slideSize = (swiperSize - (params.slidesPerView - 1) * spaceBetween) / params.slidesPerView;
      if (params.roundLengths)
        slideSize = Math.floor(slideSize);
      if (slides[i]) {
        slides[i].style[getDirectionLabel("width")] = `${slideSize}px`;
      }
    }
    if (slides[i]) {
      slides[i].swiperSlideSize = slideSize;
    }
    slidesSizesGrid.push(slideSize);
    if (params.centeredSlides) {
      slidePosition = slidePosition + slideSize / 2 + prevSlideSize / 2 + spaceBetween;
      if (prevSlideSize === 0 && i !== 0)
        slidePosition = slidePosition - swiperSize / 2 - spaceBetween;
      if (i === 0)
        slidePosition = slidePosition - swiperSize / 2 - spaceBetween;
      if (Math.abs(slidePosition) < 1 / 1e3)
        slidePosition = 0;
      if (params.roundLengths)
        slidePosition = Math.floor(slidePosition);
      if (index2 % params.slidesPerGroup === 0)
        snapGrid.push(slidePosition);
      slidesGrid.push(slidePosition);
    } else {
      if (params.roundLengths)
        slidePosition = Math.floor(slidePosition);
      if ((index2 - Math.min(swiper.params.slidesPerGroupSkip, index2)) % swiper.params.slidesPerGroup === 0)
        snapGrid.push(slidePosition);
      slidesGrid.push(slidePosition);
      slidePosition = slidePosition + slideSize + spaceBetween;
    }
    swiper.virtualSize += slideSize + spaceBetween;
    prevSlideSize = slideSize;
    index2 += 1;
  }
  swiper.virtualSize = Math.max(swiper.virtualSize, swiperSize) + offsetAfter;
  if (rtl && wrongRTL && (params.effect === "slide" || params.effect === "coverflow")) {
    $wrapperEl.css({
      width: `${swiper.virtualSize + params.spaceBetween}px`
    });
  }
  if (params.setWrapperSize) {
    $wrapperEl.css({
      [getDirectionLabel("width")]: `${swiper.virtualSize + params.spaceBetween}px`
    });
  }
  if (gridEnabled) {
    swiper.grid.updateWrapperSize(slideSize, snapGrid, getDirectionLabel);
  }
  if (!params.centeredSlides) {
    const newSlidesGrid = [];
    for (let i = 0; i < snapGrid.length; i += 1) {
      let slidesGridItem = snapGrid[i];
      if (params.roundLengths)
        slidesGridItem = Math.floor(slidesGridItem);
      if (snapGrid[i] <= swiper.virtualSize - swiperSize) {
        newSlidesGrid.push(slidesGridItem);
      }
    }
    snapGrid = newSlidesGrid;
    if (Math.floor(swiper.virtualSize - swiperSize) - Math.floor(snapGrid[snapGrid.length - 1]) > 1) {
      snapGrid.push(swiper.virtualSize - swiperSize);
    }
  }
  if (snapGrid.length === 0)
    snapGrid = [0];
  if (params.spaceBetween !== 0) {
    const key = swiper.isHorizontal() && rtl ? "marginLeft" : getDirectionLabel("marginRight");
    slides.filter((_, slideIndex) => {
      if (!params.cssMode)
        return true;
      if (slideIndex === slides.length - 1) {
        return false;
      }
      return true;
    }).css({
      [key]: `${spaceBetween}px`
    });
  }
  if (params.centeredSlides && params.centeredSlidesBounds) {
    let allSlidesSize = 0;
    slidesSizesGrid.forEach((slideSizeValue) => {
      allSlidesSize += slideSizeValue + (params.spaceBetween ? params.spaceBetween : 0);
    });
    allSlidesSize -= params.spaceBetween;
    const maxSnap = allSlidesSize - swiperSize;
    snapGrid = snapGrid.map((snap) => {
      if (snap < 0)
        return -offsetBefore;
      if (snap > maxSnap)
        return maxSnap + offsetAfter;
      return snap;
    });
  }
  if (params.centerInsufficientSlides) {
    let allSlidesSize = 0;
    slidesSizesGrid.forEach((slideSizeValue) => {
      allSlidesSize += slideSizeValue + (params.spaceBetween ? params.spaceBetween : 0);
    });
    allSlidesSize -= params.spaceBetween;
    if (allSlidesSize < swiperSize) {
      const allSlidesOffset = (swiperSize - allSlidesSize) / 2;
      snapGrid.forEach((snap, snapIndex) => {
        snapGrid[snapIndex] = snap - allSlidesOffset;
      });
      slidesGrid.forEach((snap, snapIndex) => {
        slidesGrid[snapIndex] = snap + allSlidesOffset;
      });
    }
  }
  Object.assign(swiper, {
    slides,
    snapGrid,
    slidesGrid,
    slidesSizesGrid
  });
  if (params.centeredSlides && params.cssMode && !params.centeredSlidesBounds) {
    setCSSProperty(swiper.wrapperEl, "--swiper-centered-offset-before", `${-snapGrid[0]}px`);
    setCSSProperty(swiper.wrapperEl, "--swiper-centered-offset-after", `${swiper.size / 2 - slidesSizesGrid[slidesSizesGrid.length - 1] / 2}px`);
    const addToSnapGrid = -swiper.snapGrid[0];
    const addToSlidesGrid = -swiper.slidesGrid[0];
    swiper.snapGrid = swiper.snapGrid.map((v) => v + addToSnapGrid);
    swiper.slidesGrid = swiper.slidesGrid.map((v) => v + addToSlidesGrid);
  }
  if (slidesLength !== previousSlidesLength) {
    swiper.emit("slidesLengthChange");
  }
  if (snapGrid.length !== previousSnapGridLength) {
    if (swiper.params.watchOverflow)
      swiper.checkOverflow();
    swiper.emit("snapGridLengthChange");
  }
  if (slidesGrid.length !== previousSlidesGridLength) {
    swiper.emit("slidesGridLengthChange");
  }
  if (params.watchSlidesProgress) {
    swiper.updateSlidesOffset();
  }
  if (!isVirtual && !params.cssMode && (params.effect === "slide" || params.effect === "fade")) {
    const backFaceHiddenClass = `${params.containerModifierClass}backface-hidden`;
    const hasClassBackfaceClassAdded = swiper.$el.hasClass(backFaceHiddenClass);
    if (slidesLength <= params.maxBackfaceHiddenSlides) {
      if (!hasClassBackfaceClassAdded)
        swiper.$el.addClass(backFaceHiddenClass);
    } else if (hasClassBackfaceClassAdded) {
      swiper.$el.removeClass(backFaceHiddenClass);
    }
  }
}
function updateAutoHeight(speed) {
  const swiper = this;
  const activeSlides = [];
  const isVirtual = swiper.virtual && swiper.params.virtual.enabled;
  let newHeight = 0;
  let i;
  if (typeof speed === "number") {
    swiper.setTransition(speed);
  } else if (speed === true) {
    swiper.setTransition(swiper.params.speed);
  }
  const getSlideByIndex = (index2) => {
    if (isVirtual) {
      return swiper.slides.filter((el) => parseInt(el.getAttribute("data-swiper-slide-index"), 10) === index2)[0];
    }
    return swiper.slides.eq(index2)[0];
  };
  if (swiper.params.slidesPerView !== "auto" && swiper.params.slidesPerView > 1) {
    if (swiper.params.centeredSlides) {
      swiper.visibleSlides.each((slide2) => {
        activeSlides.push(slide2);
      });
    } else {
      for (i = 0; i < Math.ceil(swiper.params.slidesPerView); i += 1) {
        const index2 = swiper.activeIndex + i;
        if (index2 > swiper.slides.length && !isVirtual)
          break;
        activeSlides.push(getSlideByIndex(index2));
      }
    }
  } else {
    activeSlides.push(getSlideByIndex(swiper.activeIndex));
  }
  for (i = 0; i < activeSlides.length; i += 1) {
    if (typeof activeSlides[i] !== "undefined") {
      const height = activeSlides[i].offsetHeight;
      newHeight = height > newHeight ? height : newHeight;
    }
  }
  if (newHeight || newHeight === 0)
    swiper.$wrapperEl.css("height", `${newHeight}px`);
}
function updateSlidesOffset() {
  const swiper = this;
  const slides = swiper.slides;
  for (let i = 0; i < slides.length; i += 1) {
    slides[i].swiperSlideOffset = swiper.isHorizontal() ? slides[i].offsetLeft : slides[i].offsetTop;
  }
}
function updateSlidesProgress(translate2) {
  if (translate2 === void 0) {
    translate2 = this && this.translate || 0;
  }
  const swiper = this;
  const params = swiper.params;
  const {
    slides,
    rtlTranslate: rtl,
    snapGrid
  } = swiper;
  if (slides.length === 0)
    return;
  if (typeof slides[0].swiperSlideOffset === "undefined")
    swiper.updateSlidesOffset();
  let offsetCenter = -translate2;
  if (rtl)
    offsetCenter = translate2;
  slides.removeClass(params.slideVisibleClass);
  swiper.visibleSlidesIndexes = [];
  swiper.visibleSlides = [];
  for (let i = 0; i < slides.length; i += 1) {
    const slide2 = slides[i];
    let slideOffset = slide2.swiperSlideOffset;
    if (params.cssMode && params.centeredSlides) {
      slideOffset -= slides[0].swiperSlideOffset;
    }
    const slideProgress = (offsetCenter + (params.centeredSlides ? swiper.minTranslate() : 0) - slideOffset) / (slide2.swiperSlideSize + params.spaceBetween);
    const originalSlideProgress = (offsetCenter - snapGrid[0] + (params.centeredSlides ? swiper.minTranslate() : 0) - slideOffset) / (slide2.swiperSlideSize + params.spaceBetween);
    const slideBefore = -(offsetCenter - slideOffset);
    const slideAfter = slideBefore + swiper.slidesSizesGrid[i];
    const isVisible = slideBefore >= 0 && slideBefore < swiper.size - 1 || slideAfter > 1 && slideAfter <= swiper.size || slideBefore <= 0 && slideAfter >= swiper.size;
    if (isVisible) {
      swiper.visibleSlides.push(slide2);
      swiper.visibleSlidesIndexes.push(i);
      slides.eq(i).addClass(params.slideVisibleClass);
    }
    slide2.progress = rtl ? -slideProgress : slideProgress;
    slide2.originalProgress = rtl ? -originalSlideProgress : originalSlideProgress;
  }
  swiper.visibleSlides = $(swiper.visibleSlides);
}
function updateProgress(translate2) {
  const swiper = this;
  if (typeof translate2 === "undefined") {
    const multiplier = swiper.rtlTranslate ? -1 : 1;
    translate2 = swiper && swiper.translate && swiper.translate * multiplier || 0;
  }
  const params = swiper.params;
  const translatesDiff = swiper.maxTranslate() - swiper.minTranslate();
  let {
    progress,
    isBeginning,
    isEnd
  } = swiper;
  const wasBeginning = isBeginning;
  const wasEnd = isEnd;
  if (translatesDiff === 0) {
    progress = 0;
    isBeginning = true;
    isEnd = true;
  } else {
    progress = (translate2 - swiper.minTranslate()) / translatesDiff;
    isBeginning = progress <= 0;
    isEnd = progress >= 1;
  }
  Object.assign(swiper, {
    progress,
    isBeginning,
    isEnd
  });
  if (params.watchSlidesProgress || params.centeredSlides && params.autoHeight)
    swiper.updateSlidesProgress(translate2);
  if (isBeginning && !wasBeginning) {
    swiper.emit("reachBeginning toEdge");
  }
  if (isEnd && !wasEnd) {
    swiper.emit("reachEnd toEdge");
  }
  if (wasBeginning && !isBeginning || wasEnd && !isEnd) {
    swiper.emit("fromEdge");
  }
  swiper.emit("progress", progress);
}
function updateSlidesClasses() {
  const swiper = this;
  const {
    slides,
    params,
    $wrapperEl,
    activeIndex,
    realIndex
  } = swiper;
  const isVirtual = swiper.virtual && params.virtual.enabled;
  slides.removeClass(`${params.slideActiveClass} ${params.slideNextClass} ${params.slidePrevClass} ${params.slideDuplicateActiveClass} ${params.slideDuplicateNextClass} ${params.slideDuplicatePrevClass}`);
  let activeSlide;
  if (isVirtual) {
    activeSlide = swiper.$wrapperEl.find(`.${params.slideClass}[data-swiper-slide-index="${activeIndex}"]`);
  } else {
    activeSlide = slides.eq(activeIndex);
  }
  activeSlide.addClass(params.slideActiveClass);
  if (params.loop) {
    if (activeSlide.hasClass(params.slideDuplicateClass)) {
      $wrapperEl.children(`.${params.slideClass}:not(.${params.slideDuplicateClass})[data-swiper-slide-index="${realIndex}"]`).addClass(params.slideDuplicateActiveClass);
    } else {
      $wrapperEl.children(`.${params.slideClass}.${params.slideDuplicateClass}[data-swiper-slide-index="${realIndex}"]`).addClass(params.slideDuplicateActiveClass);
    }
  }
  let nextSlide = activeSlide.nextAll(`.${params.slideClass}`).eq(0).addClass(params.slideNextClass);
  if (params.loop && nextSlide.length === 0) {
    nextSlide = slides.eq(0);
    nextSlide.addClass(params.slideNextClass);
  }
  let prevSlide = activeSlide.prevAll(`.${params.slideClass}`).eq(0).addClass(params.slidePrevClass);
  if (params.loop && prevSlide.length === 0) {
    prevSlide = slides.eq(-1);
    prevSlide.addClass(params.slidePrevClass);
  }
  if (params.loop) {
    if (nextSlide.hasClass(params.slideDuplicateClass)) {
      $wrapperEl.children(`.${params.slideClass}:not(.${params.slideDuplicateClass})[data-swiper-slide-index="${nextSlide.attr("data-swiper-slide-index")}"]`).addClass(params.slideDuplicateNextClass);
    } else {
      $wrapperEl.children(`.${params.slideClass}.${params.slideDuplicateClass}[data-swiper-slide-index="${nextSlide.attr("data-swiper-slide-index")}"]`).addClass(params.slideDuplicateNextClass);
    }
    if (prevSlide.hasClass(params.slideDuplicateClass)) {
      $wrapperEl.children(`.${params.slideClass}:not(.${params.slideDuplicateClass})[data-swiper-slide-index="${prevSlide.attr("data-swiper-slide-index")}"]`).addClass(params.slideDuplicatePrevClass);
    } else {
      $wrapperEl.children(`.${params.slideClass}.${params.slideDuplicateClass}[data-swiper-slide-index="${prevSlide.attr("data-swiper-slide-index")}"]`).addClass(params.slideDuplicatePrevClass);
    }
  }
  swiper.emitSlidesClasses();
}
function updateActiveIndex(newActiveIndex) {
  const swiper = this;
  const translate2 = swiper.rtlTranslate ? swiper.translate : -swiper.translate;
  const {
    slidesGrid,
    snapGrid,
    params,
    activeIndex: previousIndex,
    realIndex: previousRealIndex,
    snapIndex: previousSnapIndex
  } = swiper;
  let activeIndex = newActiveIndex;
  let snapIndex;
  if (typeof activeIndex === "undefined") {
    for (let i = 0; i < slidesGrid.length; i += 1) {
      if (typeof slidesGrid[i + 1] !== "undefined") {
        if (translate2 >= slidesGrid[i] && translate2 < slidesGrid[i + 1] - (slidesGrid[i + 1] - slidesGrid[i]) / 2) {
          activeIndex = i;
        } else if (translate2 >= slidesGrid[i] && translate2 < slidesGrid[i + 1]) {
          activeIndex = i + 1;
        }
      } else if (translate2 >= slidesGrid[i]) {
        activeIndex = i;
      }
    }
    if (params.normalizeSlideIndex) {
      if (activeIndex < 0 || typeof activeIndex === "undefined")
        activeIndex = 0;
    }
  }
  if (snapGrid.indexOf(translate2) >= 0) {
    snapIndex = snapGrid.indexOf(translate2);
  } else {
    const skip = Math.min(params.slidesPerGroupSkip, activeIndex);
    snapIndex = skip + Math.floor((activeIndex - skip) / params.slidesPerGroup);
  }
  if (snapIndex >= snapGrid.length)
    snapIndex = snapGrid.length - 1;
  if (activeIndex === previousIndex) {
    if (snapIndex !== previousSnapIndex) {
      swiper.snapIndex = snapIndex;
      swiper.emit("snapIndexChange");
    }
    return;
  }
  const realIndex = parseInt(swiper.slides.eq(activeIndex).attr("data-swiper-slide-index") || activeIndex, 10);
  Object.assign(swiper, {
    snapIndex,
    realIndex,
    previousIndex,
    activeIndex
  });
  swiper.emit("activeIndexChange");
  swiper.emit("snapIndexChange");
  if (previousRealIndex !== realIndex) {
    swiper.emit("realIndexChange");
  }
  if (swiper.initialized || swiper.params.runCallbacksOnInit) {
    swiper.emit("slideChange");
  }
}
function updateClickedSlide(e) {
  const swiper = this;
  const params = swiper.params;
  const slide2 = $(e).closest(`.${params.slideClass}`)[0];
  let slideFound = false;
  let slideIndex;
  if (slide2) {
    for (let i = 0; i < swiper.slides.length; i += 1) {
      if (swiper.slides[i] === slide2) {
        slideFound = true;
        slideIndex = i;
        break;
      }
    }
  }
  if (slide2 && slideFound) {
    swiper.clickedSlide = slide2;
    if (swiper.virtual && swiper.params.virtual.enabled) {
      swiper.clickedIndex = parseInt($(slide2).attr("data-swiper-slide-index"), 10);
    } else {
      swiper.clickedIndex = slideIndex;
    }
  } else {
    swiper.clickedSlide = void 0;
    swiper.clickedIndex = void 0;
    return;
  }
  if (params.slideToClickedSlide && swiper.clickedIndex !== void 0 && swiper.clickedIndex !== swiper.activeIndex) {
    swiper.slideToClickedSlide();
  }
}
var update = {
  updateSize,
  updateSlides,
  updateAutoHeight,
  updateSlidesOffset,
  updateSlidesProgress,
  updateProgress,
  updateSlidesClasses,
  updateActiveIndex,
  updateClickedSlide
};
function getSwiperTranslate(axis) {
  if (axis === void 0) {
    axis = this.isHorizontal() ? "x" : "y";
  }
  const swiper = this;
  const {
    params,
    rtlTranslate: rtl,
    translate: translate2,
    $wrapperEl
  } = swiper;
  if (params.virtualTranslate) {
    return rtl ? -translate2 : translate2;
  }
  if (params.cssMode) {
    return translate2;
  }
  let currentTranslate = getTranslate($wrapperEl[0], axis);
  if (rtl)
    currentTranslate = -currentTranslate;
  return currentTranslate || 0;
}
function setTranslate(translate2, byController) {
  const swiper = this;
  const {
    rtlTranslate: rtl,
    params,
    $wrapperEl,
    wrapperEl,
    progress
  } = swiper;
  let x = 0;
  let y = 0;
  const z = 0;
  if (swiper.isHorizontal()) {
    x = rtl ? -translate2 : translate2;
  } else {
    y = translate2;
  }
  if (params.roundLengths) {
    x = Math.floor(x);
    y = Math.floor(y);
  }
  if (params.cssMode) {
    wrapperEl[swiper.isHorizontal() ? "scrollLeft" : "scrollTop"] = swiper.isHorizontal() ? -x : -y;
  } else if (!params.virtualTranslate) {
    $wrapperEl.transform(`translate3d(${x}px, ${y}px, ${z}px)`);
  }
  swiper.previousTranslate = swiper.translate;
  swiper.translate = swiper.isHorizontal() ? x : y;
  let newProgress;
  const translatesDiff = swiper.maxTranslate() - swiper.minTranslate();
  if (translatesDiff === 0) {
    newProgress = 0;
  } else {
    newProgress = (translate2 - swiper.minTranslate()) / translatesDiff;
  }
  if (newProgress !== progress) {
    swiper.updateProgress(translate2);
  }
  swiper.emit("setTranslate", swiper.translate, byController);
}
function minTranslate() {
  return -this.snapGrid[0];
}
function maxTranslate() {
  return -this.snapGrid[this.snapGrid.length - 1];
}
function translateTo(translate2, speed, runCallbacks, translateBounds, internal) {
  if (translate2 === void 0) {
    translate2 = 0;
  }
  if (speed === void 0) {
    speed = this.params.speed;
  }
  if (runCallbacks === void 0) {
    runCallbacks = true;
  }
  if (translateBounds === void 0) {
    translateBounds = true;
  }
  const swiper = this;
  const {
    params,
    wrapperEl
  } = swiper;
  if (swiper.animating && params.preventInteractionOnTransition) {
    return false;
  }
  const minTranslate2 = swiper.minTranslate();
  const maxTranslate2 = swiper.maxTranslate();
  let newTranslate;
  if (translateBounds && translate2 > minTranslate2)
    newTranslate = minTranslate2;
  else if (translateBounds && translate2 < maxTranslate2)
    newTranslate = maxTranslate2;
  else
    newTranslate = translate2;
  swiper.updateProgress(newTranslate);
  if (params.cssMode) {
    const isH = swiper.isHorizontal();
    if (speed === 0) {
      wrapperEl[isH ? "scrollLeft" : "scrollTop"] = -newTranslate;
    } else {
      if (!swiper.support.smoothScroll) {
        animateCSSModeScroll({
          swiper,
          targetPosition: -newTranslate,
          side: isH ? "left" : "top"
        });
        return true;
      }
      wrapperEl.scrollTo({
        [isH ? "left" : "top"]: -newTranslate,
        behavior: "smooth"
      });
    }
    return true;
  }
  if (speed === 0) {
    swiper.setTransition(0);
    swiper.setTranslate(newTranslate);
    if (runCallbacks) {
      swiper.emit("beforeTransitionStart", speed, internal);
      swiper.emit("transitionEnd");
    }
  } else {
    swiper.setTransition(speed);
    swiper.setTranslate(newTranslate);
    if (runCallbacks) {
      swiper.emit("beforeTransitionStart", speed, internal);
      swiper.emit("transitionStart");
    }
    if (!swiper.animating) {
      swiper.animating = true;
      if (!swiper.onTranslateToWrapperTransitionEnd) {
        swiper.onTranslateToWrapperTransitionEnd = function transitionEnd2(e) {
          if (!swiper || swiper.destroyed)
            return;
          if (e.target !== this)
            return;
          swiper.$wrapperEl[0].removeEventListener("transitionend", swiper.onTranslateToWrapperTransitionEnd);
          swiper.$wrapperEl[0].removeEventListener("webkitTransitionEnd", swiper.onTranslateToWrapperTransitionEnd);
          swiper.onTranslateToWrapperTransitionEnd = null;
          delete swiper.onTranslateToWrapperTransitionEnd;
          if (runCallbacks) {
            swiper.emit("transitionEnd");
          }
        };
      }
      swiper.$wrapperEl[0].addEventListener("transitionend", swiper.onTranslateToWrapperTransitionEnd);
      swiper.$wrapperEl[0].addEventListener("webkitTransitionEnd", swiper.onTranslateToWrapperTransitionEnd);
    }
  }
  return true;
}
var translate = {
  getTranslate: getSwiperTranslate,
  setTranslate,
  minTranslate,
  maxTranslate,
  translateTo
};
function setTransition(duration, byController) {
  const swiper = this;
  if (!swiper.params.cssMode) {
    swiper.$wrapperEl.transition(duration);
  }
  swiper.emit("setTransition", duration, byController);
}
function transitionEmit(_ref) {
  let {
    swiper,
    runCallbacks,
    direction,
    step
  } = _ref;
  const {
    activeIndex,
    previousIndex
  } = swiper;
  let dir = direction;
  if (!dir) {
    if (activeIndex > previousIndex)
      dir = "next";
    else if (activeIndex < previousIndex)
      dir = "prev";
    else
      dir = "reset";
  }
  swiper.emit(`transition${step}`);
  if (runCallbacks && activeIndex !== previousIndex) {
    if (dir === "reset") {
      swiper.emit(`slideResetTransition${step}`);
      return;
    }
    swiper.emit(`slideChangeTransition${step}`);
    if (dir === "next") {
      swiper.emit(`slideNextTransition${step}`);
    } else {
      swiper.emit(`slidePrevTransition${step}`);
    }
  }
}
function transitionStart(runCallbacks, direction) {
  if (runCallbacks === void 0) {
    runCallbacks = true;
  }
  const swiper = this;
  const {
    params
  } = swiper;
  if (params.cssMode)
    return;
  if (params.autoHeight) {
    swiper.updateAutoHeight();
  }
  transitionEmit({
    swiper,
    runCallbacks,
    direction,
    step: "Start"
  });
}
function transitionEnd(runCallbacks, direction) {
  if (runCallbacks === void 0) {
    runCallbacks = true;
  }
  const swiper = this;
  const {
    params
  } = swiper;
  swiper.animating = false;
  if (params.cssMode)
    return;
  swiper.setTransition(0);
  transitionEmit({
    swiper,
    runCallbacks,
    direction,
    step: "End"
  });
}
var transition = {
  setTransition,
  transitionStart,
  transitionEnd
};
function slideTo(index2, speed, runCallbacks, internal, initial) {
  if (index2 === void 0) {
    index2 = 0;
  }
  if (speed === void 0) {
    speed = this.params.speed;
  }
  if (runCallbacks === void 0) {
    runCallbacks = true;
  }
  if (typeof index2 !== "number" && typeof index2 !== "string") {
    throw new Error(`The 'index' argument cannot have type other than 'number' or 'string'. [${typeof index2}] given.`);
  }
  if (typeof index2 === "string") {
    const indexAsNumber = parseInt(index2, 10);
    const isValidNumber = isFinite(indexAsNumber);
    if (!isValidNumber) {
      throw new Error(`The passed-in 'index' (string) couldn't be converted to 'number'. [${index2}] given.`);
    }
    index2 = indexAsNumber;
  }
  const swiper = this;
  let slideIndex = index2;
  if (slideIndex < 0)
    slideIndex = 0;
  const {
    params,
    snapGrid,
    slidesGrid,
    previousIndex,
    activeIndex,
    rtlTranslate: rtl,
    wrapperEl,
    enabled
  } = swiper;
  if (swiper.animating && params.preventInteractionOnTransition || !enabled && !internal && !initial) {
    return false;
  }
  const skip = Math.min(swiper.params.slidesPerGroupSkip, slideIndex);
  let snapIndex = skip + Math.floor((slideIndex - skip) / swiper.params.slidesPerGroup);
  if (snapIndex >= snapGrid.length)
    snapIndex = snapGrid.length - 1;
  if ((activeIndex || params.initialSlide || 0) === (previousIndex || 0) && runCallbacks) {
    swiper.emit("beforeSlideChangeStart");
  }
  const translate2 = -snapGrid[snapIndex];
  swiper.updateProgress(translate2);
  if (params.normalizeSlideIndex) {
    for (let i = 0; i < slidesGrid.length; i += 1) {
      const normalizedTranslate = -Math.floor(translate2 * 100);
      const normalizedGrid = Math.floor(slidesGrid[i] * 100);
      const normalizedGridNext = Math.floor(slidesGrid[i + 1] * 100);
      if (typeof slidesGrid[i + 1] !== "undefined") {
        if (normalizedTranslate >= normalizedGrid && normalizedTranslate < normalizedGridNext - (normalizedGridNext - normalizedGrid) / 2) {
          slideIndex = i;
        } else if (normalizedTranslate >= normalizedGrid && normalizedTranslate < normalizedGridNext) {
          slideIndex = i + 1;
        }
      } else if (normalizedTranslate >= normalizedGrid) {
        slideIndex = i;
      }
    }
  }
  if (swiper.initialized && slideIndex !== activeIndex) {
    if (!swiper.allowSlideNext && translate2 < swiper.translate && translate2 < swiper.minTranslate()) {
      return false;
    }
    if (!swiper.allowSlidePrev && translate2 > swiper.translate && translate2 > swiper.maxTranslate()) {
      if ((activeIndex || 0) !== slideIndex)
        return false;
    }
  }
  let direction;
  if (slideIndex > activeIndex)
    direction = "next";
  else if (slideIndex < activeIndex)
    direction = "prev";
  else
    direction = "reset";
  if (rtl && -translate2 === swiper.translate || !rtl && translate2 === swiper.translate) {
    swiper.updateActiveIndex(slideIndex);
    if (params.autoHeight) {
      swiper.updateAutoHeight();
    }
    swiper.updateSlidesClasses();
    if (params.effect !== "slide") {
      swiper.setTranslate(translate2);
    }
    if (direction !== "reset") {
      swiper.transitionStart(runCallbacks, direction);
      swiper.transitionEnd(runCallbacks, direction);
    }
    return false;
  }
  if (params.cssMode) {
    const isH = swiper.isHorizontal();
    const t = rtl ? translate2 : -translate2;
    if (speed === 0) {
      const isVirtual = swiper.virtual && swiper.params.virtual.enabled;
      if (isVirtual) {
        swiper.wrapperEl.style.scrollSnapType = "none";
        swiper._immediateVirtual = true;
      }
      wrapperEl[isH ? "scrollLeft" : "scrollTop"] = t;
      if (isVirtual) {
        requestAnimationFrame(() => {
          swiper.wrapperEl.style.scrollSnapType = "";
          swiper._swiperImmediateVirtual = false;
        });
      }
    } else {
      if (!swiper.support.smoothScroll) {
        animateCSSModeScroll({
          swiper,
          targetPosition: t,
          side: isH ? "left" : "top"
        });
        return true;
      }
      wrapperEl.scrollTo({
        [isH ? "left" : "top"]: t,
        behavior: "smooth"
      });
    }
    return true;
  }
  swiper.setTransition(speed);
  swiper.setTranslate(translate2);
  swiper.updateActiveIndex(slideIndex);
  swiper.updateSlidesClasses();
  swiper.emit("beforeTransitionStart", speed, internal);
  swiper.transitionStart(runCallbacks, direction);
  if (speed === 0) {
    swiper.transitionEnd(runCallbacks, direction);
  } else if (!swiper.animating) {
    swiper.animating = true;
    if (!swiper.onSlideToWrapperTransitionEnd) {
      swiper.onSlideToWrapperTransitionEnd = function transitionEnd2(e) {
        if (!swiper || swiper.destroyed)
          return;
        if (e.target !== this)
          return;
        swiper.$wrapperEl[0].removeEventListener("transitionend", swiper.onSlideToWrapperTransitionEnd);
        swiper.$wrapperEl[0].removeEventListener("webkitTransitionEnd", swiper.onSlideToWrapperTransitionEnd);
        swiper.onSlideToWrapperTransitionEnd = null;
        delete swiper.onSlideToWrapperTransitionEnd;
        swiper.transitionEnd(runCallbacks, direction);
      };
    }
    swiper.$wrapperEl[0].addEventListener("transitionend", swiper.onSlideToWrapperTransitionEnd);
    swiper.$wrapperEl[0].addEventListener("webkitTransitionEnd", swiper.onSlideToWrapperTransitionEnd);
  }
  return true;
}
function slideToLoop(index2, speed, runCallbacks, internal) {
  if (index2 === void 0) {
    index2 = 0;
  }
  if (speed === void 0) {
    speed = this.params.speed;
  }
  if (runCallbacks === void 0) {
    runCallbacks = true;
  }
  const swiper = this;
  let newIndex = index2;
  if (swiper.params.loop) {
    newIndex += swiper.loopedSlides;
  }
  return swiper.slideTo(newIndex, speed, runCallbacks, internal);
}
function slideNext(speed, runCallbacks, internal) {
  if (speed === void 0) {
    speed = this.params.speed;
  }
  if (runCallbacks === void 0) {
    runCallbacks = true;
  }
  const swiper = this;
  const {
    animating,
    enabled,
    params
  } = swiper;
  if (!enabled)
    return swiper;
  let perGroup = params.slidesPerGroup;
  if (params.slidesPerView === "auto" && params.slidesPerGroup === 1 && params.slidesPerGroupAuto) {
    perGroup = Math.max(swiper.slidesPerViewDynamic("current", true), 1);
  }
  const increment = swiper.activeIndex < params.slidesPerGroupSkip ? 1 : perGroup;
  if (params.loop) {
    if (animating && params.loopPreventsSlide)
      return false;
    swiper.loopFix();
    swiper._clientLeft = swiper.$wrapperEl[0].clientLeft;
  }
  if (params.rewind && swiper.isEnd) {
    return swiper.slideTo(0, speed, runCallbacks, internal);
  }
  return swiper.slideTo(swiper.activeIndex + increment, speed, runCallbacks, internal);
}
function slidePrev(speed, runCallbacks, internal) {
  if (speed === void 0) {
    speed = this.params.speed;
  }
  if (runCallbacks === void 0) {
    runCallbacks = true;
  }
  const swiper = this;
  const {
    params,
    animating,
    snapGrid,
    slidesGrid,
    rtlTranslate,
    enabled
  } = swiper;
  if (!enabled)
    return swiper;
  if (params.loop) {
    if (animating && params.loopPreventsSlide)
      return false;
    swiper.loopFix();
    swiper._clientLeft = swiper.$wrapperEl[0].clientLeft;
  }
  const translate2 = rtlTranslate ? swiper.translate : -swiper.translate;
  function normalize(val) {
    if (val < 0)
      return -Math.floor(Math.abs(val));
    return Math.floor(val);
  }
  const normalizedTranslate = normalize(translate2);
  const normalizedSnapGrid = snapGrid.map((val) => normalize(val));
  let prevSnap = snapGrid[normalizedSnapGrid.indexOf(normalizedTranslate) - 1];
  if (typeof prevSnap === "undefined" && params.cssMode) {
    let prevSnapIndex;
    snapGrid.forEach((snap, snapIndex) => {
      if (normalizedTranslate >= snap) {
        prevSnapIndex = snapIndex;
      }
    });
    if (typeof prevSnapIndex !== "undefined") {
      prevSnap = snapGrid[prevSnapIndex > 0 ? prevSnapIndex - 1 : prevSnapIndex];
    }
  }
  let prevIndex = 0;
  if (typeof prevSnap !== "undefined") {
    prevIndex = slidesGrid.indexOf(prevSnap);
    if (prevIndex < 0)
      prevIndex = swiper.activeIndex - 1;
    if (params.slidesPerView === "auto" && params.slidesPerGroup === 1 && params.slidesPerGroupAuto) {
      prevIndex = prevIndex - swiper.slidesPerViewDynamic("previous", true) + 1;
      prevIndex = Math.max(prevIndex, 0);
    }
  }
  if (params.rewind && swiper.isBeginning) {
    const lastIndex = swiper.params.virtual && swiper.params.virtual.enabled && swiper.virtual ? swiper.virtual.slides.length - 1 : swiper.slides.length - 1;
    return swiper.slideTo(lastIndex, speed, runCallbacks, internal);
  }
  return swiper.slideTo(prevIndex, speed, runCallbacks, internal);
}
function slideReset(speed, runCallbacks, internal) {
  if (speed === void 0) {
    speed = this.params.speed;
  }
  if (runCallbacks === void 0) {
    runCallbacks = true;
  }
  const swiper = this;
  return swiper.slideTo(swiper.activeIndex, speed, runCallbacks, internal);
}
function slideToClosest(speed, runCallbacks, internal, threshold) {
  if (speed === void 0) {
    speed = this.params.speed;
  }
  if (runCallbacks === void 0) {
    runCallbacks = true;
  }
  if (threshold === void 0) {
    threshold = 0.5;
  }
  const swiper = this;
  let index2 = swiper.activeIndex;
  const skip = Math.min(swiper.params.slidesPerGroupSkip, index2);
  const snapIndex = skip + Math.floor((index2 - skip) / swiper.params.slidesPerGroup);
  const translate2 = swiper.rtlTranslate ? swiper.translate : -swiper.translate;
  if (translate2 >= swiper.snapGrid[snapIndex]) {
    const currentSnap = swiper.snapGrid[snapIndex];
    const nextSnap = swiper.snapGrid[snapIndex + 1];
    if (translate2 - currentSnap > (nextSnap - currentSnap) * threshold) {
      index2 += swiper.params.slidesPerGroup;
    }
  } else {
    const prevSnap = swiper.snapGrid[snapIndex - 1];
    const currentSnap = swiper.snapGrid[snapIndex];
    if (translate2 - prevSnap <= (currentSnap - prevSnap) * threshold) {
      index2 -= swiper.params.slidesPerGroup;
    }
  }
  index2 = Math.max(index2, 0);
  index2 = Math.min(index2, swiper.slidesGrid.length - 1);
  return swiper.slideTo(index2, speed, runCallbacks, internal);
}
function slideToClickedSlide() {
  const swiper = this;
  const {
    params,
    $wrapperEl
  } = swiper;
  const slidesPerView = params.slidesPerView === "auto" ? swiper.slidesPerViewDynamic() : params.slidesPerView;
  let slideToIndex = swiper.clickedIndex;
  let realIndex;
  if (params.loop) {
    if (swiper.animating)
      return;
    realIndex = parseInt($(swiper.clickedSlide).attr("data-swiper-slide-index"), 10);
    if (params.centeredSlides) {
      if (slideToIndex < swiper.loopedSlides - slidesPerView / 2 || slideToIndex > swiper.slides.length - swiper.loopedSlides + slidesPerView / 2) {
        swiper.loopFix();
        slideToIndex = $wrapperEl.children(`.${params.slideClass}[data-swiper-slide-index="${realIndex}"]:not(.${params.slideDuplicateClass})`).eq(0).index();
        nextTick(() => {
          swiper.slideTo(slideToIndex);
        });
      } else {
        swiper.slideTo(slideToIndex);
      }
    } else if (slideToIndex > swiper.slides.length - slidesPerView) {
      swiper.loopFix();
      slideToIndex = $wrapperEl.children(`.${params.slideClass}[data-swiper-slide-index="${realIndex}"]:not(.${params.slideDuplicateClass})`).eq(0).index();
      nextTick(() => {
        swiper.slideTo(slideToIndex);
      });
    } else {
      swiper.slideTo(slideToIndex);
    }
  } else {
    swiper.slideTo(slideToIndex);
  }
}
var slide = {
  slideTo,
  slideToLoop,
  slideNext,
  slidePrev,
  slideReset,
  slideToClosest,
  slideToClickedSlide
};
function loopCreate() {
  const swiper = this;
  const document2 = getDocument();
  const {
    params,
    $wrapperEl
  } = swiper;
  const $selector = $wrapperEl.children().length > 0 ? $($wrapperEl.children()[0].parentNode) : $wrapperEl;
  $selector.children(`.${params.slideClass}.${params.slideDuplicateClass}`).remove();
  let slides = $selector.children(`.${params.slideClass}`);
  if (params.loopFillGroupWithBlank) {
    const blankSlidesNum = params.slidesPerGroup - slides.length % params.slidesPerGroup;
    if (blankSlidesNum !== params.slidesPerGroup) {
      for (let i = 0; i < blankSlidesNum; i += 1) {
        const blankNode = $(document2.createElement("div")).addClass(`${params.slideClass} ${params.slideBlankClass}`);
        $selector.append(blankNode);
      }
      slides = $selector.children(`.${params.slideClass}`);
    }
  }
  if (params.slidesPerView === "auto" && !params.loopedSlides)
    params.loopedSlides = slides.length;
  swiper.loopedSlides = Math.ceil(parseFloat(params.loopedSlides || params.slidesPerView, 10));
  swiper.loopedSlides += params.loopAdditionalSlides;
  if (swiper.loopedSlides > slides.length) {
    swiper.loopedSlides = slides.length;
  }
  const prependSlides = [];
  const appendSlides = [];
  slides.each((el, index2) => {
    const slide2 = $(el);
    if (index2 < swiper.loopedSlides) {
      appendSlides.push(el);
    }
    if (index2 < slides.length && index2 >= slides.length - swiper.loopedSlides) {
      prependSlides.push(el);
    }
    slide2.attr("data-swiper-slide-index", index2);
  });
  for (let i = 0; i < appendSlides.length; i += 1) {
    $selector.append($(appendSlides[i].cloneNode(true)).addClass(params.slideDuplicateClass));
  }
  for (let i = prependSlides.length - 1; i >= 0; i -= 1) {
    $selector.prepend($(prependSlides[i].cloneNode(true)).addClass(params.slideDuplicateClass));
  }
}
function loopFix() {
  const swiper = this;
  swiper.emit("beforeLoopFix");
  const {
    activeIndex,
    slides,
    loopedSlides,
    allowSlidePrev,
    allowSlideNext,
    snapGrid,
    rtlTranslate: rtl
  } = swiper;
  let newIndex;
  swiper.allowSlidePrev = true;
  swiper.allowSlideNext = true;
  const snapTranslate = -snapGrid[activeIndex];
  const diff = snapTranslate - swiper.getTranslate();
  if (activeIndex < loopedSlides) {
    newIndex = slides.length - loopedSlides * 3 + activeIndex;
    newIndex += loopedSlides;
    const slideChanged = swiper.slideTo(newIndex, 0, false, true);
    if (slideChanged && diff !== 0) {
      swiper.setTranslate((rtl ? -swiper.translate : swiper.translate) - diff);
    }
  } else if (activeIndex >= slides.length - loopedSlides) {
    newIndex = -slides.length + activeIndex + loopedSlides;
    newIndex += loopedSlides;
    const slideChanged = swiper.slideTo(newIndex, 0, false, true);
    if (slideChanged && diff !== 0) {
      swiper.setTranslate((rtl ? -swiper.translate : swiper.translate) - diff);
    }
  }
  swiper.allowSlidePrev = allowSlidePrev;
  swiper.allowSlideNext = allowSlideNext;
  swiper.emit("loopFix");
}
function loopDestroy() {
  const swiper = this;
  const {
    $wrapperEl,
    params,
    slides
  } = swiper;
  $wrapperEl.children(`.${params.slideClass}.${params.slideDuplicateClass},.${params.slideClass}.${params.slideBlankClass}`).remove();
  slides.removeAttr("data-swiper-slide-index");
}
var loop = {
  loopCreate,
  loopFix,
  loopDestroy
};
function setGrabCursor(moving) {
  const swiper = this;
  if (swiper.support.touch || !swiper.params.simulateTouch || swiper.params.watchOverflow && swiper.isLocked || swiper.params.cssMode)
    return;
  const el = swiper.params.touchEventsTarget === "container" ? swiper.el : swiper.wrapperEl;
  el.style.cursor = "move";
  el.style.cursor = moving ? "-webkit-grabbing" : "-webkit-grab";
  el.style.cursor = moving ? "-moz-grabbin" : "-moz-grab";
  el.style.cursor = moving ? "grabbing" : "grab";
}
function unsetGrabCursor() {
  const swiper = this;
  if (swiper.support.touch || swiper.params.watchOverflow && swiper.isLocked || swiper.params.cssMode) {
    return;
  }
  swiper[swiper.params.touchEventsTarget === "container" ? "el" : "wrapperEl"].style.cursor = "";
}
var grabCursor = {
  setGrabCursor,
  unsetGrabCursor
};
function closestElement(selector, base) {
  if (base === void 0) {
    base = this;
  }
  function __closestFrom(el) {
    if (!el || el === getDocument() || el === getWindow())
      return null;
    if (el.assignedSlot)
      el = el.assignedSlot;
    const found = el.closest(selector);
    return found || __closestFrom(el.getRootNode().host);
  }
  return __closestFrom(base);
}
function onTouchStart(event) {
  const swiper = this;
  const document2 = getDocument();
  const window2 = getWindow();
  const data = swiper.touchEventsData;
  const {
    params,
    touches,
    enabled
  } = swiper;
  if (!enabled)
    return;
  if (swiper.animating && params.preventInteractionOnTransition) {
    return;
  }
  if (!swiper.animating && params.cssMode && params.loop) {
    swiper.loopFix();
  }
  let e = event;
  if (e.originalEvent)
    e = e.originalEvent;
  let $targetEl = $(e.target);
  if (params.touchEventsTarget === "wrapper") {
    if (!$targetEl.closest(swiper.wrapperEl).length)
      return;
  }
  data.isTouchEvent = e.type === "touchstart";
  if (!data.isTouchEvent && "which" in e && e.which === 3)
    return;
  if (!data.isTouchEvent && "button" in e && e.button > 0)
    return;
  if (data.isTouched && data.isMoved)
    return;
  const swipingClassHasValue = !!params.noSwipingClass && params.noSwipingClass !== "";
  if (swipingClassHasValue && e.target && e.target.shadowRoot && event.path && event.path[0]) {
    $targetEl = $(event.path[0]);
  }
  const noSwipingSelector = params.noSwipingSelector ? params.noSwipingSelector : `.${params.noSwipingClass}`;
  const isTargetShadow = !!(e.target && e.target.shadowRoot);
  if (params.noSwiping && (isTargetShadow ? closestElement(noSwipingSelector, e.target) : $targetEl.closest(noSwipingSelector)[0])) {
    swiper.allowClick = true;
    return;
  }
  if (params.swipeHandler) {
    if (!$targetEl.closest(params.swipeHandler)[0])
      return;
  }
  touches.currentX = e.type === "touchstart" ? e.targetTouches[0].pageX : e.pageX;
  touches.currentY = e.type === "touchstart" ? e.targetTouches[0].pageY : e.pageY;
  const startX = touches.currentX;
  const startY = touches.currentY;
  const edgeSwipeDetection = params.edgeSwipeDetection || params.iOSEdgeSwipeDetection;
  const edgeSwipeThreshold = params.edgeSwipeThreshold || params.iOSEdgeSwipeThreshold;
  if (edgeSwipeDetection && (startX <= edgeSwipeThreshold || startX >= window2.innerWidth - edgeSwipeThreshold)) {
    if (edgeSwipeDetection === "prevent") {
      event.preventDefault();
    } else {
      return;
    }
  }
  Object.assign(data, {
    isTouched: true,
    isMoved: false,
    allowTouchCallbacks: true,
    isScrolling: void 0,
    startMoving: void 0
  });
  touches.startX = startX;
  touches.startY = startY;
  data.touchStartTime = now();
  swiper.allowClick = true;
  swiper.updateSize();
  swiper.swipeDirection = void 0;
  if (params.threshold > 0)
    data.allowThresholdMove = false;
  if (e.type !== "touchstart") {
    let preventDefault = true;
    if ($targetEl.is(data.focusableElements)) {
      preventDefault = false;
      if ($targetEl[0].nodeName === "SELECT") {
        data.isTouched = false;
      }
    }
    if (document2.activeElement && $(document2.activeElement).is(data.focusableElements) && document2.activeElement !== $targetEl[0]) {
      document2.activeElement.blur();
    }
    const shouldPreventDefault = preventDefault && swiper.allowTouchMove && params.touchStartPreventDefault;
    if ((params.touchStartForcePreventDefault || shouldPreventDefault) && !$targetEl[0].isContentEditable) {
      e.preventDefault();
    }
  }
  if (swiper.params.freeMode && swiper.params.freeMode.enabled && swiper.freeMode && swiper.animating && !params.cssMode) {
    swiper.freeMode.onTouchStart();
  }
  swiper.emit("touchStart", e);
}
function onTouchMove(event) {
  const document2 = getDocument();
  const swiper = this;
  const data = swiper.touchEventsData;
  const {
    params,
    touches,
    rtlTranslate: rtl,
    enabled
  } = swiper;
  if (!enabled)
    return;
  let e = event;
  if (e.originalEvent)
    e = e.originalEvent;
  if (!data.isTouched) {
    if (data.startMoving && data.isScrolling) {
      swiper.emit("touchMoveOpposite", e);
    }
    return;
  }
  if (data.isTouchEvent && e.type !== "touchmove")
    return;
  const targetTouch = e.type === "touchmove" && e.targetTouches && (e.targetTouches[0] || e.changedTouches[0]);
  const pageX = e.type === "touchmove" ? targetTouch.pageX : e.pageX;
  const pageY = e.type === "touchmove" ? targetTouch.pageY : e.pageY;
  if (e.preventedByNestedSwiper) {
    touches.startX = pageX;
    touches.startY = pageY;
    return;
  }
  if (!swiper.allowTouchMove) {
    if (!$(e.target).is(data.focusableElements)) {
      swiper.allowClick = false;
    }
    if (data.isTouched) {
      Object.assign(touches, {
        startX: pageX,
        startY: pageY,
        currentX: pageX,
        currentY: pageY
      });
      data.touchStartTime = now();
    }
    return;
  }
  if (data.isTouchEvent && params.touchReleaseOnEdges && !params.loop) {
    if (swiper.isVertical()) {
      if (pageY < touches.startY && swiper.translate <= swiper.maxTranslate() || pageY > touches.startY && swiper.translate >= swiper.minTranslate()) {
        data.isTouched = false;
        data.isMoved = false;
        return;
      }
    } else if (pageX < touches.startX && swiper.translate <= swiper.maxTranslate() || pageX > touches.startX && swiper.translate >= swiper.minTranslate()) {
      return;
    }
  }
  if (data.isTouchEvent && document2.activeElement) {
    if (e.target === document2.activeElement && $(e.target).is(data.focusableElements)) {
      data.isMoved = true;
      swiper.allowClick = false;
      return;
    }
  }
  if (data.allowTouchCallbacks) {
    swiper.emit("touchMove", e);
  }
  if (e.targetTouches && e.targetTouches.length > 1)
    return;
  touches.currentX = pageX;
  touches.currentY = pageY;
  const diffX = touches.currentX - touches.startX;
  const diffY = touches.currentY - touches.startY;
  if (swiper.params.threshold && Math.sqrt(diffX ** 2 + diffY ** 2) < swiper.params.threshold)
    return;
  if (typeof data.isScrolling === "undefined") {
    let touchAngle;
    if (swiper.isHorizontal() && touches.currentY === touches.startY || swiper.isVertical() && touches.currentX === touches.startX) {
      data.isScrolling = false;
    } else {
      if (diffX * diffX + diffY * diffY >= 25) {
        touchAngle = Math.atan2(Math.abs(diffY), Math.abs(diffX)) * 180 / Math.PI;
        data.isScrolling = swiper.isHorizontal() ? touchAngle > params.touchAngle : 90 - touchAngle > params.touchAngle;
      }
    }
  }
  if (data.isScrolling) {
    swiper.emit("touchMoveOpposite", e);
  }
  if (typeof data.startMoving === "undefined") {
    if (touches.currentX !== touches.startX || touches.currentY !== touches.startY) {
      data.startMoving = true;
    }
  }
  if (data.isScrolling) {
    data.isTouched = false;
    return;
  }
  if (!data.startMoving) {
    return;
  }
  swiper.allowClick = false;
  if (!params.cssMode && e.cancelable) {
    e.preventDefault();
  }
  if (params.touchMoveStopPropagation && !params.nested) {
    e.stopPropagation();
  }
  if (!data.isMoved) {
    if (params.loop && !params.cssMode) {
      swiper.loopFix();
    }
    data.startTranslate = swiper.getTranslate();
    swiper.setTransition(0);
    if (swiper.animating) {
      swiper.$wrapperEl.trigger("webkitTransitionEnd transitionend");
    }
    data.allowMomentumBounce = false;
    if (params.grabCursor && (swiper.allowSlideNext === true || swiper.allowSlidePrev === true)) {
      swiper.setGrabCursor(true);
    }
    swiper.emit("sliderFirstMove", e);
  }
  swiper.emit("sliderMove", e);
  data.isMoved = true;
  let diff = swiper.isHorizontal() ? diffX : diffY;
  touches.diff = diff;
  diff *= params.touchRatio;
  if (rtl)
    diff = -diff;
  swiper.swipeDirection = diff > 0 ? "prev" : "next";
  data.currentTranslate = diff + data.startTranslate;
  let disableParentSwiper = true;
  let resistanceRatio = params.resistanceRatio;
  if (params.touchReleaseOnEdges) {
    resistanceRatio = 0;
  }
  if (diff > 0 && data.currentTranslate > swiper.minTranslate()) {
    disableParentSwiper = false;
    if (params.resistance)
      data.currentTranslate = swiper.minTranslate() - 1 + (-swiper.minTranslate() + data.startTranslate + diff) ** resistanceRatio;
  } else if (diff < 0 && data.currentTranslate < swiper.maxTranslate()) {
    disableParentSwiper = false;
    if (params.resistance)
      data.currentTranslate = swiper.maxTranslate() + 1 - (swiper.maxTranslate() - data.startTranslate - diff) ** resistanceRatio;
  }
  if (disableParentSwiper) {
    e.preventedByNestedSwiper = true;
  }
  if (!swiper.allowSlideNext && swiper.swipeDirection === "next" && data.currentTranslate < data.startTranslate) {
    data.currentTranslate = data.startTranslate;
  }
  if (!swiper.allowSlidePrev && swiper.swipeDirection === "prev" && data.currentTranslate > data.startTranslate) {
    data.currentTranslate = data.startTranslate;
  }
  if (!swiper.allowSlidePrev && !swiper.allowSlideNext) {
    data.currentTranslate = data.startTranslate;
  }
  if (params.threshold > 0) {
    if (Math.abs(diff) > params.threshold || data.allowThresholdMove) {
      if (!data.allowThresholdMove) {
        data.allowThresholdMove = true;
        touches.startX = touches.currentX;
        touches.startY = touches.currentY;
        data.currentTranslate = data.startTranslate;
        touches.diff = swiper.isHorizontal() ? touches.currentX - touches.startX : touches.currentY - touches.startY;
        return;
      }
    } else {
      data.currentTranslate = data.startTranslate;
      return;
    }
  }
  if (!params.followFinger || params.cssMode)
    return;
  if (params.freeMode && params.freeMode.enabled && swiper.freeMode || params.watchSlidesProgress) {
    swiper.updateActiveIndex();
    swiper.updateSlidesClasses();
  }
  if (swiper.params.freeMode && params.freeMode.enabled && swiper.freeMode) {
    swiper.freeMode.onTouchMove();
  }
  swiper.updateProgress(data.currentTranslate);
  swiper.setTranslate(data.currentTranslate);
}
function onTouchEnd(event) {
  const swiper = this;
  const data = swiper.touchEventsData;
  const {
    params,
    touches,
    rtlTranslate: rtl,
    slidesGrid,
    enabled
  } = swiper;
  if (!enabled)
    return;
  let e = event;
  if (e.originalEvent)
    e = e.originalEvent;
  if (data.allowTouchCallbacks) {
    swiper.emit("touchEnd", e);
  }
  data.allowTouchCallbacks = false;
  if (!data.isTouched) {
    if (data.isMoved && params.grabCursor) {
      swiper.setGrabCursor(false);
    }
    data.isMoved = false;
    data.startMoving = false;
    return;
  }
  if (params.grabCursor && data.isMoved && data.isTouched && (swiper.allowSlideNext === true || swiper.allowSlidePrev === true)) {
    swiper.setGrabCursor(false);
  }
  const touchEndTime = now();
  const timeDiff = touchEndTime - data.touchStartTime;
  if (swiper.allowClick) {
    const pathTree = e.path || e.composedPath && e.composedPath();
    swiper.updateClickedSlide(pathTree && pathTree[0] || e.target);
    swiper.emit("tap click", e);
    if (timeDiff < 300 && touchEndTime - data.lastClickTime < 300) {
      swiper.emit("doubleTap doubleClick", e);
    }
  }
  data.lastClickTime = now();
  nextTick(() => {
    if (!swiper.destroyed)
      swiper.allowClick = true;
  });
  if (!data.isTouched || !data.isMoved || !swiper.swipeDirection || touches.diff === 0 || data.currentTranslate === data.startTranslate) {
    data.isTouched = false;
    data.isMoved = false;
    data.startMoving = false;
    return;
  }
  data.isTouched = false;
  data.isMoved = false;
  data.startMoving = false;
  let currentPos;
  if (params.followFinger) {
    currentPos = rtl ? swiper.translate : -swiper.translate;
  } else {
    currentPos = -data.currentTranslate;
  }
  if (params.cssMode) {
    return;
  }
  if (swiper.params.freeMode && params.freeMode.enabled) {
    swiper.freeMode.onTouchEnd({
      currentPos
    });
    return;
  }
  let stopIndex = 0;
  let groupSize = swiper.slidesSizesGrid[0];
  for (let i = 0; i < slidesGrid.length; i += i < params.slidesPerGroupSkip ? 1 : params.slidesPerGroup) {
    const increment2 = i < params.slidesPerGroupSkip - 1 ? 1 : params.slidesPerGroup;
    if (typeof slidesGrid[i + increment2] !== "undefined") {
      if (currentPos >= slidesGrid[i] && currentPos < slidesGrid[i + increment2]) {
        stopIndex = i;
        groupSize = slidesGrid[i + increment2] - slidesGrid[i];
      }
    } else if (currentPos >= slidesGrid[i]) {
      stopIndex = i;
      groupSize = slidesGrid[slidesGrid.length - 1] - slidesGrid[slidesGrid.length - 2];
    }
  }
  let rewindFirstIndex = null;
  let rewindLastIndex = null;
  if (params.rewind) {
    if (swiper.isBeginning) {
      rewindLastIndex = swiper.params.virtual && swiper.params.virtual.enabled && swiper.virtual ? swiper.virtual.slides.length - 1 : swiper.slides.length - 1;
    } else if (swiper.isEnd) {
      rewindFirstIndex = 0;
    }
  }
  const ratio = (currentPos - slidesGrid[stopIndex]) / groupSize;
  const increment = stopIndex < params.slidesPerGroupSkip - 1 ? 1 : params.slidesPerGroup;
  if (timeDiff > params.longSwipesMs) {
    if (!params.longSwipes) {
      swiper.slideTo(swiper.activeIndex);
      return;
    }
    if (swiper.swipeDirection === "next") {
      if (ratio >= params.longSwipesRatio)
        swiper.slideTo(params.rewind && swiper.isEnd ? rewindFirstIndex : stopIndex + increment);
      else
        swiper.slideTo(stopIndex);
    }
    if (swiper.swipeDirection === "prev") {
      if (ratio > 1 - params.longSwipesRatio) {
        swiper.slideTo(stopIndex + increment);
      } else if (rewindLastIndex !== null && ratio < 0 && Math.abs(ratio) > params.longSwipesRatio) {
        swiper.slideTo(rewindLastIndex);
      } else {
        swiper.slideTo(stopIndex);
      }
    }
  } else {
    if (!params.shortSwipes) {
      swiper.slideTo(swiper.activeIndex);
      return;
    }
    const isNavButtonTarget = swiper.navigation && (e.target === swiper.navigation.nextEl || e.target === swiper.navigation.prevEl);
    if (!isNavButtonTarget) {
      if (swiper.swipeDirection === "next") {
        swiper.slideTo(rewindFirstIndex !== null ? rewindFirstIndex : stopIndex + increment);
      }
      if (swiper.swipeDirection === "prev") {
        swiper.slideTo(rewindLastIndex !== null ? rewindLastIndex : stopIndex);
      }
    } else if (e.target === swiper.navigation.nextEl) {
      swiper.slideTo(stopIndex + increment);
    } else {
      swiper.slideTo(stopIndex);
    }
  }
}
function onResize() {
  const swiper = this;
  const {
    params,
    el
  } = swiper;
  if (el && el.offsetWidth === 0)
    return;
  if (params.breakpoints) {
    swiper.setBreakpoint();
  }
  const {
    allowSlideNext,
    allowSlidePrev,
    snapGrid
  } = swiper;
  swiper.allowSlideNext = true;
  swiper.allowSlidePrev = true;
  swiper.updateSize();
  swiper.updateSlides();
  swiper.updateSlidesClasses();
  if ((params.slidesPerView === "auto" || params.slidesPerView > 1) && swiper.isEnd && !swiper.isBeginning && !swiper.params.centeredSlides) {
    swiper.slideTo(swiper.slides.length - 1, 0, false, true);
  } else {
    swiper.slideTo(swiper.activeIndex, 0, false, true);
  }
  if (swiper.autoplay && swiper.autoplay.running && swiper.autoplay.paused) {
    swiper.autoplay.run();
  }
  swiper.allowSlidePrev = allowSlidePrev;
  swiper.allowSlideNext = allowSlideNext;
  if (swiper.params.watchOverflow && snapGrid !== swiper.snapGrid) {
    swiper.checkOverflow();
  }
}
function onClick(e) {
  const swiper = this;
  if (!swiper.enabled)
    return;
  if (!swiper.allowClick) {
    if (swiper.params.preventClicks)
      e.preventDefault();
    if (swiper.params.preventClicksPropagation && swiper.animating) {
      e.stopPropagation();
      e.stopImmediatePropagation();
    }
  }
}
function onScroll() {
  const swiper = this;
  const {
    wrapperEl,
    rtlTranslate,
    enabled
  } = swiper;
  if (!enabled)
    return;
  swiper.previousTranslate = swiper.translate;
  if (swiper.isHorizontal()) {
    swiper.translate = -wrapperEl.scrollLeft;
  } else {
    swiper.translate = -wrapperEl.scrollTop;
  }
  if (swiper.translate === -0)
    swiper.translate = 0;
  swiper.updateActiveIndex();
  swiper.updateSlidesClasses();
  let newProgress;
  const translatesDiff = swiper.maxTranslate() - swiper.minTranslate();
  if (translatesDiff === 0) {
    newProgress = 0;
  } else {
    newProgress = (swiper.translate - swiper.minTranslate()) / translatesDiff;
  }
  if (newProgress !== swiper.progress) {
    swiper.updateProgress(rtlTranslate ? -swiper.translate : swiper.translate);
  }
  swiper.emit("setTranslate", swiper.translate, false);
}
let dummyEventAttached = false;
function dummyEventListener() {
}
const events = (swiper, method) => {
  const document2 = getDocument();
  const {
    params,
    touchEvents,
    el,
    wrapperEl,
    device,
    support: support2
  } = swiper;
  const capture = !!params.nested;
  const domMethod = method === "on" ? "addEventListener" : "removeEventListener";
  const swiperMethod = method;
  if (!support2.touch) {
    el[domMethod](touchEvents.start, swiper.onTouchStart, false);
    document2[domMethod](touchEvents.move, swiper.onTouchMove, capture);
    document2[domMethod](touchEvents.end, swiper.onTouchEnd, false);
  } else {
    const passiveListener = touchEvents.start === "touchstart" && support2.passiveListener && params.passiveListeners ? {
      passive: true,
      capture: false
    } : false;
    el[domMethod](touchEvents.start, swiper.onTouchStart, passiveListener);
    el[domMethod](touchEvents.move, swiper.onTouchMove, support2.passiveListener ? {
      passive: false,
      capture
    } : capture);
    el[domMethod](touchEvents.end, swiper.onTouchEnd, passiveListener);
    if (touchEvents.cancel) {
      el[domMethod](touchEvents.cancel, swiper.onTouchEnd, passiveListener);
    }
  }
  if (params.preventClicks || params.preventClicksPropagation) {
    el[domMethod]("click", swiper.onClick, true);
  }
  if (params.cssMode) {
    wrapperEl[domMethod]("scroll", swiper.onScroll);
  }
  if (params.updateOnWindowResize) {
    swiper[swiperMethod](device.ios || device.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", onResize, true);
  } else {
    swiper[swiperMethod]("observerUpdate", onResize, true);
  }
};
function attachEvents() {
  const swiper = this;
  const document2 = getDocument();
  const {
    params,
    support: support2
  } = swiper;
  swiper.onTouchStart = onTouchStart.bind(swiper);
  swiper.onTouchMove = onTouchMove.bind(swiper);
  swiper.onTouchEnd = onTouchEnd.bind(swiper);
  if (params.cssMode) {
    swiper.onScroll = onScroll.bind(swiper);
  }
  swiper.onClick = onClick.bind(swiper);
  if (support2.touch && !dummyEventAttached) {
    document2.addEventListener("touchstart", dummyEventListener);
    dummyEventAttached = true;
  }
  events(swiper, "on");
}
function detachEvents() {
  const swiper = this;
  events(swiper, "off");
}
var events$1 = {
  attachEvents,
  detachEvents
};
const isGridEnabled = (swiper, params) => {
  return swiper.grid && params.grid && params.grid.rows > 1;
};
function setBreakpoint() {
  const swiper = this;
  const {
    activeIndex,
    initialized,
    loopedSlides = 0,
    params,
    $el
  } = swiper;
  const breakpoints2 = params.breakpoints;
  if (!breakpoints2 || breakpoints2 && Object.keys(breakpoints2).length === 0)
    return;
  const breakpoint = swiper.getBreakpoint(breakpoints2, swiper.params.breakpointsBase, swiper.el);
  if (!breakpoint || swiper.currentBreakpoint === breakpoint)
    return;
  const breakpointOnlyParams = breakpoint in breakpoints2 ? breakpoints2[breakpoint] : void 0;
  const breakpointParams = breakpointOnlyParams || swiper.originalParams;
  const wasMultiRow = isGridEnabled(swiper, params);
  const isMultiRow = isGridEnabled(swiper, breakpointParams);
  const wasEnabled = params.enabled;
  if (wasMultiRow && !isMultiRow) {
    $el.removeClass(`${params.containerModifierClass}grid ${params.containerModifierClass}grid-column`);
    swiper.emitContainerClasses();
  } else if (!wasMultiRow && isMultiRow) {
    $el.addClass(`${params.containerModifierClass}grid`);
    if (breakpointParams.grid.fill && breakpointParams.grid.fill === "column" || !breakpointParams.grid.fill && params.grid.fill === "column") {
      $el.addClass(`${params.containerModifierClass}grid-column`);
    }
    swiper.emitContainerClasses();
  }
  const directionChanged = breakpointParams.direction && breakpointParams.direction !== params.direction;
  const needsReLoop = params.loop && (breakpointParams.slidesPerView !== params.slidesPerView || directionChanged);
  if (directionChanged && initialized) {
    swiper.changeDirection();
  }
  extend(swiper.params, breakpointParams);
  const isEnabled = swiper.params.enabled;
  Object.assign(swiper, {
    allowTouchMove: swiper.params.allowTouchMove,
    allowSlideNext: swiper.params.allowSlideNext,
    allowSlidePrev: swiper.params.allowSlidePrev
  });
  if (wasEnabled && !isEnabled) {
    swiper.disable();
  } else if (!wasEnabled && isEnabled) {
    swiper.enable();
  }
  swiper.currentBreakpoint = breakpoint;
  swiper.emit("_beforeBreakpoint", breakpointParams);
  if (needsReLoop && initialized) {
    swiper.loopDestroy();
    swiper.loopCreate();
    swiper.updateSlides();
    swiper.slideTo(activeIndex - loopedSlides + swiper.loopedSlides, 0, false);
  }
  swiper.emit("breakpoint", breakpointParams);
}
function getBreakpoint(breakpoints2, base, containerEl) {
  if (base === void 0) {
    base = "window";
  }
  if (!breakpoints2 || base === "container" && !containerEl)
    return void 0;
  let breakpoint = false;
  const window2 = getWindow();
  const currentHeight = base === "window" ? window2.innerHeight : containerEl.clientHeight;
  const points = Object.keys(breakpoints2).map((point) => {
    if (typeof point === "string" && point.indexOf("@") === 0) {
      const minRatio = parseFloat(point.substr(1));
      const value = currentHeight * minRatio;
      return {
        value,
        point
      };
    }
    return {
      value: point,
      point
    };
  });
  points.sort((a, b) => parseInt(a.value, 10) - parseInt(b.value, 10));
  for (let i = 0; i < points.length; i += 1) {
    const {
      point,
      value
    } = points[i];
    if (base === "window") {
      if (window2.matchMedia(`(min-width: ${value}px)`).matches) {
        breakpoint = point;
      }
    } else if (value <= containerEl.clientWidth) {
      breakpoint = point;
    }
  }
  return breakpoint || "max";
}
var breakpoints = {
  setBreakpoint,
  getBreakpoint
};
function prepareClasses(entries, prefix) {
  const resultClasses = [];
  entries.forEach((item) => {
    if (typeof item === "object") {
      Object.keys(item).forEach((classNames) => {
        if (item[classNames]) {
          resultClasses.push(prefix + classNames);
        }
      });
    } else if (typeof item === "string") {
      resultClasses.push(prefix + item);
    }
  });
  return resultClasses;
}
function addClasses() {
  const swiper = this;
  const {
    classNames,
    params,
    rtl,
    $el,
    device,
    support: support2
  } = swiper;
  const suffixes = prepareClasses(["initialized", params.direction, {
    "pointer-events": !support2.touch
  }, {
    "free-mode": swiper.params.freeMode && params.freeMode.enabled
  }, {
    "autoheight": params.autoHeight
  }, {
    "rtl": rtl
  }, {
    "grid": params.grid && params.grid.rows > 1
  }, {
    "grid-column": params.grid && params.grid.rows > 1 && params.grid.fill === "column"
  }, {
    "android": device.android
  }, {
    "ios": device.ios
  }, {
    "css-mode": params.cssMode
  }, {
    "centered": params.cssMode && params.centeredSlides
  }], params.containerModifierClass);
  classNames.push(...suffixes);
  $el.addClass([...classNames].join(" "));
  swiper.emitContainerClasses();
}
function removeClasses() {
  const swiper = this;
  const {
    $el,
    classNames
  } = swiper;
  $el.removeClass(classNames.join(" "));
  swiper.emitContainerClasses();
}
var classes = {
  addClasses,
  removeClasses
};
function loadImage(imageEl, src, srcset, sizes, checkForComplete, callback) {
  const window2 = getWindow();
  let image;
  function onReady() {
    if (callback)
      callback();
  }
  const isPicture = $(imageEl).parent("picture")[0];
  if (!isPicture && (!imageEl.complete || !checkForComplete)) {
    if (src) {
      image = new window2.Image();
      image.onload = onReady;
      image.onerror = onReady;
      if (sizes) {
        image.sizes = sizes;
      }
      if (srcset) {
        image.srcset = srcset;
      }
      if (src) {
        image.src = src;
      }
    } else {
      onReady();
    }
  } else {
    onReady();
  }
}
function preloadImages() {
  const swiper = this;
  swiper.imagesToLoad = swiper.$el.find("img");
  function onReady() {
    if (typeof swiper === "undefined" || swiper === null || !swiper || swiper.destroyed)
      return;
    if (swiper.imagesLoaded !== void 0)
      swiper.imagesLoaded += 1;
    if (swiper.imagesLoaded === swiper.imagesToLoad.length) {
      if (swiper.params.updateOnImagesReady)
        swiper.update();
      swiper.emit("imagesReady");
    }
  }
  for (let i = 0; i < swiper.imagesToLoad.length; i += 1) {
    const imageEl = swiper.imagesToLoad[i];
    swiper.loadImage(imageEl, imageEl.currentSrc || imageEl.getAttribute("src"), imageEl.srcset || imageEl.getAttribute("srcset"), imageEl.sizes || imageEl.getAttribute("sizes"), true, onReady);
  }
}
var images = {
  loadImage,
  preloadImages
};
function checkOverflow() {
  const swiper = this;
  const {
    isLocked: wasLocked,
    params
  } = swiper;
  const {
    slidesOffsetBefore
  } = params;
  if (slidesOffsetBefore) {
    const lastSlideIndex = swiper.slides.length - 1;
    const lastSlideRightEdge = swiper.slidesGrid[lastSlideIndex] + swiper.slidesSizesGrid[lastSlideIndex] + slidesOffsetBefore * 2;
    swiper.isLocked = swiper.size > lastSlideRightEdge;
  } else {
    swiper.isLocked = swiper.snapGrid.length === 1;
  }
  if (params.allowSlideNext === true) {
    swiper.allowSlideNext = !swiper.isLocked;
  }
  if (params.allowSlidePrev === true) {
    swiper.allowSlidePrev = !swiper.isLocked;
  }
  if (wasLocked && wasLocked !== swiper.isLocked) {
    swiper.isEnd = false;
  }
  if (wasLocked !== swiper.isLocked) {
    swiper.emit(swiper.isLocked ? "lock" : "unlock");
  }
}
var checkOverflow$1 = {
  checkOverflow
};
var defaults = {
  init: true,
  direction: "horizontal",
  touchEventsTarget: "wrapper",
  initialSlide: 0,
  speed: 300,
  cssMode: false,
  updateOnWindowResize: true,
  resizeObserver: true,
  nested: false,
  createElements: false,
  enabled: true,
  focusableElements: "input, select, option, textarea, button, video, label",
  width: null,
  height: null,
  preventInteractionOnTransition: false,
  userAgent: null,
  url: null,
  edgeSwipeDetection: false,
  edgeSwipeThreshold: 20,
  autoHeight: false,
  setWrapperSize: false,
  virtualTranslate: false,
  effect: "slide",
  breakpoints: void 0,
  breakpointsBase: "window",
  spaceBetween: 0,
  slidesPerView: 1,
  slidesPerGroup: 1,
  slidesPerGroupSkip: 0,
  slidesPerGroupAuto: false,
  centeredSlides: false,
  centeredSlidesBounds: false,
  slidesOffsetBefore: 0,
  slidesOffsetAfter: 0,
  normalizeSlideIndex: true,
  centerInsufficientSlides: false,
  watchOverflow: true,
  roundLengths: false,
  touchRatio: 1,
  touchAngle: 45,
  simulateTouch: true,
  shortSwipes: true,
  longSwipes: true,
  longSwipesRatio: 0.5,
  longSwipesMs: 300,
  followFinger: true,
  allowTouchMove: true,
  threshold: 0,
  touchMoveStopPropagation: false,
  touchStartPreventDefault: true,
  touchStartForcePreventDefault: false,
  touchReleaseOnEdges: false,
  uniqueNavElements: true,
  resistance: true,
  resistanceRatio: 0.85,
  watchSlidesProgress: false,
  grabCursor: false,
  preventClicks: true,
  preventClicksPropagation: true,
  slideToClickedSlide: false,
  preloadImages: true,
  updateOnImagesReady: true,
  loop: false,
  loopAdditionalSlides: 0,
  loopedSlides: null,
  loopFillGroupWithBlank: false,
  loopPreventsSlide: true,
  rewind: false,
  allowSlidePrev: true,
  allowSlideNext: true,
  swipeHandler: null,
  noSwiping: true,
  noSwipingClass: "swiper-no-swiping",
  noSwipingSelector: null,
  passiveListeners: true,
  maxBackfaceHiddenSlides: 10,
  containerModifierClass: "swiper-",
  slideClass: "swiper-slide",
  slideBlankClass: "swiper-slide-invisible-blank",
  slideActiveClass: "swiper-slide-active",
  slideDuplicateActiveClass: "swiper-slide-duplicate-active",
  slideVisibleClass: "swiper-slide-visible",
  slideDuplicateClass: "swiper-slide-duplicate",
  slideNextClass: "swiper-slide-next",
  slideDuplicateNextClass: "swiper-slide-duplicate-next",
  slidePrevClass: "swiper-slide-prev",
  slideDuplicatePrevClass: "swiper-slide-duplicate-prev",
  wrapperClass: "swiper-wrapper",
  runCallbacksOnInit: true,
  _emitClasses: false
};
function moduleExtendParams(params, allModulesParams) {
  return function extendParams(obj) {
    if (obj === void 0) {
      obj = {};
    }
    const moduleParamName = Object.keys(obj)[0];
    const moduleParams = obj[moduleParamName];
    if (typeof moduleParams !== "object" || moduleParams === null) {
      extend(allModulesParams, obj);
      return;
    }
    if (["navigation", "pagination", "scrollbar"].indexOf(moduleParamName) >= 0 && params[moduleParamName] === true) {
      params[moduleParamName] = {
        auto: true
      };
    }
    if (!(moduleParamName in params && "enabled" in moduleParams)) {
      extend(allModulesParams, obj);
      return;
    }
    if (params[moduleParamName] === true) {
      params[moduleParamName] = {
        enabled: true
      };
    }
    if (typeof params[moduleParamName] === "object" && !("enabled" in params[moduleParamName])) {
      params[moduleParamName].enabled = true;
    }
    if (!params[moduleParamName])
      params[moduleParamName] = {
        enabled: false
      };
    extend(allModulesParams, obj);
  };
}
const prototypes = {
  eventsEmitter,
  update,
  translate,
  transition,
  slide,
  loop,
  grabCursor,
  events: events$1,
  breakpoints,
  checkOverflow: checkOverflow$1,
  classes,
  images
};
const extendedDefaults = {};
class Swiper {
  constructor() {
    let el;
    let params;
    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }
    if (args.length === 1 && args[0].constructor && Object.prototype.toString.call(args[0]).slice(8, -1) === "Object") {
      params = args[0];
    } else {
      [el, params] = args;
    }
    if (!params)
      params = {};
    params = extend({}, params);
    if (el && !params.el)
      params.el = el;
    if (params.el && $(params.el).length > 1) {
      const swipers = [];
      $(params.el).each((containerEl) => {
        const newParams = extend({}, params, {
          el: containerEl
        });
        swipers.push(new Swiper(newParams));
      });
      return swipers;
    }
    const swiper = this;
    swiper.__swiper__ = true;
    swiper.support = getSupport();
    swiper.device = getDevice({
      userAgent: params.userAgent
    });
    swiper.browser = getBrowser();
    swiper.eventsListeners = {};
    swiper.eventsAnyListeners = [];
    swiper.modules = [...swiper.__modules__];
    if (params.modules && Array.isArray(params.modules)) {
      swiper.modules.push(...params.modules);
    }
    const allModulesParams = {};
    swiper.modules.forEach((mod) => {
      mod({
        swiper,
        extendParams: moduleExtendParams(params, allModulesParams),
        on: swiper.on.bind(swiper),
        once: swiper.once.bind(swiper),
        off: swiper.off.bind(swiper),
        emit: swiper.emit.bind(swiper)
      });
    });
    const swiperParams = extend({}, defaults, allModulesParams);
    swiper.params = extend({}, swiperParams, extendedDefaults, params);
    swiper.originalParams = extend({}, swiper.params);
    swiper.passedParams = extend({}, params);
    if (swiper.params && swiper.params.on) {
      Object.keys(swiper.params.on).forEach((eventName) => {
        swiper.on(eventName, swiper.params.on[eventName]);
      });
    }
    if (swiper.params && swiper.params.onAny) {
      swiper.onAny(swiper.params.onAny);
    }
    swiper.$ = $;
    Object.assign(swiper, {
      enabled: swiper.params.enabled,
      el,
      classNames: [],
      slides: $(),
      slidesGrid: [],
      snapGrid: [],
      slidesSizesGrid: [],
      isHorizontal() {
        return swiper.params.direction === "horizontal";
      },
      isVertical() {
        return swiper.params.direction === "vertical";
      },
      activeIndex: 0,
      realIndex: 0,
      isBeginning: true,
      isEnd: false,
      translate: 0,
      previousTranslate: 0,
      progress: 0,
      velocity: 0,
      animating: false,
      allowSlideNext: swiper.params.allowSlideNext,
      allowSlidePrev: swiper.params.allowSlidePrev,
      touchEvents: function touchEvents() {
        const touch = ["touchstart", "touchmove", "touchend", "touchcancel"];
        const desktop = ["pointerdown", "pointermove", "pointerup"];
        swiper.touchEventsTouch = {
          start: touch[0],
          move: touch[1],
          end: touch[2],
          cancel: touch[3]
        };
        swiper.touchEventsDesktop = {
          start: desktop[0],
          move: desktop[1],
          end: desktop[2]
        };
        return swiper.support.touch || !swiper.params.simulateTouch ? swiper.touchEventsTouch : swiper.touchEventsDesktop;
      }(),
      touchEventsData: {
        isTouched: void 0,
        isMoved: void 0,
        allowTouchCallbacks: void 0,
        touchStartTime: void 0,
        isScrolling: void 0,
        currentTranslate: void 0,
        startTranslate: void 0,
        allowThresholdMove: void 0,
        focusableElements: swiper.params.focusableElements,
        lastClickTime: now(),
        clickTimeout: void 0,
        velocities: [],
        allowMomentumBounce: void 0,
        isTouchEvent: void 0,
        startMoving: void 0
      },
      allowClick: true,
      allowTouchMove: swiper.params.allowTouchMove,
      touches: {
        startX: 0,
        startY: 0,
        currentX: 0,
        currentY: 0,
        diff: 0
      },
      imagesToLoad: [],
      imagesLoaded: 0
    });
    swiper.emit("_swiper");
    if (swiper.params.init) {
      swiper.init();
    }
    return swiper;
  }
  enable() {
    const swiper = this;
    if (swiper.enabled)
      return;
    swiper.enabled = true;
    if (swiper.params.grabCursor) {
      swiper.setGrabCursor();
    }
    swiper.emit("enable");
  }
  disable() {
    const swiper = this;
    if (!swiper.enabled)
      return;
    swiper.enabled = false;
    if (swiper.params.grabCursor) {
      swiper.unsetGrabCursor();
    }
    swiper.emit("disable");
  }
  setProgress(progress, speed) {
    const swiper = this;
    progress = Math.min(Math.max(progress, 0), 1);
    const min = swiper.minTranslate();
    const max = swiper.maxTranslate();
    const current = (max - min) * progress + min;
    swiper.translateTo(current, typeof speed === "undefined" ? 0 : speed);
    swiper.updateActiveIndex();
    swiper.updateSlidesClasses();
  }
  emitContainerClasses() {
    const swiper = this;
    if (!swiper.params._emitClasses || !swiper.el)
      return;
    const cls = swiper.el.className.split(" ").filter((className) => {
      return className.indexOf("swiper") === 0 || className.indexOf(swiper.params.containerModifierClass) === 0;
    });
    swiper.emit("_containerClasses", cls.join(" "));
  }
  getSlideClasses(slideEl) {
    const swiper = this;
    return slideEl.className.split(" ").filter((className) => {
      return className.indexOf("swiper-slide") === 0 || className.indexOf(swiper.params.slideClass) === 0;
    }).join(" ");
  }
  emitSlidesClasses() {
    const swiper = this;
    if (!swiper.params._emitClasses || !swiper.el)
      return;
    const updates = [];
    swiper.slides.each((slideEl) => {
      const classNames = swiper.getSlideClasses(slideEl);
      updates.push({
        slideEl,
        classNames
      });
      swiper.emit("_slideClass", slideEl, classNames);
    });
    swiper.emit("_slideClasses", updates);
  }
  slidesPerViewDynamic(view, exact) {
    if (view === void 0) {
      view = "current";
    }
    if (exact === void 0) {
      exact = false;
    }
    const swiper = this;
    const {
      params,
      slides,
      slidesGrid,
      slidesSizesGrid,
      size: swiperSize,
      activeIndex
    } = swiper;
    let spv = 1;
    if (params.centeredSlides) {
      let slideSize = slides[activeIndex].swiperSlideSize;
      let breakLoop;
      for (let i = activeIndex + 1; i < slides.length; i += 1) {
        if (slides[i] && !breakLoop) {
          slideSize += slides[i].swiperSlideSize;
          spv += 1;
          if (slideSize > swiperSize)
            breakLoop = true;
        }
      }
      for (let i = activeIndex - 1; i >= 0; i -= 1) {
        if (slides[i] && !breakLoop) {
          slideSize += slides[i].swiperSlideSize;
          spv += 1;
          if (slideSize > swiperSize)
            breakLoop = true;
        }
      }
    } else {
      if (view === "current") {
        for (let i = activeIndex + 1; i < slides.length; i += 1) {
          const slideInView = exact ? slidesGrid[i] + slidesSizesGrid[i] - slidesGrid[activeIndex] < swiperSize : slidesGrid[i] - slidesGrid[activeIndex] < swiperSize;
          if (slideInView) {
            spv += 1;
          }
        }
      } else {
        for (let i = activeIndex - 1; i >= 0; i -= 1) {
          const slideInView = slidesGrid[activeIndex] - slidesGrid[i] < swiperSize;
          if (slideInView) {
            spv += 1;
          }
        }
      }
    }
    return spv;
  }
  update() {
    const swiper = this;
    if (!swiper || swiper.destroyed)
      return;
    const {
      snapGrid,
      params
    } = swiper;
    if (params.breakpoints) {
      swiper.setBreakpoint();
    }
    swiper.updateSize();
    swiper.updateSlides();
    swiper.updateProgress();
    swiper.updateSlidesClasses();
    function setTranslate2() {
      const translateValue = swiper.rtlTranslate ? swiper.translate * -1 : swiper.translate;
      const newTranslate = Math.min(Math.max(translateValue, swiper.maxTranslate()), swiper.minTranslate());
      swiper.setTranslate(newTranslate);
      swiper.updateActiveIndex();
      swiper.updateSlidesClasses();
    }
    let translated;
    if (swiper.params.freeMode && swiper.params.freeMode.enabled) {
      setTranslate2();
      if (swiper.params.autoHeight) {
        swiper.updateAutoHeight();
      }
    } else {
      if ((swiper.params.slidesPerView === "auto" || swiper.params.slidesPerView > 1) && swiper.isEnd && !swiper.params.centeredSlides) {
        translated = swiper.slideTo(swiper.slides.length - 1, 0, false, true);
      } else {
        translated = swiper.slideTo(swiper.activeIndex, 0, false, true);
      }
      if (!translated) {
        setTranslate2();
      }
    }
    if (params.watchOverflow && snapGrid !== swiper.snapGrid) {
      swiper.checkOverflow();
    }
    swiper.emit("update");
  }
  changeDirection(newDirection, needUpdate) {
    if (needUpdate === void 0) {
      needUpdate = true;
    }
    const swiper = this;
    const currentDirection = swiper.params.direction;
    if (!newDirection) {
      newDirection = currentDirection === "horizontal" ? "vertical" : "horizontal";
    }
    if (newDirection === currentDirection || newDirection !== "horizontal" && newDirection !== "vertical") {
      return swiper;
    }
    swiper.$el.removeClass(`${swiper.params.containerModifierClass}${currentDirection}`).addClass(`${swiper.params.containerModifierClass}${newDirection}`);
    swiper.emitContainerClasses();
    swiper.params.direction = newDirection;
    swiper.slides.each((slideEl) => {
      if (newDirection === "vertical") {
        slideEl.style.width = "";
      } else {
        slideEl.style.height = "";
      }
    });
    swiper.emit("changeDirection");
    if (needUpdate)
      swiper.update();
    return swiper;
  }
  mount(el) {
    const swiper = this;
    if (swiper.mounted)
      return true;
    const $el = $(el || swiper.params.el);
    el = $el[0];
    if (!el) {
      return false;
    }
    el.swiper = swiper;
    const getWrapperSelector = () => {
      return `.${(swiper.params.wrapperClass || "").trim().split(" ").join(".")}`;
    };
    const getWrapper = () => {
      if (el && el.shadowRoot && el.shadowRoot.querySelector) {
        const res = $(el.shadowRoot.querySelector(getWrapperSelector()));
        res.children = (options) => $el.children(options);
        return res;
      }
      return $el.children(getWrapperSelector());
    };
    let $wrapperEl = getWrapper();
    if ($wrapperEl.length === 0 && swiper.params.createElements) {
      const document2 = getDocument();
      const wrapper = document2.createElement("div");
      $wrapperEl = $(wrapper);
      wrapper.className = swiper.params.wrapperClass;
      $el.append(wrapper);
      $el.children(`.${swiper.params.slideClass}`).each((slideEl) => {
        $wrapperEl.append(slideEl);
      });
    }
    Object.assign(swiper, {
      $el,
      el,
      $wrapperEl,
      wrapperEl: $wrapperEl[0],
      mounted: true,
      rtl: el.dir.toLowerCase() === "rtl" || $el.css("direction") === "rtl",
      rtlTranslate: swiper.params.direction === "horizontal" && (el.dir.toLowerCase() === "rtl" || $el.css("direction") === "rtl"),
      wrongRTL: $wrapperEl.css("display") === "-webkit-box"
    });
    return true;
  }
  init(el) {
    const swiper = this;
    if (swiper.initialized)
      return swiper;
    const mounted = swiper.mount(el);
    if (mounted === false)
      return swiper;
    swiper.emit("beforeInit");
    if (swiper.params.breakpoints) {
      swiper.setBreakpoint();
    }
    swiper.addClasses();
    if (swiper.params.loop) {
      swiper.loopCreate();
    }
    swiper.updateSize();
    swiper.updateSlides();
    if (swiper.params.watchOverflow) {
      swiper.checkOverflow();
    }
    if (swiper.params.grabCursor && swiper.enabled) {
      swiper.setGrabCursor();
    }
    if (swiper.params.preloadImages) {
      swiper.preloadImages();
    }
    if (swiper.params.loop) {
      swiper.slideTo(swiper.params.initialSlide + swiper.loopedSlides, 0, swiper.params.runCallbacksOnInit, false, true);
    } else {
      swiper.slideTo(swiper.params.initialSlide, 0, swiper.params.runCallbacksOnInit, false, true);
    }
    swiper.attachEvents();
    swiper.initialized = true;
    swiper.emit("init");
    swiper.emit("afterInit");
    return swiper;
  }
  destroy(deleteInstance, cleanStyles) {
    if (deleteInstance === void 0) {
      deleteInstance = true;
    }
    if (cleanStyles === void 0) {
      cleanStyles = true;
    }
    const swiper = this;
    const {
      params,
      $el,
      $wrapperEl,
      slides
    } = swiper;
    if (typeof swiper.params === "undefined" || swiper.destroyed) {
      return null;
    }
    swiper.emit("beforeDestroy");
    swiper.initialized = false;
    swiper.detachEvents();
    if (params.loop) {
      swiper.loopDestroy();
    }
    if (cleanStyles) {
      swiper.removeClasses();
      $el.removeAttr("style");
      $wrapperEl.removeAttr("style");
      if (slides && slides.length) {
        slides.removeClass([params.slideVisibleClass, params.slideActiveClass, params.slideNextClass, params.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-slide-index");
      }
    }
    swiper.emit("destroy");
    Object.keys(swiper.eventsListeners).forEach((eventName) => {
      swiper.off(eventName);
    });
    if (deleteInstance !== false) {
      swiper.$el[0].swiper = null;
      deleteProps(swiper);
    }
    swiper.destroyed = true;
    return null;
  }
  static extendDefaults(newDefaults) {
    extend(extendedDefaults, newDefaults);
  }
  static get extendedDefaults() {
    return extendedDefaults;
  }
  static get defaults() {
    return defaults;
  }
  static installModule(mod) {
    if (!Swiper.prototype.__modules__)
      Swiper.prototype.__modules__ = [];
    const modules = Swiper.prototype.__modules__;
    if (typeof mod === "function" && modules.indexOf(mod) < 0) {
      modules.push(mod);
    }
  }
  static use(module) {
    if (Array.isArray(module)) {
      module.forEach((m) => Swiper.installModule(m));
      return Swiper;
    }
    Swiper.installModule(module);
    return Swiper;
  }
}
Object.keys(prototypes).forEach((prototypeGroup) => {
  Object.keys(prototypes[prototypeGroup]).forEach((protoMethod) => {
    Swiper.prototype[protoMethod] = prototypes[prototypeGroup][protoMethod];
  });
});
Swiper.use([Resize, Observer]);
function createElementIfNotDefined(swiper, originalParams, params, checkProps) {
  const document2 = getDocument();
  if (swiper.params.createElements) {
    Object.keys(checkProps).forEach((key) => {
      if (!params[key] && params.auto === true) {
        let element = swiper.$el.children(`.${checkProps[key]}`)[0];
        if (!element) {
          element = document2.createElement("div");
          element.className = checkProps[key];
          swiper.$el.append(element);
        }
        params[key] = element;
        originalParams[key] = element;
      }
    });
  }
  return params;
}
function Navigation(_ref) {
  let {
    swiper,
    extendParams,
    on: on2,
    emit
  } = _ref;
  extendParams({
    navigation: {
      nextEl: null,
      prevEl: null,
      hideOnClick: false,
      disabledClass: "swiper-button-disabled",
      hiddenClass: "swiper-button-hidden",
      lockClass: "swiper-button-lock"
    }
  });
  swiper.navigation = {
    nextEl: null,
    $nextEl: null,
    prevEl: null,
    $prevEl: null
  };
  function getEl(el) {
    let $el;
    if (el) {
      $el = $(el);
      if (swiper.params.uniqueNavElements && typeof el === "string" && $el.length > 1 && swiper.$el.find(el).length === 1) {
        $el = swiper.$el.find(el);
      }
    }
    return $el;
  }
  function toggleEl($el, disabled) {
    const params = swiper.params.navigation;
    if ($el && $el.length > 0) {
      $el[disabled ? "addClass" : "removeClass"](params.disabledClass);
      if ($el[0] && $el[0].tagName === "BUTTON")
        $el[0].disabled = disabled;
      if (swiper.params.watchOverflow && swiper.enabled) {
        $el[swiper.isLocked ? "addClass" : "removeClass"](params.lockClass);
      }
    }
  }
  function update2() {
    if (swiper.params.loop)
      return;
    const {
      $nextEl,
      $prevEl
    } = swiper.navigation;
    toggleEl($prevEl, swiper.isBeginning && !swiper.params.rewind);
    toggleEl($nextEl, swiper.isEnd && !swiper.params.rewind);
  }
  function onPrevClick(e) {
    e.preventDefault();
    if (swiper.isBeginning && !swiper.params.loop && !swiper.params.rewind)
      return;
    swiper.slidePrev();
  }
  function onNextClick(e) {
    e.preventDefault();
    if (swiper.isEnd && !swiper.params.loop && !swiper.params.rewind)
      return;
    swiper.slideNext();
  }
  function init() {
    const params = swiper.params.navigation;
    swiper.params.navigation = createElementIfNotDefined(swiper, swiper.originalParams.navigation, swiper.params.navigation, {
      nextEl: "swiper-button-next",
      prevEl: "swiper-button-prev"
    });
    if (!(params.nextEl || params.prevEl))
      return;
    const $nextEl = getEl(params.nextEl);
    const $prevEl = getEl(params.prevEl);
    if ($nextEl && $nextEl.length > 0) {
      $nextEl.on("click", onNextClick);
    }
    if ($prevEl && $prevEl.length > 0) {
      $prevEl.on("click", onPrevClick);
    }
    Object.assign(swiper.navigation, {
      $nextEl,
      nextEl: $nextEl && $nextEl[0],
      $prevEl,
      prevEl: $prevEl && $prevEl[0]
    });
    if (!swiper.enabled) {
      if ($nextEl)
        $nextEl.addClass(params.lockClass);
      if ($prevEl)
        $prevEl.addClass(params.lockClass);
    }
  }
  function destroy() {
    const {
      $nextEl,
      $prevEl
    } = swiper.navigation;
    if ($nextEl && $nextEl.length) {
      $nextEl.off("click", onNextClick);
      $nextEl.removeClass(swiper.params.navigation.disabledClass);
    }
    if ($prevEl && $prevEl.length) {
      $prevEl.off("click", onPrevClick);
      $prevEl.removeClass(swiper.params.navigation.disabledClass);
    }
  }
  on2("init", () => {
    init();
    update2();
  });
  on2("toEdge fromEdge lock unlock", () => {
    update2();
  });
  on2("destroy", () => {
    destroy();
  });
  on2("enable disable", () => {
    const {
      $nextEl,
      $prevEl
    } = swiper.navigation;
    if ($nextEl) {
      $nextEl[swiper.enabled ? "removeClass" : "addClass"](swiper.params.navigation.lockClass);
    }
    if ($prevEl) {
      $prevEl[swiper.enabled ? "removeClass" : "addClass"](swiper.params.navigation.lockClass);
    }
  });
  on2("click", (_s, e) => {
    const {
      $nextEl,
      $prevEl
    } = swiper.navigation;
    const targetEl = e.target;
    if (swiper.params.navigation.hideOnClick && !$(targetEl).is($prevEl) && !$(targetEl).is($nextEl)) {
      if (swiper.pagination && swiper.params.pagination && swiper.params.pagination.clickable && (swiper.pagination.el === targetEl || swiper.pagination.el.contains(targetEl)))
        return;
      let isHidden;
      if ($nextEl) {
        isHidden = $nextEl.hasClass(swiper.params.navigation.hiddenClass);
      } else if ($prevEl) {
        isHidden = $prevEl.hasClass(swiper.params.navigation.hiddenClass);
      }
      if (isHidden === true) {
        emit("navigationShow");
      } else {
        emit("navigationHide");
      }
      if ($nextEl) {
        $nextEl.toggleClass(swiper.params.navigation.hiddenClass);
      }
      if ($prevEl) {
        $prevEl.toggleClass(swiper.params.navigation.hiddenClass);
      }
    }
  });
  Object.assign(swiper.navigation, {
    update: update2,
    init,
    destroy
  });
}
function classesToSelector(classes2) {
  if (classes2 === void 0) {
    classes2 = "";
  }
  return `.${classes2.trim().replace(/([\.:!\/])/g, "\\$1").replace(/ /g, ".")}`;
}
function Pagination(_ref) {
  let {
    swiper,
    extendParams,
    on: on2,
    emit
  } = _ref;
  const pfx = "swiper-pagination";
  extendParams({
    pagination: {
      el: null,
      bulletElement: "span",
      clickable: false,
      hideOnClick: false,
      renderBullet: null,
      renderProgressbar: null,
      renderFraction: null,
      renderCustom: null,
      progressbarOpposite: false,
      type: "bullets",
      dynamicBullets: false,
      dynamicMainBullets: 1,
      formatFractionCurrent: (number) => number,
      formatFractionTotal: (number) => number,
      bulletClass: `${pfx}-bullet`,
      bulletActiveClass: `${pfx}-bullet-active`,
      modifierClass: `${pfx}-`,
      currentClass: `${pfx}-current`,
      totalClass: `${pfx}-total`,
      hiddenClass: `${pfx}-hidden`,
      progressbarFillClass: `${pfx}-progressbar-fill`,
      progressbarOppositeClass: `${pfx}-progressbar-opposite`,
      clickableClass: `${pfx}-clickable`,
      lockClass: `${pfx}-lock`,
      horizontalClass: `${pfx}-horizontal`,
      verticalClass: `${pfx}-vertical`
    }
  });
  swiper.pagination = {
    el: null,
    $el: null,
    bullets: []
  };
  let bulletSize;
  let dynamicBulletIndex = 0;
  function isPaginationDisabled() {
    return !swiper.params.pagination.el || !swiper.pagination.el || !swiper.pagination.$el || swiper.pagination.$el.length === 0;
  }
  function setSideBullets($bulletEl, position) {
    const {
      bulletActiveClass
    } = swiper.params.pagination;
    $bulletEl[position]().addClass(`${bulletActiveClass}-${position}`)[position]().addClass(`${bulletActiveClass}-${position}-${position}`);
  }
  function update2() {
    const rtl = swiper.rtl;
    const params = swiper.params.pagination;
    if (isPaginationDisabled())
      return;
    const slidesLength = swiper.virtual && swiper.params.virtual.enabled ? swiper.virtual.slides.length : swiper.slides.length;
    const $el = swiper.pagination.$el;
    let current;
    const total = swiper.params.loop ? Math.ceil((slidesLength - swiper.loopedSlides * 2) / swiper.params.slidesPerGroup) : swiper.snapGrid.length;
    if (swiper.params.loop) {
      current = Math.ceil((swiper.activeIndex - swiper.loopedSlides) / swiper.params.slidesPerGroup);
      if (current > slidesLength - 1 - swiper.loopedSlides * 2) {
        current -= slidesLength - swiper.loopedSlides * 2;
      }
      if (current > total - 1)
        current -= total;
      if (current < 0 && swiper.params.paginationType !== "bullets")
        current = total + current;
    } else if (typeof swiper.snapIndex !== "undefined") {
      current = swiper.snapIndex;
    } else {
      current = swiper.activeIndex || 0;
    }
    if (params.type === "bullets" && swiper.pagination.bullets && swiper.pagination.bullets.length > 0) {
      const bullets = swiper.pagination.bullets;
      let firstIndex;
      let lastIndex;
      let midIndex;
      if (params.dynamicBullets) {
        bulletSize = bullets.eq(0)[swiper.isHorizontal() ? "outerWidth" : "outerHeight"](true);
        $el.css(swiper.isHorizontal() ? "width" : "height", `${bulletSize * (params.dynamicMainBullets + 4)}px`);
        if (params.dynamicMainBullets > 1 && swiper.previousIndex !== void 0) {
          dynamicBulletIndex += current - (swiper.previousIndex - swiper.loopedSlides || 0);
          if (dynamicBulletIndex > params.dynamicMainBullets - 1) {
            dynamicBulletIndex = params.dynamicMainBullets - 1;
          } else if (dynamicBulletIndex < 0) {
            dynamicBulletIndex = 0;
          }
        }
        firstIndex = Math.max(current - dynamicBulletIndex, 0);
        lastIndex = firstIndex + (Math.min(bullets.length, params.dynamicMainBullets) - 1);
        midIndex = (lastIndex + firstIndex) / 2;
      }
      bullets.removeClass(["", "-next", "-next-next", "-prev", "-prev-prev", "-main"].map((suffix) => `${params.bulletActiveClass}${suffix}`).join(" "));
      if ($el.length > 1) {
        bullets.each((bullet) => {
          const $bullet = $(bullet);
          const bulletIndex = $bullet.index();
          if (bulletIndex === current) {
            $bullet.addClass(params.bulletActiveClass);
          }
          if (params.dynamicBullets) {
            if (bulletIndex >= firstIndex && bulletIndex <= lastIndex) {
              $bullet.addClass(`${params.bulletActiveClass}-main`);
            }
            if (bulletIndex === firstIndex) {
              setSideBullets($bullet, "prev");
            }
            if (bulletIndex === lastIndex) {
              setSideBullets($bullet, "next");
            }
          }
        });
      } else {
        const $bullet = bullets.eq(current);
        const bulletIndex = $bullet.index();
        $bullet.addClass(params.bulletActiveClass);
        if (params.dynamicBullets) {
          const $firstDisplayedBullet = bullets.eq(firstIndex);
          const $lastDisplayedBullet = bullets.eq(lastIndex);
          for (let i = firstIndex; i <= lastIndex; i += 1) {
            bullets.eq(i).addClass(`${params.bulletActiveClass}-main`);
          }
          if (swiper.params.loop) {
            if (bulletIndex >= bullets.length) {
              for (let i = params.dynamicMainBullets; i >= 0; i -= 1) {
                bullets.eq(bullets.length - i).addClass(`${params.bulletActiveClass}-main`);
              }
              bullets.eq(bullets.length - params.dynamicMainBullets - 1).addClass(`${params.bulletActiveClass}-prev`);
            } else {
              setSideBullets($firstDisplayedBullet, "prev");
              setSideBullets($lastDisplayedBullet, "next");
            }
          } else {
            setSideBullets($firstDisplayedBullet, "prev");
            setSideBullets($lastDisplayedBullet, "next");
          }
        }
      }
      if (params.dynamicBullets) {
        const dynamicBulletsLength = Math.min(bullets.length, params.dynamicMainBullets + 4);
        const bulletsOffset = (bulletSize * dynamicBulletsLength - bulletSize) / 2 - midIndex * bulletSize;
        const offsetProp = rtl ? "right" : "left";
        bullets.css(swiper.isHorizontal() ? offsetProp : "top", `${bulletsOffset}px`);
      }
    }
    if (params.type === "fraction") {
      $el.find(classesToSelector(params.currentClass)).text(params.formatFractionCurrent(current + 1));
      $el.find(classesToSelector(params.totalClass)).text(params.formatFractionTotal(total));
    }
    if (params.type === "progressbar") {
      let progressbarDirection;
      if (params.progressbarOpposite) {
        progressbarDirection = swiper.isHorizontal() ? "vertical" : "horizontal";
      } else {
        progressbarDirection = swiper.isHorizontal() ? "horizontal" : "vertical";
      }
      const scale = (current + 1) / total;
      let scaleX = 1;
      let scaleY = 1;
      if (progressbarDirection === "horizontal") {
        scaleX = scale;
      } else {
        scaleY = scale;
      }
      $el.find(classesToSelector(params.progressbarFillClass)).transform(`translate3d(0,0,0) scaleX(${scaleX}) scaleY(${scaleY})`).transition(swiper.params.speed);
    }
    if (params.type === "custom" && params.renderCustom) {
      $el.html(params.renderCustom(swiper, current + 1, total));
      emit("paginationRender", $el[0]);
    } else {
      emit("paginationUpdate", $el[0]);
    }
    if (swiper.params.watchOverflow && swiper.enabled) {
      $el[swiper.isLocked ? "addClass" : "removeClass"](params.lockClass);
    }
  }
  function render() {
    const params = swiper.params.pagination;
    if (isPaginationDisabled())
      return;
    const slidesLength = swiper.virtual && swiper.params.virtual.enabled ? swiper.virtual.slides.length : swiper.slides.length;
    const $el = swiper.pagination.$el;
    let paginationHTML = "";
    if (params.type === "bullets") {
      let numberOfBullets = swiper.params.loop ? Math.ceil((slidesLength - swiper.loopedSlides * 2) / swiper.params.slidesPerGroup) : swiper.snapGrid.length;
      if (swiper.params.freeMode && swiper.params.freeMode.enabled && !swiper.params.loop && numberOfBullets > slidesLength) {
        numberOfBullets = slidesLength;
      }
      for (let i = 0; i < numberOfBullets; i += 1) {
        if (params.renderBullet) {
          paginationHTML += params.renderBullet.call(swiper, i, params.bulletClass);
        } else {
          paginationHTML += `<${params.bulletElement} class="${params.bulletClass}"></${params.bulletElement}>`;
        }
      }
      $el.html(paginationHTML);
      swiper.pagination.bullets = $el.find(classesToSelector(params.bulletClass));
    }
    if (params.type === "fraction") {
      if (params.renderFraction) {
        paginationHTML = params.renderFraction.call(swiper, params.currentClass, params.totalClass);
      } else {
        paginationHTML = `<span class="${params.currentClass}"></span> / <span class="${params.totalClass}"></span>`;
      }
      $el.html(paginationHTML);
    }
    if (params.type === "progressbar") {
      if (params.renderProgressbar) {
        paginationHTML = params.renderProgressbar.call(swiper, params.progressbarFillClass);
      } else {
        paginationHTML = `<span class="${params.progressbarFillClass}"></span>`;
      }
      $el.html(paginationHTML);
    }
    if (params.type !== "custom") {
      emit("paginationRender", swiper.pagination.$el[0]);
    }
  }
  function init() {
    swiper.params.pagination = createElementIfNotDefined(swiper, swiper.originalParams.pagination, swiper.params.pagination, {
      el: "swiper-pagination"
    });
    const params = swiper.params.pagination;
    if (!params.el)
      return;
    let $el = $(params.el);
    if ($el.length === 0)
      return;
    if (swiper.params.uniqueNavElements && typeof params.el === "string" && $el.length > 1) {
      $el = swiper.$el.find(params.el);
      if ($el.length > 1) {
        $el = $el.filter((el) => {
          if ($(el).parents(".swiper")[0] !== swiper.el)
            return false;
          return true;
        });
      }
    }
    if (params.type === "bullets" && params.clickable) {
      $el.addClass(params.clickableClass);
    }
    $el.addClass(params.modifierClass + params.type);
    $el.addClass(swiper.isHorizontal() ? params.horizontalClass : params.verticalClass);
    if (params.type === "bullets" && params.dynamicBullets) {
      $el.addClass(`${params.modifierClass}${params.type}-dynamic`);
      dynamicBulletIndex = 0;
      if (params.dynamicMainBullets < 1) {
        params.dynamicMainBullets = 1;
      }
    }
    if (params.type === "progressbar" && params.progressbarOpposite) {
      $el.addClass(params.progressbarOppositeClass);
    }
    if (params.clickable) {
      $el.on("click", classesToSelector(params.bulletClass), function onClick2(e) {
        e.preventDefault();
        let index2 = $(this).index() * swiper.params.slidesPerGroup;
        if (swiper.params.loop)
          index2 += swiper.loopedSlides;
        swiper.slideTo(index2);
      });
    }
    Object.assign(swiper.pagination, {
      $el,
      el: $el[0]
    });
    if (!swiper.enabled) {
      $el.addClass(params.lockClass);
    }
  }
  function destroy() {
    const params = swiper.params.pagination;
    if (isPaginationDisabled())
      return;
    const $el = swiper.pagination.$el;
    $el.removeClass(params.hiddenClass);
    $el.removeClass(params.modifierClass + params.type);
    $el.removeClass(swiper.isHorizontal() ? params.horizontalClass : params.verticalClass);
    if (swiper.pagination.bullets && swiper.pagination.bullets.removeClass)
      swiper.pagination.bullets.removeClass(params.bulletActiveClass);
    if (params.clickable) {
      $el.off("click", classesToSelector(params.bulletClass));
    }
  }
  on2("init", () => {
    init();
    render();
    update2();
  });
  on2("activeIndexChange", () => {
    if (swiper.params.loop) {
      update2();
    } else if (typeof swiper.snapIndex === "undefined") {
      update2();
    }
  });
  on2("snapIndexChange", () => {
    if (!swiper.params.loop) {
      update2();
    }
  });
  on2("slidesLengthChange", () => {
    if (swiper.params.loop) {
      render();
      update2();
    }
  });
  on2("snapGridLengthChange", () => {
    if (!swiper.params.loop) {
      render();
      update2();
    }
  });
  on2("destroy", () => {
    destroy();
  });
  on2("enable disable", () => {
    const {
      $el
    } = swiper.pagination;
    if ($el) {
      $el[swiper.enabled ? "removeClass" : "addClass"](swiper.params.pagination.lockClass);
    }
  });
  on2("lock unlock", () => {
    update2();
  });
  on2("click", (_s, e) => {
    const targetEl = e.target;
    const {
      $el
    } = swiper.pagination;
    if (swiper.params.pagination.el && swiper.params.pagination.hideOnClick && $el.length > 0 && !$(targetEl).hasClass(swiper.params.pagination.bulletClass)) {
      if (swiper.navigation && (swiper.navigation.nextEl && targetEl === swiper.navigation.nextEl || swiper.navigation.prevEl && targetEl === swiper.navigation.prevEl))
        return;
      const isHidden = $el.hasClass(swiper.params.pagination.hiddenClass);
      if (isHidden === true) {
        emit("paginationShow");
      } else {
        emit("paginationHide");
      }
      $el.toggleClass(swiper.params.pagination.hiddenClass);
    }
  });
  Object.assign(swiper.pagination, {
    render,
    update: update2,
    init,
    destroy
  });
}
const _hoisted_1$9 = { class: "section intro" };
const _hoisted_2$9 = { class: "intro__slider" };
const _hoisted_3$8 = { class: "swiper" };
const _hoisted_4$7 = { class: "swiper-wrapper" };
const _hoisted_5$7 = { class: "swiper-slide" };
const _hoisted_6$5 = { class: "headline-24" };
const _hoisted_7$5 = { class: "text swiper-slide__text" };
const _hoisted_8$5 = { class: "swiper-slide" };
const _hoisted_9$5 = { class: "headline-24" };
const _hoisted_10$5 = { class: "text swiper-slide__text" };
const _hoisted_11$5 = { class: "swiper-slide" };
const _hoisted_12$5 = { class: "headline-24" };
const _hoisted_13$5 = { class: "text swiper-slide__text" };
const _hoisted_14$5 = /* @__PURE__ */ createElementVNode("div", { class: "swiper-btns" }, [
  /* @__PURE__ */ createElementVNode("div", { class: "swiper-button-prev" }),
  /* @__PURE__ */ createElementVNode("div", { class: "swiper-button-next" })
], -1);
const _hoisted_15$5 = /* @__PURE__ */ createElementVNode("div", { class: "swiper-pagination" }, null, -1);
const _hoisted_16$5 = { class: "dialog-start" };
const _hoisted_17$5 = { class: "dialog-start__title headline-24" };
const _hoisted_18$5 = { class: "dialog-start__description footnote-12" };
const _hoisted_19$5 = { class: "dialog-start__details" };
const _hoisted_20$5 = { class: "dialog-start__list" };
const _sfc_main$a = {
  __name: "Intro",
  emits: ["next"],
  setup(__props, { emit }) {
    const $t = inject("translate");
    const $l = inject("log");
    const $m = inject("media");
    const isModalOpen = ref(false);
    const openModal = () => {
      isModalOpen.value = true;
    };
    onMounted(() => {
      if (window.localStorage.getItem("session") === null) {
        let today = new Date();
        window.localStorage.setItem("session", btoa(today.toISOString()));
        $l("SYSTEM", "session_created");
      }
      new Swiper(".swiper", {
        loop: false,
        autoHeight: true,
        modules: [Navigation, Pagination],
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev"
        },
        pagination: {
          el: ".swiper-pagination"
        }
      });
    });
    const start = () => {
      $l("SYSTEM", "session_started");
      window.localStorage.setItem("metalurg_block", "1");
      emit("next");
    };
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("main", null, [
        createElementVNode("section", _hoisted_1$9, [
          createElementVNode("div", {
            class: "intro__box",
            style: normalizeStyle(`background: url(${unref($m)(
              "/images/Intro-2.jpg"
            )}) center/cover no-repeat;`)
          }, [
            createElementVNode("div", _hoisted_2$9, [
              createElementVNode("div", _hoisted_3$8, [
                createElementVNode("div", _hoisted_4$7, [
                  createElementVNode("div", _hoisted_5$7, [
                    createElementVNode("h2", _hoisted_6$5, toDisplayString(unref($t)("\u0420\u0430\u0437\u043E\u0433\u0440\u0435\u0432\u0430\u0439\u0442\u0435 \u043F\u0435\u0447\u0438!")), 1),
                    createElementVNode("p", _hoisted_7$5, toDisplayString(unref($t)("\u0412\u044B \u043F\u043E\u043B\u0443\u0447\u0438\u043B\u0438 \u0437\u0430\u043A\u0430\u0437 \u043D\u0430 \u043F\u0440\u043E\u0438\u0437\u0432\u043E\u0434\u0441\u0442\u0432\u043E 200 \u0442\u043E\u043D\u043D \u043A\u0432\u0430\u0434\u0440\u0430\u0442\u043D\u043E\u0433\u043E \u043C\u0435\u0442\u0430\u043B\u043B\u043E\u043F\u0440\u043E\u043A\u0430\u0442\u0430. \u0422\u043E\u043B\u044C\u043A\u043E \u0432\u0434\u0443\u043C\u0430\u0439\u0442\u0435\u0441\u044C! \u041E\u0434\u043D\u0430\u043A\u043E, \u044D\u0442\u043E \u0434\u0430\u043B\u0435\u043A\u043E \u043D\u0435 \u0441\u0430\u043C\u044B\u0439 \u0431\u043E\u043B\u044C\u0448\u043E\u0439 \u0437\u0430\u043A\u0430\u0437, \u0437\u0430\u0442\u043E \u043A\u0440\u0430\u0439\u043D\u0435 \u0432\u0430\u0436\u043D\u044B\u0439.")), 1)
                  ]),
                  createElementVNode("div", _hoisted_8$5, [
                    createElementVNode("h2", _hoisted_9$5, toDisplayString(unref($t)("\u0420\u0430\u0437\u043E\u0433\u0440\u0435\u0432\u0430\u0439\u0442\u0435 \u043F\u0435\u0447\u0438!")), 1),
                    createElementVNode("p", _hoisted_10$5, toDisplayString(unref($t)("\u0421\u0442\u0430\u043B\u044C \u0434\u043E\u043B\u0436\u043D\u0430 \u0431\u044B\u0442\u044C \u043E\u0441\u043E\u0431\u0435\u043D\u043D\u043E\u0439: \u0438\u0437 \u043D\u0435\u0451 \u043D\u0430 \u0434\u0440\u0443\u0433\u0438\u0445 \u0437\u0430\u0432\u043E\u0434\u0430\u0445 \u0431\u0443\u0434\u0443\u0442 \u0441\u043E\u0437\u0434\u0430\u0432\u0430\u0442\u044C \u043E\u0431\u043E\u0440\u0443\u0434\u043E\u0432\u0430\u043D\u0438\u0435 \u0438 \u0434\u0435\u0442\u0430\u043B\u0438 \u0434\u043B\u044F \u043F\u0440\u043E\u0438\u0437\u0432\u043E\u0434\u0441\u0442\u0432\u0430 \u0441\u0430\u043C\u044B\u0445 \u0440\u0430\u0437\u043D\u044B\u0445 \u043C\u0430\u0448\u0438\u043D \u0438 \u043A\u043E\u043D\u0441\u0442\u0440\u0443\u043A\u0446\u0438\u0439, \u043A\u043E\u0442\u043E\u0440\u044B\u043C\u0438 \u043F\u043E\u0442\u043E\u043C \u0431\u0443\u0434\u0443\u0442 \u043F\u043E\u043B\u044C\u0437\u043E\u0432\u0430\u0442\u044C\u0441\u044F \u043F\u043E \u0432\u0441\u0435\u043C\u0443 \u043C\u0438\u0440\u0443.")), 1)
                  ]),
                  createElementVNode("div", _hoisted_11$5, [
                    createElementVNode("h2", _hoisted_12$5, toDisplayString(unref($t)("\u0420\u0430\u0437\u043E\u0433\u0440\u0435\u0432\u0430\u0439\u0442\u0435 \u043F\u0435\u0447\u0438!")), 1),
                    createElementVNode("p", _hoisted_13$5, toDisplayString(unref($t)("\u041F\u043E\u043B\u0443\u0447\u0430\u0435\u0442\u0441\u044F, \u0447\u0442\u043E \u0431\u0435\u0437 \u0432\u0430\u0441 \u043D\u0435 \u0431\u0443\u0434\u0443\u0442 \u0435\u0437\u0434\u0438\u0442\u044C \u043C\u0430\u0448\u0438\u043D\u044B \u0438 \u043B\u0435\u0442\u0430\u0442\u044C \u0441\u0430\u043C\u043E\u043B\u0451\u0442\u044B, \u043E\u0441\u0442\u0430\u043D\u043E\u0432\u044F\u0442\u0441\u044F \u0441\u0442\u0440\u043E\u0439\u043A\u0438 \u0438 \u043F\u0440\u0435\u0434\u043F\u0440\u0438\u044F\u0442\u0438\u044F. \u0413\u043E\u0442\u043E\u0432\u044B \u043A \u0442\u0430\u043A\u043E\u0439 \u043E\u0442\u0432\u0435\u0442\u0441\u0442\u0432\u0435\u043D\u043D\u043E\u0439 \u0437\u0430\u0434\u0430\u0447\u0435? \u0422\u043E\u0433\u0434\u0430 \u043F\u043E\u0440\u0430 \u043E\u0431\u0443\u0437\u0434\u0430\u0442\u044C \u0441\u0442\u0438\u0445\u0438\u044E \u043E\u0433\u043D\u044F \u0438 \u043C\u0435\u0442\u0430\u043B\u043B\u0430!")), 1)
                  ])
                ]),
                _hoisted_14$5,
                _hoisted_15$5
              ])
            ]),
            createElementVNode("span", {
              class: "intro__btn button-final",
              onClick: openModal
            }, toDisplayString(unref($t)("\u041F\u0435\u0440\u0435\u0439\u0442\u0438 \u0432 \u0446\u0435\u0445")), 1)
          ], 4)
        ]),
        createVNode(Transition, { name: "fade" }, {
          default: withCtx(() => [
            isModalOpen.value ? (openBlock(), createBlock(_sfc_main$b, {
              key: 0,
              width: 398
            }, {
              default: withCtx(() => [
                createElementVNode("div", _hoisted_16$5, [
                  createElementVNode("div", _hoisted_17$5, toDisplayString(unref($t)("\u0412\u044B \u043F\u043E\u043B\u0443\u0447\u0438\u043B\u0438 \u0437\u0430\u043A\u0430\u0437")), 1),
                  createElementVNode("div", _hoisted_18$5, toDisplayString(unref($t)("\u0417\u0430\u043A\u0430\u0437\u0447\u0438\u043A: \u0437\u0430\u0432\u043E\u0434 \u041E\u041E\u041E \xAB\u042D\u0441\u043A\u0438\u0437 \u0414\u0435\u0442\u0430\u043B\u044C \u041F\u0440\u043E\u043C\xBB")), 1),
                  createElementVNode("div", _hoisted_19$5, [
                    createElementVNode("div", null, [
                      createElementVNode("p", null, toDisplayString(unref($t)("\u0414\u0435\u0442\u0430\u043B\u0438")), 1),
                      createElementVNode("span", null, toDisplayString(unref($t)("\u0418\u0437\u0433\u043E\u0442\u043E\u0432\u0438\u0442\u044C 200 \u0442\u043E\u043D\u043D \u043F\u0440\u043E\u043A\u0430\u0442\u0430 \u043F\u043E \u0441\u043B\u0435\u0434\u0443\u044E\u0449\u0438\u043C \u043F\u0430\u0440\u0430\u043C\u0435\u0442\u0440\u0430\u043C:")), 1)
                    ]),
                    createElementVNode("div", null, [
                      createElementVNode("p", null, toDisplayString(unref($t)("\u0420\u043E\u0434 \u0434\u0435\u044F\u0442\u0435\u043B\u044C\u043D\u043E\u0441\u0442\u0438")), 1),
                      createElementVNode("span", null, toDisplayString(unref($t)("\u041F\u0440\u043E\u0438\u0437\u0432\u043E\u0434\u0441\u0442\u0432\u043E \u0448\u0442\u0430\u043C\u043F\u043E\u0432 \u0434\u043B\u044F \u043A\u043E\u0432\u043A\u0438 \u0430\u0432\u0442\u043E\u043C\u043E\u0431\u0438\u043B\u0435\u0439, \u0434\u0435\u0442\u0430\u043B\u0435\u0439 \u0438 \u043A\u043E\u043D\u0441\u0442\u0440\u0443\u043A\u0446\u0438\u0439")), 1)
                    ])
                  ]),
                  createElementVNode("ul", _hoisted_20$5, [
                    createElementVNode("li", null, [
                      createElementVNode("span", null, toDisplayString(unref($t)("\u041A\u0432\u0430\u0434\u0440\u0430\u0442\u043D\u044B\u0439 \u043F\u0440\u043E\u0444\u0438\u043B\u044C")), 1),
                      createElementVNode("p", null, toDisplayString(unref($t)("170x170 \u043C\u043C")), 1)
                    ]),
                    createElementVNode("li", null, [
                      createElementVNode("span", null, toDisplayString(unref($t)("\u041C\u0430\u0440\u043A\u0430 \u0441\u0442\u0430\u043B\u0438")), 1),
                      createElementVNode("p", null, toDisplayString(unref($t)("4\u04255\u041C\u0424\u0421")), 1)
                    ]),
                    createElementVNode("li", null, [
                      createElementVNode("span", null, toDisplayString(unref($t)("\u0414\u043B\u0438\u043D\u0430")), 1),
                      createElementVNode("p", null, toDisplayString(unref($t)("3000 \u2014 6000 \u043C\u043C")), 1)
                    ]),
                    createElementVNode("li", null, [
                      createElementVNode("span", null, toDisplayString(unref($t)("\u0422\u0435\u0440\u043C\u043E\u043E\u0431\u0440\u0430\u0431\u043E\u0442\u043A\u0430")), 1),
                      createElementVNode("p", null, toDisplayString(unref($t)("\u041E\u0442\u0436\u0438\u0433")), 1)
                    ])
                  ]),
                  createElementVNode("button", {
                    type: "button",
                    onClick: start
                  }, toDisplayString(unref($t)("\u041F\u0440\u0438\u043D\u044F\u0442\u044C")), 1)
                ])
              ]),
              _: 1
            })) : createCommentVNode("", true)
          ]),
          _: 1
        })
      ]);
    };
  }
};
const _hoisted_1$8 = { class: "button-banner h-min w-min px-4 py-2" };
const _hoisted_2$8 = ["textContent"];
const _sfc_main$9 = {
  __name: "ButtonBanner",
  props: {
    label: {
      required: true,
      type: String
    }
  },
  setup(__props) {
    const $t = inject("translate");
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("button", _hoisted_1$8, [
        createElementVNode("span", {
          class: "button-banner__label headline-16 whitespace-nowrap",
          textContent: toDisplayString(unref($t)(__props.label))
        }, null, 8, _hoisted_2$8)
      ]);
    };
  }
};
const _hoisted_1$7 = { class: "banner-standard fixed bottom-20 flex flex-nowrap items-center justify-between space-x-8 px-6 py-4" };
const _hoisted_2$7 = ["textContent"];
const _hoisted_3$7 = ["textContent"];
const _sfc_main$8 = {
  __name: "BannerStandard",
  props: {
    title: {
      required: true,
      type: String
    },
    text: {
      required: true,
      type: String
    },
    label: {
      required: true,
      type: String
    }
  },
  emits: ["label-click"],
  setup(__props, { emit: emits }) {
    const $t = inject("translate");
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("div", _hoisted_1$7, [
        createElementVNode("div", null, [
          createElementVNode("p", {
            class: "headline-20",
            textContent: toDisplayString(unref($t)(__props.title))
          }, null, 8, _hoisted_2$7),
          createElementVNode("p", {
            class: "main-text-16",
            textContent: toDisplayString(unref($t)(__props.text))
          }, null, 8, _hoisted_3$7)
        ]),
        createVNode(_sfc_main$9, {
          onClick: _cache[0] || (_cache[0] = ($event) => emits("label-click")),
          label: __props.label
        }, null, 8, ["label"])
      ]);
    };
  }
};
const _hoisted_1$6 = ["src"];
const _hoisted_2$6 = { class: "ml-2 space-y-0.5" };
const _hoisted_3$6 = ["textContent"];
const _hoisted_4$6 = ["textContent"];
const _hoisted_5$6 = ["src"];
const _sfc_main$7 = {
  __name: "InformerPopup",
  props: {
    title: {
      required: true,
      type: String
    },
    text: {
      required: true,
      type: String
    },
    error: {
      default: true,
      type: Boolean
    }
  },
  emits: ["close"],
  setup(__props, { emit: emits }) {
    const $t = inject("translate");
    const $m = inject("media");
    onMounted(() => {
      setTimeout(() => {
        emits("close");
      }, 6e3);
    });
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("div", {
        class: "informer-popup absolute left-1/2 top-2 flex -translate-x-1/2 px-3 py-2.5",
        style: normalizeStyle(
          __props.error ? "" : "--border-color: #08b585; --shadow-blur: rgba(2, 136, 99, 0.28)"
        )
      }, [
        createElementVNode("img", {
          src: __props.error ? unref($m)("/images/ui/attention.svg") : unref($m)("/images/ui/info.svg"),
          alt: "Informer icon",
          class: "informer-popup__icon"
        }, null, 8, _hoisted_1$6),
        createElementVNode("div", _hoisted_2$6, [
          createElementVNode("p", {
            class: "headline-12",
            textContent: toDisplayString(unref($t)(__props.title))
          }, null, 8, _hoisted_3$6),
          createElementVNode("p", {
            class: "footnote-12",
            textContent: toDisplayString(unref($t)(__props.text))
          }, null, 8, _hoisted_4$6)
        ]),
        createElementVNode("img", {
          src: unref($m)("/images/ui/close.svg"),
          alt: "Close icon",
          class: "informer-popup__close-icon ml-3 self-start",
          onClick: _cache[0] || (_cache[0] = ($event) => emits("close"))
        }, null, 8, _hoisted_5$6)
      ], 4);
    };
  }
};
const _hoisted_1$5 = { class: "section technology" };
const _hoisted_2$5 = { class: "panel technology__col-1" };
const _hoisted_3$5 = { class: "main-text-16 technology__col-1__pre-title" };
const _hoisted_4$5 = { class: "headline-24 technology__col-1__title" };
const _hoisted_5$5 = { class: "main-text-16 technology__col-1__text-1" };
const _hoisted_6$4 = { class: "technology__col-1__text-2" };
const _hoisted_7$4 = { class: "panel technology__col-2" };
const _hoisted_8$4 = { class: "technology-items" };
const _hoisted_9$4 = ["src"];
const _hoisted_10$4 = { class: "technology-items__title headline-16" };
const _hoisted_11$4 = ["src"];
const _hoisted_12$4 = { class: "technology-items__title headline-16" };
const _hoisted_13$4 = ["src"];
const _hoisted_14$4 = { class: "technology-items__title headline-16" };
const _hoisted_15$4 = ["src"];
const _hoisted_16$4 = { class: "technology-items__title headline-16" };
const _hoisted_17$4 = ["src"];
const _hoisted_18$4 = { class: "technology-items__title headline-16" };
const _hoisted_19$4 = ["src"];
const _hoisted_20$4 = { class: "technology-items__title headline-16" };
const _hoisted_21$4 = ["src"];
const _hoisted_22$4 = { class: "technology-items__title headline-16" };
const _hoisted_23$4 = ["src"];
const _hoisted_24$4 = { class: "technology-items__title headline-16" };
const _hoisted_25$3 = { class: "panel technology-quiz" };
const _hoisted_26$3 = { class: "technology-quiz__questions" };
const _hoisted_27$3 = { class: "technology-quiz__questions__progress" };
const _hoisted_28$3 = { class: "_left" };
const _hoisted_29$3 = { class: "_right" };
const _hoisted_30$3 = {
  key: 0,
  class: "technology-quiz__questions__desc"
};
const _hoisted_31$3 = {
  key: 1,
  class: "technology-quiz__questions__desc"
};
const _hoisted_32$3 = {
  key: 2,
  class: "technology-quiz__questions__desc"
};
const _hoisted_33$3 = {
  key: 3,
  class: "technology-quiz__questions__desc"
};
const _hoisted_34$3 = {
  key: 4,
  class: "technology-quiz__questions__desc"
};
const _hoisted_35$2 = {
  key: 5,
  class: "technology-quiz__questions__desc"
};
const _hoisted_36$2 = {
  key: 6,
  class: "technology-quiz__questions__desc"
};
const _hoisted_37$2 = {
  key: 7,
  class: "technology-quiz__questions__desc"
};
const _hoisted_38$2 = { class: "technology-quiz__variants" };
const _sfc_main$6 = {
  __name: "Technology",
  emits: ["next"],
  setup(__props, { emit }) {
    const $t = inject("translate");
    inject("log");
    const $m = inject("media");
    const quizStep = ref(0);
    const wrongAnswer = ref(0);
    const selectQuizAnswer = (num) => {
      wrongAnswer.value = 0;
      if (num === quizStep.value + 1)
        quizStep.value++;
      else {
        wrongAnswer.value = num;
      }
    };
    const checkQuizAnswerPassed = (num) => {
      return num <= quizStep.value;
    };
    const nextStep = () => {
      window.localStorage.setItem("metalurg_block", "2");
      emit("next");
    };
    const hideError = () => {
      wrongAnswer.value = false;
    };
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("main", null, [
        createElementVNode("section", _hoisted_1$5, [
          createElementVNode("div", null, [
            createElementVNode("div", _hoisted_2$5, [
              createElementVNode("div", _hoisted_3$5, toDisplayString(unref($t)("\u0427\u0430\u0441\u0442\u044C 1")), 1),
              createElementVNode("div", _hoisted_4$5, toDisplayString(unref($t)("\u0422\u0435\u0445\u043D\u043E\u043B\u043E\u0433\u0438\u044F")), 1),
              createElementVNode("div", _hoisted_5$5, toDisplayString(unref($t)("\u0427\u0442\u043E\u0431\u044B \u0441\u0434\u0435\u043B\u0430\u0442\u044C \u0438\u0437 \u0437\u0430\u0433\u043E\u0442\u043E\u0432\u043A\u0438 \u043C\u0435\u0442\u0430\u043B\u043B \u0441 \u0438\u0434\u0435\u0430\u043B\u044C\u043D\u044B\u043C\u0438 \u043A\u0430\u0447\u0435\u0441\u0442\u0432\u0430\u043C\u0438, \u043D\u0443\u0436\u043D\u043E \u043F\u0440\u043E\u0439\u0442\u0438 \u043D\u0435\u043C\u0430\u043B\u043E \u0448\u0430\u0433\u043E\u0432.")), 1),
              createElementVNode("div", _hoisted_6$4, toDisplayString(unref($t)("\u041E\u043F\u0440\u0435\u0434\u0435\u043B\u0438\u0442\u0435, \u043E \u043A\u0430\u043A\u043E\u043C \u0442\u0435\u0445\u043D\u043E\u043B\u043E\u0433\u0438\u0447\u0435\u0441\u043A\u043E\u043C \u043F\u0440\u043E\u0446\u0435\u0441\u0441\u0435 \u0438\u0434\u0435\u0442 \u0440\u0435\u0447\u044C?")), 1)
            ])
          ]),
          createElementVNode("div", _hoisted_7$4, [
            createElementVNode("div", _hoisted_8$4, [
              createElementVNode("div", {
                class: normalizeClass({ "--passed": checkQuizAnswerPassed(1) })
              }, [
                createElementVNode("img", {
                  class: "technology-items__img",
                  src: unref($m)("/images/tech-1.png")
                }, null, 8, _hoisted_9$4),
                createElementVNode("div", _hoisted_10$4, toDisplayString(unref($t)("\u041D\u0430\u0433\u0440\u0435\u0432")), 1)
              ], 2),
              createElementVNode("div", {
                class: normalizeClass({ "--passed": checkQuizAnswerPassed(2) })
              }, [
                createElementVNode("img", {
                  class: "technology-items__img",
                  src: unref($m)("/images/tech-2.png")
                }, null, 8, _hoisted_11$4),
                createElementVNode("div", _hoisted_12$4, toDisplayString(unref($t)("\u041F\u0440\u043E\u043A\u0430\u0442")), 1)
              ], 2),
              createElementVNode("div", {
                class: normalizeClass({ "--passed": checkQuizAnswerPassed(3) })
              }, [
                createElementVNode("img", {
                  class: "technology-items__img",
                  src: unref($m)("/images/tech-3.png")
                }, null, 8, _hoisted_13$4),
                createElementVNode("div", _hoisted_14$4, toDisplayString(unref($t)("\u041F\u043E\u0440\u0435\u0437\u043A\u0430")), 1)
              ], 2),
              createElementVNode("div", {
                class: normalizeClass({ "--passed": checkQuizAnswerPassed(4) })
              }, [
                createElementVNode("img", {
                  class: "technology-items__img",
                  src: unref($m)("/images/tech-4.png")
                }, null, 8, _hoisted_15$4),
                createElementVNode("div", _hoisted_16$4, toDisplayString(unref($t)("\u041E\u0442\u0431\u043E\u0440 \u043F\u0440\u043E\u0431")), 1)
              ], 2),
              createElementVNode("div", {
                class: normalizeClass({ "--passed": checkQuizAnswerPassed(5) })
              }, [
                createElementVNode("img", {
                  class: "technology-items__img",
                  src: unref($m)("/images/tech-5.png")
                }, null, 8, _hoisted_17$4),
                createElementVNode("div", _hoisted_18$4, toDisplayString(unref($t)("\u041E\u0445\u043B\u0430\u0436\u0434\u0435\u043D\u0438\u0435")), 1)
              ], 2),
              createElementVNode("div", {
                class: normalizeClass({ "--passed": checkQuizAnswerPassed(6) })
              }, [
                createElementVNode("img", {
                  class: "technology-items__img",
                  src: unref($m)("/images/tech-6.png")
                }, null, 8, _hoisted_19$4),
                createElementVNode("div", _hoisted_20$4, toDisplayString(unref($t)("\u0422\u0435\u0440\u043C\u043E\u043E\u0431\u0440\u0430\u0431\u043E\u0442\u043A\u0430")), 1)
              ], 2),
              createElementVNode("div", {
                class: normalizeClass({ "--passed": checkQuizAnswerPassed(7) })
              }, [
                createElementVNode("img", {
                  class: "technology-items__img",
                  src: unref($m)("/images/tech-7.png")
                }, null, 8, _hoisted_21$4),
                createElementVNode("div", _hoisted_22$4, toDisplayString(unref($t)("\u041F\u0440\u0430\u0432\u043A\u0438, \u043E\u0431\u0442\u043E\u0447\u043A\u0430 \u0438 \u0444\u0438\u043D\u0430\u043B\u044C\u043D\u0430\u044F \u043F\u0440\u0438\u0435\u043C\u043A\u0430")), 1)
              ], 2),
              createElementVNode("div", {
                class: normalizeClass({ "--passed": checkQuizAnswerPassed(8) })
              }, [
                createElementVNode("img", {
                  class: "technology-items__img",
                  src: unref($m)("/images/tech-8.png")
                }, null, 8, _hoisted_23$4),
                createElementVNode("div", _hoisted_24$4, toDisplayString(unref($t)("\u0423\u043F\u0430\u043A\u043E\u0432\u043A\u0430 \u0438 \u043F\u0435\u0440\u0435\u0434\u0430\u0447\u0430 \u043D\u0430 \u0441\u043A\u043B\u0430\u0434")), 1)
              ], 2)
            ])
          ]),
          createElementVNode("div", null, [
            createElementVNode("div", _hoisted_25$3, [
              createElementVNode("div", _hoisted_26$3, [
                createElementVNode("div", _hoisted_27$3, [
                  createElementVNode("span", _hoisted_28$3, toDisplayString(unref($t)("\u0421\u043E\u043F\u043E\u0441\u0442\u0430\u0432\u044C\u0442\u0435 \u0432\u0430\u0440\u0438\u0430\u043D\u0442 \u0441 \u043E\u043F\u0438\u0441\u0430\u043D\u0438\u0435\u043C")), 1),
                  createElementVNode("span", _hoisted_29$3, toDisplayString(quizStep.value + 1 < 9 ? quizStep.value + 1 : 8) + "/8", 1)
                ]),
                quizStep.value + 1 === 1 ? (openBlock(), createElementBlock("div", _hoisted_30$3, toDisplayString(unref($t)("\u041F\u0440\u043E\u0446\u0435\u0441\u0441, \u043F\u0440\u0438 \u043A\u043E\u0442\u043E\u0440\u043E\u043C \u0441\u043B\u0438\u0442\u043A\u0438 \u0432 \u043F\u0435\u0447\u0438 \u0434\u043E\u0445\u043E\u0434\u044F\u0442 \u0434\u043E \u0442\u0435\u043C\u043F\u0435\u0440\u0430\u0442\u0443\u0440\u044B \u043F\u043B\u0430\u0441\u0442\u0438\u0447\u0435\u0441\u043A\u043E\u0439 \u0434\u0435\u0444\u043E\u0440\u043C\u0430\u0446\u0438\u0438")), 1)) : quizStep.value + 1 === 2 ? (openBlock(), createElementBlock("div", _hoisted_31$3, toDisplayString(unref($t)("\u0414\u0435\u0444\u043E\u0440\u043C\u0438\u0440\u043E\u0432\u0430\u043D\u0438\u0435 \u043C\u0435\u0442\u0430\u043B\u043B\u0430 \u0441\xA0\u043F\u043E\u043C\u043E\u0449\u044C\u044E \u0441\u043F\u0435\u0446\u0438\u0430\u043B\u044C\u043D\u043E\u0439 \u043C\u0430\u0448\u0438\u043D\u044B\xA0\u2014 \u043F\u0440\u043E\u043A\u0430\u0442\u043D\u043E\u0433\u043E \u0441\u0442\u0430\u043D\u0430")), 1)) : quizStep.value + 1 === 3 ? (openBlock(), createElementBlock("div", _hoisted_32$3, toDisplayString(unref($t)("\u042D\u0442\u0430\u043F \u0440\u0430\u0437\u0434\u0435\u043B\u0435\u043D\u0438\u044F \u043F\u0440\u043E\u043A\u0430\u0442\u0430 \u043D\u0430 \u0442\u0440\u0435\u0431\u0443\u0435\u043C\u0443\u044E \u0434\u043B\u0438\u043D\u0443")), 1)) : quizStep.value + 1 === 4 ? (openBlock(), createElementBlock("div", _hoisted_33$3, toDisplayString(unref($t)("\u041F\u0440\u043E\u0432\u0435\u0440\u043A\u0430 \u043C\u0435\u0442\u0430\u043B\u043B\u0430 \u043D\u0430 \u043A\u0430\u0447\u0435\u0441\u0442\u0432\u043E. \u042D\u0442\u0443 \u0437\u0430\u0434\u0430\u0447\u0443 \u043C\u043E\u0433\u0443\u0442 \u0432\u044B\u043F\u043E\u043B\u043D\u044F\u0442\u044C \u043D\u0435\u0441\u043A\u043E\u043B\u044C\u043A\u043E \u0440\u0430\u0437: \u043F\u043E\u0441\u043B\u0435 \u043F\u043E\u0440\u0435\u0437\u043A\u0438 \u043F\u0440\u043E\u043A\u0430\u0442\u0430, \u043E\u0445\u043B\u0430\u0436\u0434\u0435\u043D\u0438\u044F \u0438 \u0442\u0435\u0440\u043C\u043E\u043E\u0431\u0440\u0430\u0431\u043E\u0442\u043A\u0438")), 1)) : quizStep.value + 1 === 5 ? (openBlock(), createElementBlock("div", _hoisted_34$3, toDisplayString(unref($t)("\u042D\u0442\u0430\u043F, \u043D\u0430 \u043A\u043E\u0442\u043E\u0440\u043E\u043C \u0433\u043E\u0440\u044F\u0447\u0438\u0439 \u043F\u0440\u043E\u043A\u0430\u0442 \u043E\u0441\u0442\u044B\u0432\u0430\u0435\u0442 \u043D\u0430 \u043E\u0442\u043A\u0440\u044B\u0442\u043E\u043C \u0432\u043E\u0437\u0434\u0443\u0445\u0435 \u043B\u0438\u0431\u043E \u0432 \u0441\u043F\u0435\u0446\u0438\u0430\u043B\u044C\u043D\u044B\u0445 \u0442\u0435\u0440\u043C\u0438\u0447\u0435\u0441\u043A\u0438\u0445 \u043A\u043E\u043B\u043E\u0434\u0446\u0430\u0445")), 1)) : quizStep.value + 1 === 6 ? (openBlock(), createElementBlock("div", _hoisted_35$2, toDisplayString(unref($t)("\u041F\u0440\u043E\u0446\u0435\u0441\u0441 \u043D\u0430\u0433\u0440\u0435\u0432\u0430, \u0432\u044B\u0434\u0435\u0440\u0436\u043A\u0438 \u0438\xA0\u043E\u0445\u043B\u0430\u0436\u0434\u0435\u043D\u0438\u044F \u0438\u0437\u0434\u0435\u043B\u0438\u044F, \u0447\u0442\u043E\u0431\u044B \u0438\u0437\u043C\u0435\u043D\u0438\u043B\u0430\u0441\u044C \u0432\u043D\u0443\u0442\u0440\u0435\u043D\u043D\u044F\u044F \u0441\u0442\u0440\u0443\u043A\u0442\u0443\u0440\u0430 \u043C\u0435\u0442\u0430\u043B\u043B\u0430 \u0438 \u043F\u043E\u043B\u0443\u0447\u0438\u043B\u0438\u0441\u044C \u0438\u0434\u0435\u0430\u043B\u044C\u043D\u044B\u0435 \u0441\u0432\u043E\u0439\u0441\u0442\u0432\u0430")), 1)) : quizStep.value + 1 === 7 ? (openBlock(), createElementBlock("div", _hoisted_36$2, toDisplayString(unref($t)("\u042D\u0442\u0430\u043F, \u043D\u0430 \u043A\u043E\u0442\u043E\u0440\u043E\u043C \u043C\u0435\u0442\u0430\u043B\u043B \u043F\u0440\u0438\xA0\u043D\u0435\u043E\u0431\u0445\u043E\u0434\u0438\u043C\u043E\u0441\u0442\u0438 \u043E\u0431\u0442\u0430\u0447\u0438\u0432\u0430\u044E\u0442, \u0437\u0430\u0447\u0438\u0449\u0430\u044E\u0442 \u0434\u0435\u0444\u0435\u043A\u0442\u044B \u0438 \u0433\u043E\u0442\u043E\u0432\u044F\u0442 \u043A\xA0\u043E\u0442\u043F\u0440\u0430\u0432\u043A\u0435 \u043A\u043B\u0438\u0435\u043D\u0442\u0443")), 1)) : quizStep.value + 1 === 8 || quizStep.value + 1 === 9 ? (openBlock(), createElementBlock("div", _hoisted_37$2, toDisplayString(unref($t)("\u041F\u043E\u0441\u043B\u0435\u0434\u043D\u0438\u0439 \u0448\u0430\u0433: \u0433\u043E\u0442\u043E\u0432\u044B\u0439 \u043C\u0435\u0442\u0430\u043B\u043B\u0438\u0447\u0435\u0441\u043A\u0438\u0439 \u043F\u0440\u043E\u043A\u0430\u0442 \u043C\u043E\u0436\u043D\u043E \u043E\u0442\u043F\u0440\u0430\u0432\u043B\u044F\u0442\u044C \u0437\u0430\u043A\u0430\u0437\u0447\u0438\u043A\u0443")), 1)) : createCommentVNode("", true)
              ]),
              createElementVNode("ul", _hoisted_38$2, [
                createElementVNode("li", {
                  class: normalizeClass({
                    "technology-quiz__variants__item": true,
                    "--passed": checkQuizAnswerPassed(8),
                    "--wrong": wrongAnswer.value === 8
                  }),
                  onClick: _cache[0] || (_cache[0] = ($event) => selectQuizAnswer(8))
                }, toDisplayString(unref($t)("\u0423\u043F\u0430\u043A\u043E\u0432\u043A\u0430 \u0438 \u043F\u0435\u0440\u0435\u0434\u0430\u0447\u0430 \u043D\u0430 \u0441\u043A\u043B\u0430\u0434")), 3),
                createElementVNode("li", {
                  class: normalizeClass({
                    "technology-quiz__variants__item": true,
                    "--passed": checkQuizAnswerPassed(2),
                    "--wrong": wrongAnswer.value === 2
                  }),
                  onClick: _cache[1] || (_cache[1] = ($event) => selectQuizAnswer(2))
                }, toDisplayString(unref($t)("\u041F\u0440\u043E\u043A\u0430\u0442")), 3),
                createElementVNode("li", {
                  class: normalizeClass({
                    "technology-quiz__variants__item": true,
                    "--passed": checkQuizAnswerPassed(6),
                    "--wrong": wrongAnswer.value === 6
                  }),
                  onClick: _cache[2] || (_cache[2] = ($event) => selectQuizAnswer(6))
                }, toDisplayString(unref($t)("\u0422\u0435\u0440\u043C\u043E\u043E\u0431\u0440\u0430\u0431\u043E\u0442\u043A\u0430")), 3),
                createElementVNode("li", {
                  class: normalizeClass({
                    "technology-quiz__variants__item": true,
                    "--passed": checkQuizAnswerPassed(4),
                    "--wrong": wrongAnswer.value === 4
                  }),
                  onClick: _cache[3] || (_cache[3] = ($event) => selectQuizAnswer(4))
                }, toDisplayString(unref($t)("\u041E\u0442\u0431\u043E\u0440 \u043F\u0440\u043E\u0431")), 3),
                createElementVNode("li", {
                  class: normalizeClass({
                    "technology-quiz__variants__item": true,
                    "--passed": checkQuizAnswerPassed(5),
                    "--wrong": wrongAnswer.value === 5
                  }),
                  onClick: _cache[4] || (_cache[4] = ($event) => selectQuizAnswer(5))
                }, toDisplayString(unref($t)("\u041E\u0445\u043B\u0430\u0436\u0434\u0435\u043D\u0438\u0435")), 3),
                createElementVNode("li", {
                  class: normalizeClass({
                    "technology-quiz__variants__item": true,
                    "--passed": checkQuizAnswerPassed(7),
                    "--wrong": wrongAnswer.value === 7
                  }),
                  onClick: _cache[5] || (_cache[5] = ($event) => selectQuizAnswer(7))
                }, toDisplayString(unref($t)("\u041F\u0440\u0430\u0432\u043A\u0438, \u043E\u0431\u0442\u043E\u0447\u043A\u0430 \u0438 \u0444\u0438\u043D\u0430\u043B\u044C\u043D\u0430\u044F \u043F\u0440\u0438\u0451\u043C\u043A\u0430")), 3),
                createElementVNode("li", {
                  class: normalizeClass({
                    "technology-quiz__variants__item": true,
                    "--passed": checkQuizAnswerPassed(1),
                    "--wrong": wrongAnswer.value === 1
                  }),
                  onClick: _cache[6] || (_cache[6] = ($event) => selectQuizAnswer(1))
                }, toDisplayString(unref($t)("\u041D\u0430\u0433\u0440\u0435\u0432")), 3),
                createElementVNode("li", {
                  class: normalizeClass({
                    "technology-quiz__variants__item": true,
                    "--passed": checkQuizAnswerPassed(3),
                    "--wrong": wrongAnswer.value === 3
                  }),
                  onClick: _cache[7] || (_cache[7] = ($event) => selectQuizAnswer(3))
                }, toDisplayString(unref($t)("\u041F\u043E\u0440\u0435\u0437\u043A\u0430")), 3)
              ])
            ])
          ]),
          createVNode(Transition, { name: "fade" }, {
            default: withCtx(() => [
              wrongAnswer.value ? (openBlock(), createBlock(_sfc_main$7, {
                key: 0,
                width: 880,
                title: "\u041E\u0439, \u044D\u0442\u043E \u043D\u0435\u0432\u0435\u0440\u043D\u044B\u0439 \u0432\u0430\u0440\u0438\u0430\u043D\u0442!",
                text: "\u0417\u0430\u0433\u043B\u044F\u043D\u0438\u0442\u0435 \u0432 \u0441\u043F\u0440\u0430\u0432\u043E\u0447\u043D\u0438\u043A, \u0435\u0441\u043B\u0438 \u043D\u0435 \u043F\u043E\u043B\u0443\u0447\u0430\u0435\u0442\u0441\u044F \u0432\u044B\u0431\u0440\u0430\u0442\u044C \u043F\u0440\u0430\u0432\u0438\u043B\u044C\u043D\u044B\u0439 \u043E\u0442\u0432\u0435\u0442.",
                error: true,
                onClose: hideError
              }, null, 8, ["text"])) : createCommentVNode("", true)
            ]),
            _: 1
          }),
          quizStep.value >= 8 ? (openBlock(), createBlock(_sfc_main$8, {
            key: 0,
            title: unref($t)("\u0413\u043E\u0442\u043E\u0432\u043E!"),
            text: unref($t)("\u041D\u0430\u0433\u0440\u0435\u0432 \u043C\u0435\u0442\u0430\u043B\u043B\u0430 \u043E\u0447\u0435\u043D\u044C \u043F\u043E\u0445\u043E\u0436 \u043D\u0430 \u043F\u0440\u0438\u0433\u043E\u0442\u043E\u0432\u043B\u0435\u043D\u0438\u0435 \u043F\u0438\u0440\u043E\u0433\u0430: \u0441\u043D\u0430\u0447\u0430\u043B\u0430 \u043F\u0435\u0447\u044C \u043D\u0430\u0434\u043E \u0440\u0430\u0437\u043E\u0433\u0440\u0435\u0442\u044C, \u0437\u0430\u0433\u0440\u0443\u0437\u0438\u0442\u044C \u0442\u0443\u0434\u0430 \xAB\u0442\u0435\u0441\u0442\u043E\xBB, \u0430 \u0432 \u043A\u043E\u043D\u0446\u0435 \u2014 \u0434\u0430\u0442\u044C \u043F\u043E\u0442\u043E\u043C\u0438\u0442\u044C\u0441\u044F. \u0422\u0435\u043C\u043F\u0435\u0440\u0430\u0442\u0443\u0440\u0430 \u0438 \u0432\u0440\u0435\u043C\u044F \u0437\u0430\u043F\u0435\u043A\u0430\u043D\u0438\u044F \u0431\u0443\u0434\u0443\u0442 \u0437\u0430\u0432\u0438\u0441\u0435\u0442\u044C \u043E\u0442 \u043D\u0430\u0447\u0438\u043D\u043A\u0438 \u2014 \u0438\u043D\u0430\u0447\u0435 \u043F\u0438\u0440\u043E\u0433 \u0441\u0433\u043E\u0440\u0438\u0442 \u0438\u043B\u0438 \u043D\u0435 \u043F\u0440\u043E\u043F\u0435\u0447\u0451\u0442\u0441\u044F."),
            label: unref($t)("\u0413\u043E\u0442\u043E\u0432\u043E"),
            onLabelClick: nextStep
          }, null, 8, ["title", "text", "label"])) : createCommentVNode("", true)
        ])
      ]);
    };
  }
};
const _hoisted_1$4 = {
  xmlns: "http://www.w3.org/2000/svg",
  width: "12",
  height: "13",
  viewBox: "0 0 12 13",
  fill: "none"
};
const _hoisted_2$4 = /* @__PURE__ */ createElementVNode("circle", {
  cx: "6",
  cy: "6.5",
  r: "5.5",
  fill: "white",
  stroke: "#C8CDD0"
}, null, -1);
const _hoisted_3$4 = {
  key: 0,
  cx: "6",
  cy: "6.5",
  r: "3",
  fill: "#DE234B"
};
const _hoisted_4$4 = {
  key: 1,
  cx: "6",
  cy: "6.5",
  r: "3",
  fill: "#490FE7"
};
const _hoisted_5$4 = ["innerHTML"];
const _sfc_main$5 = {
  __name: "AnswerButton",
  props: {
    active: {
      required: true,
      type: Boolean
    },
    label: {
      required: true,
      type: String
    },
    wrong: {
      required: false,
      type: Boolean
    },
    fixed: {
      required: false,
      type: Boolean
    },
    disabled: {
      required: false,
      type: Boolean
    }
  },
  emits: ["button-click"],
  setup(__props, { emit: emits }) {
    const $t = inject("translate");
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("div", {
        class: normalizeClass({
          "answer-button": true,
          "--active": __props.active,
          "--bounce": __props.wrong,
          "--fixed": __props.fixed,
          "--disabled": __props.disabled
        }),
        onClick: _cache[0] || (_cache[0] = ($event) => emits("button-click"))
      }, [
        (openBlock(), createElementBlock("svg", _hoisted_1$4, [
          _hoisted_2$4,
          __props.active ? (openBlock(), createElementBlock("circle", _hoisted_3$4)) : createCommentVNode("", true),
          __props.fixed ? (openBlock(), createElementBlock("circle", _hoisted_4$4)) : createCommentVNode("", true)
        ])),
        createElementVNode("span", {
          innerHTML: unref($t)(__props.label)
        }, null, 8, _hoisted_5$4)
      ], 2);
    };
  }
};
const _hoisted_1$3 = {
  style: { "overflow": "visible" },
  width: "1028",
  height: "502",
  viewBox: "0 0 1028 502",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};
const _hoisted_2$3 = ["fill"];
const _hoisted_3$3 = ["fill"];
const _hoisted_4$3 = ["fill"];
const _hoisted_5$3 = ["fill"];
const _hoisted_6$3 = ["fill"];
const _hoisted_7$3 = ["fill"];
const _hoisted_8$3 = ["fill"];
const _hoisted_9$3 = ["fill"];
const _hoisted_10$3 = ["fill"];
const _hoisted_11$3 = ["fill"];
const _hoisted_12$3 = ["fill"];
const _hoisted_13$3 = ["fill"];
const _hoisted_14$3 = ["fill"];
const _hoisted_15$3 = ["fill"];
const _hoisted_16$3 = ["fill"];
const _hoisted_17$3 = ["fill"];
const _hoisted_18$3 = ["fill"];
const _hoisted_19$3 = ["fill"];
const _hoisted_20$3 = ["fill"];
const _hoisted_21$3 = ["fill"];
const _hoisted_22$3 = ["fill"];
const _hoisted_23$3 = ["fill"];
const _hoisted_24$3 = /* @__PURE__ */ createStaticVNode('<path d="M990.776 234V232.288H993.368V224.448H994.36L991.032 226.432V224.48L993.96 222.72H995.432V232.288H997.864V234H990.776ZM999.842 234V232.48L1003.59 228.576C1004.06 228.085 1004.4 227.632 1004.63 227.216C1004.85 226.8 1004.96 226.373 1004.96 225.936C1004.96 225.392 1004.79 224.981 1004.45 224.704C1004.11 224.427 1003.61 224.288 1002.96 224.288C1002.44 224.288 1001.94 224.384 1001.46 224.576C1000.98 224.757 1000.51 225.045 1000.05 225.44L999.378 223.904C999.836 223.499 1000.4 223.173 1001.07 222.928C1001.76 222.683 1002.47 222.56 1003.22 222.56C1004.43 222.56 1005.37 222.832 1006.02 223.376C1006.67 223.92 1006.99 224.699 1006.99 225.712C1006.99 226.416 1006.83 227.088 1006.5 227.728C1006.17 228.357 1005.66 229.019 1004.96 229.712L1001.84 232.832V232.288H1007.44V234H999.842ZM1009.32 234V225.92H1010.99L1013.77 230.752L1016.56 225.92H1018.22V234H1016.46V228.16H1016.83L1014.24 232.688H1013.31L1010.72 228.16H1011.08V234H1009.32Z" fill="#080809"></path><path d="M983.292 116.739L137.203 365.58L7.79399 294.842L860.448 43.859L983.292 116.739Z" fill="url(#paint0_linear_476_7307)" stroke="#380BB3" stroke-width="6.00671"></path><mask id="path-26-inside-1_476_7307" fill="white"><path d="M4 373L0.0234375 367V294.041L4 295L137.849 368.468L138.935 366.141L141.823 365.353L148.056 377.079L143.527 378.44L137.849 368.468L4 295V373Z"></path></mask><path d="M4 373L0.0234375 367V294.041L4 295L137.849 368.468L138.935 366.141L141.823 365.353L148.056 377.079L143.527 378.44L137.849 368.468L4 295V373Z" fill="#6C3BF0"></path><path d="M2.55488 297.633L136.404 371.101L139.294 365.835L5.44512 292.367L2.55488 297.633ZM145.056 376.933L141.163 457.109L147.163 457.4L151.056 377.225L145.056 376.933ZM4 373L-1.00689 376.318L10.0067 392.936V373H4ZM0.0234375 367H-5.98327V368.81L-4.98345 370.318L0.0234375 367ZM0.0234375 294.041L1.43164 288.202L-5.98327 286.414V294.041H0.0234375ZM4 295H10.0067V290.27L5.4082 289.161L4 295ZM137.849 368.468L132.406 365.928L131.093 368.742L132.629 371.44L137.849 368.468ZM148.056 377.079L149.785 382.832L156.796 380.725L153.36 374.26L148.056 377.079ZM143.527 378.44L138.308 381.412L140.675 385.569L145.256 384.193L143.527 378.44ZM141.823 365.353L147.128 362.534L144.874 358.295L140.243 359.558L141.823 365.353ZM138.935 366.141L137.354 360.346L134.669 361.078L133.492 363.601L138.935 366.141ZM9.00689 369.682L5.03033 363.682L-4.98345 370.318L-1.00689 376.318L9.00689 369.682ZM6.03014 367V294.041H-5.98327V367H6.03014ZM-1.38476 299.88L2.5918 300.839L5.4082 289.161L1.43164 288.202L-1.38476 299.88ZM-2.00671 295V373H10.0067V295H-2.00671ZM146.327 371.326L141.799 372.687L145.256 384.193L149.785 382.832L146.327 371.326ZM148.747 375.468L143.069 365.496L132.629 371.44L138.308 381.412L148.747 375.468ZM136.519 368.172L142.752 379.898L153.36 374.26L147.128 362.534L136.519 368.172ZM143.293 371.008L144.378 368.68L133.492 363.601L132.406 365.928L143.293 371.008ZM140.515 371.936L143.404 371.148L140.243 359.558L137.354 360.346L140.515 371.936Z" fill="#380BB3" mask="url(#path-26-inside-1_476_7307)"></path><path d="M147.064 454.291L146.478 380.634L996.826 130.867L996.955 199.151L147.064 454.291Z" fill="url(#paint1_linear_476_7307)" stroke="#380BB3" stroke-width="6.00671"></path><path d="M4 295.999V372.861L144.289 456.402L143.592 378.425L137.799 368.306L4 295.999Z" fill="#75797B"></path><path d="M982.5 118.5L990.731 117.687L1000 127L992.5 130L982.5 118.5Z" fill="#380BB3"></path><path d="M143 366.999L983.5 119.499L992.5 129.719L148 376.999L143 366.999Z" fill="#F1F5F6"></path><path d="M213 363V431" stroke="#380BB3" stroke-width="6" stroke-dasharray="7 7"></path><path d="M284 342.5V411" stroke="#380BB3" stroke-width="6" stroke-dasharray="7 7"></path><path d="M355 321.5V390" stroke="#380BB3" stroke-width="6" stroke-dasharray="7 7"></path><path d="M426 300.5V369" stroke="#380BB3" stroke-width="6" stroke-dasharray="7 7"></path><path d="M497 279.5V348" stroke="#380BB3" stroke-width="6" stroke-dasharray="7 7"></path><path d="M923 152.5V221" stroke="#380BB3" stroke-width="6" stroke-dasharray="7 7"></path><path d="M568 258.5V327" stroke="#380BB3" stroke-width="6" stroke-dasharray="7 7"></path><path d="M781 195.5V264" stroke="#380BB3" stroke-width="6" stroke-dasharray="7 7"></path>', 17);
const _hoisted_41$2 = {
  key: 0,
  d: "M355 321V389.5",
  stroke: "#380BB3",
  "stroke-width": "6"
};
const _hoisted_42$2 = {
  key: 1,
  d: "M426 300V368.5",
  stroke: "#380BB3",
  "stroke-width": "6"
};
const _hoisted_43$2 = {
  key: 2,
  d: "M568 258.5V327",
  stroke: "#380BB3",
  "stroke-width": "6"
};
const _hoisted_44$2 = {
  key: 3,
  d: "M710 219V287.5",
  stroke: "#380BB3",
  "stroke-width": "6"
};
const _hoisted_45$2 = /* @__PURE__ */ createElementVNode("path", {
  d: "M639 237.5V306",
  stroke: "#380BB3",
  "stroke-width": "6",
  "stroke-dasharray": "7 7"
}, null, -1);
const _hoisted_46$2 = /* @__PURE__ */ createElementVNode("path", {
  d: "M852 173.5V242",
  stroke: "#380BB3",
  "stroke-width": "6",
  "stroke-dasharray": "7 7"
}, null, -1);
const _hoisted_47$2 = /* @__PURE__ */ createElementVNode("path", {
  d: "M710 216.5V285",
  stroke: "#380BB3",
  "stroke-width": "6",
  "stroke-dasharray": "7 7"
}, null, -1);
const _hoisted_48$2 = {
  key: 4,
  d: "M781 195.5V264",
  stroke: "#380BB3",
  "stroke-width": "6"
};
const _hoisted_49$2 = {
  key: 5,
  d: "M432 169.5L564.5 240.5",
  stroke: "#380BB3",
  "stroke-width": "6"
};
const _hoisted_50$2 = {
  key: 6,
  d: "M289 211L421.5 282",
  stroke: "#380BB3",
  "stroke-width": "6"
};
const _hoisted_51$2 = {
  key: 7,
  d: "M221 231L353.5 302",
  stroke: "#380BB3",
  "stroke-width": "6"
};
const _hoisted_52$2 = {
  key: 8,
  d: "M646 106.5L778.5 177.5",
  stroke: "#380BB3",
  "stroke-width": "6"
};
const _hoisted_53$2 = {
  key: 9,
  d: "M574 127L706.5 198",
  stroke: "#380BB3",
  "stroke-width": "6"
};
const _hoisted_54$2 = {
  key: 10,
  class: "_crane",
  style: { "transform": "translateY(-70px)" },
  d: "M307.671 65V154.838H297.462V244.676H303.587V252.843H299.504V267.136H311.755L303.587 293.679H299.504V310.013H291.337H283.17V293.679H279.086L276.023 280.407L264.794 281.428L244.376 279.386L248.46 252.843L252.543 201.799H226L234.167 128.295V115.963V69.0835V65H307.671Z",
  fill: "#E7DFFC"
};
const _hoisted_55$2 = {
  key: 11,
  class: "_crane",
  style: { "transform": "translateY(-70px)" },
  d: "M289.295 69.0835L288.205 115.963M283.17 252.843H277.044V244.676H285.211L286.209 201.799M283.17 252.843V267.136M283.17 252.843H299.504M283.17 267.136H299.504M283.17 267.136H272.961L276.023 280.407M299.504 267.136V252.843M299.504 267.136H311.755L303.587 293.679H299.504M299.504 252.843H303.587V244.676H297.462V154.838H307.671V65H234.167V69.0835V115.963M258.668 201.799L262.752 118.086M258.668 201.799H252.543M258.668 201.799H277.044M262.752 118.086L234.167 115.963M262.752 118.086L288.205 115.963M234.167 115.963V128.295L226 201.799H252.543M288.205 115.963L286.209 201.799M286.209 201.799H277.044M252.543 201.799L248.46 252.843M248.46 252.843L244.376 279.386L264.794 281.428M248.46 252.843L264.794 255.177M277.044 256.927V201.799M277.044 256.927L264.794 255.177M277.044 256.927L276.023 280.407M264.794 281.428V255.177M264.794 281.428L276.023 280.407M276.023 280.407L279.086 293.679H283.17M283.17 293.679V310.013H291.337M283.17 293.679H299.504M299.504 293.679V310.013H291.337M291.337 310.013V334.514",
  stroke: "#490FE7",
  "stroke-width": "6.00671"
};
const _hoisted_56$2 = {
  key: 12,
  d: "M290.973 265.004L290.866 271.856L222.017 234.902L230.162 232.498L290.973 265.004Z",
  fill: "#FF5C00"
};
const _hoisted_57$2 = {
  key: 13,
  class: "_spark",
  d: "M276 266.05L289.861 267.786L283.428 256.512L291.348 264.834L293.349 251.999L296.06 264.138L302.993 254.721L298.35 265.4L312.014 261.342L296.62 271.978H290.531L276 266.05Z",
  fill: "url(#paint2_radial_476_7307)"
};
const _hoisted_58$2 = {
  key: 14,
  class: "_crane",
  style: { "transform": "translateY(-70px)" },
  d: "M375.671 43V132.838H365.462V222.676H371.587V230.843H367.504V245.136H379.755L371.587 271.679H367.504V288.013H359.337H351.17V271.679H347.086L344.023 258.407L332.794 259.428L312.376 257.386L316.46 230.843L320.543 179.799H294L302.167 106.295V93.9629V47.0835V43H375.671Z",
  fill: "#E7DFFC"
};
const _hoisted_59$2 = {
  key: 15,
  class: "_crane",
  style: { "transform": "translateY(-70px)" },
  d: "M357.295 47.0835L356.205 93.9629M351.17 230.843H345.044V222.676H353.211L354.209 179.799M351.17 230.843V245.136M351.17 230.843H367.504M351.17 245.136H367.504M351.17 245.136H340.961L344.023 258.407M367.504 245.136V230.843M367.504 245.136H379.755L371.587 271.679H367.504M367.504 230.843H371.587V222.676H365.462V132.838H375.671V43H302.167V47.0835V93.9629M326.668 179.799L330.752 96.0861M326.668 179.799H320.543M326.668 179.799H345.044M330.752 96.0861L302.167 93.9629M330.752 96.0861L356.205 93.9629M302.167 93.9629V106.295L294 179.799H320.543M356.205 93.9629L354.209 179.799M354.209 179.799H345.044M320.543 179.799L316.46 230.843M316.46 230.843L312.376 257.386L332.794 259.428M316.46 230.843L332.794 233.177M345.044 234.927V179.799M345.044 234.927L332.794 233.177M345.044 234.927L344.023 258.407M332.794 259.428V233.177M332.794 259.428L344.023 258.407M344.023 258.407L347.086 271.679H351.17M351.17 271.679V288.013H359.337M351.17 271.679H367.504M367.504 271.679V288.013H359.337M359.337 288.013V312.514",
  stroke: "#490FE7",
  "stroke-width": "6.00671"
};
const _hoisted_60$2 = {
  key: 16,
  d: "M358.984 245.062L359.016 251.923L289.954 214.91L298.149 212.5L358.984 245.062Z",
  fill: "#FF5C00"
};
const _hoisted_61$2 = {
  key: 17,
  class: "_spark",
  d: "M344 246.05L357.861 247.786L351.428 236.512L359.348 244.834L361.349 231.999L364.06 244.138L370.993 234.721L366.35 245.4L380.014 241.342L364.62 251.978H358.531L344 246.05Z",
  fill: "url(#paint3_radial_476_7307)"
};
const _hoisted_62$2 = {
  key: 18,
  class: "_crane",
  style: { "transform": "translateY(-70px)" },
  d: "M517.671 4V93.8381H507.462V183.676H513.587V191.843H509.504V206.136H521.755L513.587 232.679H509.504V249.013H501.337H493.17V232.679H489.086L486.023 219.407L474.794 220.428L454.376 218.386L458.46 191.843L462.543 140.799H436L444.167 67.295V54.9629V8.08355V4H517.671Z",
  fill: "#E7DFFC"
};
const _hoisted_63$2 = {
  key: 19,
  class: "_crane",
  style: { "transform": "translateY(-70px)" },
  d: "M499.295 8.08355L498.205 54.9629M493.17 191.843H487.044V183.676H495.211L496.209 140.799M493.17 191.843V206.136M493.17 191.843H509.504M493.17 206.136H509.504M493.17 206.136H482.961L486.023 219.407M509.504 206.136V191.843M509.504 206.136H521.755L513.587 232.679H509.504M509.504 191.843H513.587V183.676H507.462V93.8381H517.671V4H444.167V8.08355V54.9629M468.668 140.799L472.752 57.0861M468.668 140.799H462.543M468.668 140.799H487.044M472.752 57.0861L444.167 54.9629M472.752 57.0861L498.205 54.9629M444.167 54.9629V67.295L436 140.799H462.543M498.205 54.9629L496.209 140.799M496.209 140.799H487.044M462.543 140.799L458.46 191.843M458.46 191.843L454.376 218.386L474.794 220.428M458.46 191.843L474.794 194.177M487.044 195.927V140.799M487.044 195.927L474.794 194.177M487.044 195.927L486.023 219.407M474.794 220.428V194.177M474.794 220.428L486.023 219.407M486.023 219.407L489.086 232.679H493.17M493.17 232.679V249.013H501.337M493.17 232.679H509.504M509.504 232.679V249.013H501.337M501.337 249.013V273.514",
  stroke: "#490FE7",
  "stroke-width": "6.00671"
};
const _hoisted_64$2 = {
  key: 20,
  d: "M501.053 203.065L501.071 209.909L432.287 173.021L440.401 170.588L501.053 203.065Z",
  fill: "#FF5C00"
};
const _hoisted_65$2 = {
  key: 21,
  class: "_spark",
  d: "M486 204.05L499.861 205.786L493.428 194.512L501.348 202.834L503.349 189.999L506.06 202.138L512.993 192.721L508.35 203.4L522.014 199.342L506.62 209.978H500.531L486 204.05Z",
  fill: "url(#paint4_radial_476_7307)"
};
const _hoisted_66$2 = /* @__PURE__ */ createStaticVNode('<defs><linearGradient id="paint0_linear_476_7307" x1="619.284" y1="5.3905" x2="80.8094" y2="338.036" gradientUnits="userSpaceOnUse"><stop stop-color="#E0E3E5"></stop><stop offset="1" stop-color="#9AA3A8"></stop></linearGradient><linearGradient id="paint1_linear_476_7307" x1="640.098" y1="81.7555" x2="152.847" y2="314.182" gradientUnits="userSpaceOnUse"><stop stop-color="#E0E3E5"></stop><stop offset="1" stop-color="#9AA3A8"></stop></linearGradient><radialGradient xmlns="http://www.w3.org/2000/svg" id="paint2_radial_476_7307" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(293.213 268.715) rotate(95.5287) scale(20.7483 22.3101)"><stop offset="0.046875" stop-color="#FFFDE7"></stop><stop offset="0.359375" stop-color="#FFD158"></stop><stop offset="0.84375" stop-color="#E36215"></stop></radialGradient><radialGradient xmlns="http://www.w3.org/2000/svg" id="paint3_radial_476_7307" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(361.213 248.715) rotate(95.5287) scale(20.7483 22.3101)"><stop offset="0.046875" stop-color="#FFFDE7"></stop><stop offset="0.359375" stop-color="#FFD158"></stop><stop offset="0.84375" stop-color="#E36215"></stop></radialGradient><radialGradient xmlns="http://www.w3.org/2000/svg" id="paint4_radial_476_7307" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(503.213 206.715) rotate(95.5287) scale(20.7483 22.3101)"><stop offset="0.046875" stop-color="#FFFDE7"></stop><stop offset="0.359375" stop-color="#FFD158"></stop><stop offset="0.84375" stop-color="#E36215"></stop></radialGradient></defs>', 1);
const _sfc_main$4 = {
  __name: "Process__cut",
  props: {},
  emits: ["finish", "wrongCase", "selected"],
  setup(__props, { emit: emits }) {
    const $t = inject("translate");
    const selectedEdges = ref([]);
    const selectEdge = (num) => {
      if (wrongNum(num))
        emits("wrongCase");
      if (selectedEdges.value.indexOf(num) == -1) {
        selectedEdges.value.push(num);
      }
      if (checkRightSelection())
        emits("finish");
    };
    const wrongNum = (num) => {
      return num != 3 && num != 4 && num != 6 && num != 8 && num != 9;
    };
    const numBGColor = (num) => {
      return selectedEdges.value.indexOf(num) != -1 ? wrongNum(num) ? "#DE234B" : "#6C3BF0" : "#E7DFFC";
    };
    const numTextColor = (num) => {
      return selectedEdges.value.indexOf(num) != -1 ? "#FFF" : "#080809";
    };
    const checkEdgeSelection = (num) => {
      return selectedEdges.value.indexOf(num) != -1;
    };
    const checkRightSelection = () => {
      if (selectedEdges.value.length == 1 && selectedEdges.value.indexOf(6) != -1 || selectedEdges.value.length == 2 && selectedEdges.value.indexOf(4) != -1 && selectedEdges.value.indexOf(8) != -1 || selectedEdges.value.length == 3 && selectedEdges.value.indexOf(3) != -1 && selectedEdges.value.indexOf(9) != -1 && selectedEdges.value.indexOf(6) != -1)
        return true;
      else
        return false;
    };
    const discardEdges = () => {
      selectedEdges.value = [];
    };
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("div", null, [
        createElementVNode("div", {
          class: normalizeClass({
            "step-3__discard": true,
            "button-simple": true,
            "--disabled": selectedEdges.value.length === 0 || checkRightSelection()
          }),
          onClick: discardEdges
        }, toDisplayString(unref($t)("\u0421\u0431\u0440\u043E\u0441\u0438\u0442\u044C")), 3),
        (openBlock(), createElementBlock("svg", _hoisted_1$3, [
          createElementVNode("rect", {
            x: "191.974",
            y: "469.87",
            width: "37",
            height: "31",
            rx: "8",
            transform: "rotate(-33.6968 191.974 469.87)",
            fill: numBGColor(1),
            onClick: _cache[0] || (_cache[0] = ($event) => selectEdge(1)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(1) && checkRightSelection() })
          }, null, 10, _hoisted_2$3),
          createElementVNode("path", {
            d: "M212.312 482.75L211.363 481.326L213.519 479.888L209.169 473.365L209.995 472.815L208.327 476.312L207.244 474.688L208.703 471.599L209.928 470.782L215.236 478.743L217.26 477.393L218.209 478.818L212.312 482.75ZM219.762 477.783L215.279 471.06L216.663 470.137L221.66 472.613L221.296 467.048L222.68 466.125L227.163 472.847L225.699 473.824L222.459 468.965L222.765 468.761L223.121 473.966L222.349 474.481L217.68 472.152L217.986 471.948L221.226 476.806L219.762 477.783Z",
            fill: numTextColor(1),
            onClick: _cache[1] || (_cache[1] = ($event) => selectEdge(1)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(1) && checkRightSelection() })
          }, null, 10, _hoisted_3$3),
          createElementVNode("rect", {
            x: "262.169",
            y: "448.513",
            width: "37",
            height: "31",
            rx: "8",
            transform: "rotate(-33.6968 262.169 448.513)",
            fill: numBGColor(2),
            onClick: _cache[2] || (_cache[2] = ($event) => selectEdge(2)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(2) && checkRightSelection() })
          }, null, 10, _hoisted_4$3),
          createElementVNode("path", {
            d: "M282.068 461.686L281.225 460.421L282.174 455.096C282.292 454.427 282.329 453.858 282.285 453.387C282.24 452.917 282.097 452.5 281.854 452.136C281.552 451.683 281.183 451.436 280.745 451.395C280.307 451.354 279.817 451.514 279.276 451.875C278.841 452.165 278.477 452.523 278.184 452.949C277.886 453.366 277.655 453.866 277.492 454.449L276.081 453.543C276.238 452.952 276.528 452.367 276.951 451.791C277.383 451.208 277.909 450.709 278.53 450.295C279.542 449.62 280.469 449.329 281.313 449.42C282.156 449.512 282.858 449.979 283.421 450.822C283.811 451.408 284.046 452.059 284.126 452.775C284.2 453.482 284.141 454.316 283.949 455.278L283.084 459.604L282.783 459.152L287.442 456.045L288.391 457.469L282.068 461.686ZM289.957 456.425L285.474 449.703L286.859 448.78L291.856 451.255L291.491 445.691L292.876 444.767L297.358 451.49L295.894 452.466L292.654 447.607L292.96 447.403L293.316 452.609L292.544 453.123L287.875 450.794L288.181 450.59L291.421 455.449L289.957 456.425Z",
            fill: numTextColor(2),
            onClick: _cache[3] || (_cache[3] = ($event) => selectEdge(2)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(2) && checkRightSelection() })
          }, null, 10, _hoisted_5$3),
          createElementVNode("rect", {
            x: "332.364",
            y: "427.156",
            width: "37",
            height: "31",
            rx: "8",
            transform: "rotate(-33.6968 332.364 427.156)",
            fill: numBGColor(3),
            onClick: _cache[4] || (_cache[4] = ($event) => selectEdge(3)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(3) && checkRightSelection() })
          }, null, 10, _hoisted_6$3),
          createElementVNode("path", {
            d: "M355.161 438.589C354.513 439.021 353.83 439.343 353.111 439.553C352.386 439.754 351.721 439.813 351.114 439.73L350.812 438.066C351.443 438.108 352.03 438.056 352.574 437.911C353.127 437.76 353.652 437.519 354.149 437.187C354.753 436.785 355.13 436.36 355.281 435.914C355.432 435.467 355.353 435.013 355.045 434.551C354.75 434.108 354.372 433.878 353.914 433.864C353.458 433.834 352.937 434.015 352.352 434.405L350.834 435.417L349.92 434.046L351.291 433.132C351.797 432.794 352.118 432.401 352.254 431.951C352.39 431.501 352.313 431.059 352.023 430.624C351.751 430.216 351.402 430 350.976 429.977C350.558 429.947 350.088 430.107 349.564 430.456C348.624 431.083 348.007 431.956 347.714 433.074L346.303 432.169C346.448 431.56 346.735 430.971 347.164 430.403C347.602 429.829 348.131 429.335 348.752 428.921C349.728 428.27 350.64 427.983 351.486 428.059C352.335 428.121 353.02 428.543 353.54 429.324C353.913 429.883 354.076 430.473 354.029 431.094C353.984 431.701 353.732 432.254 353.27 432.754L353.164 432.594C353.882 432.269 354.561 432.201 355.2 432.39C355.842 432.565 356.374 432.967 356.794 433.597C357.35 434.431 357.485 435.296 357.198 436.193C356.905 437.08 356.226 437.879 355.161 438.589ZM360.152 435.069L355.67 428.346L357.054 427.423L362.051 429.899L361.687 424.334L363.071 423.411L367.554 430.133L366.089 431.11L362.849 426.251L363.156 426.047L363.511 431.252L362.739 431.767L358.07 429.438L358.377 429.234L361.617 434.092L360.152 435.069Z",
            fill: numTextColor(3),
            onClick: _cache[5] || (_cache[5] = ($event) => selectEdge(3)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(3) && checkRightSelection() })
          }, null, 10, _hoisted_7$3),
          createElementVNode("rect", {
            x: "402.559",
            y: "405.799",
            width: "37",
            height: "31",
            rx: "8",
            transform: "rotate(-33.6968 402.559 405.799)",
            fill: numBGColor(4),
            onClick: _cache[6] || (_cache[6] = ($event) => selectEdge(4)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(4) && checkRightSelection() })
          }, null, 10, _hoisted_8$3),
          createElementVNode("path", {
            d: "M426.159 416.504L424.934 414.667L420.741 417.463L419.942 416.265L420.167 406.942L421.551 406.019L425.714 412.262L427.059 411.365L427.929 412.67L426.584 413.566L427.809 415.403L426.159 416.504ZM424.064 413.363L421.25 409.143L421.569 408.93L421.415 415.706L421.149 415.307L424.064 413.363ZM430.347 413.711L425.864 406.989L427.248 406.066L432.245 408.541L431.881 402.977L433.265 402.054L437.748 408.776L436.284 409.752L433.044 404.894L433.35 404.689L433.706 409.895L432.933 410.41L428.265 408.08L428.571 407.876L431.811 412.735L430.347 413.711Z",
            fill: numTextColor(4),
            onClick: _cache[7] || (_cache[7] = ($event) => selectEdge(4)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(4) && checkRightSelection() })
          }, null, 10, _hoisted_9$3),
          createElementVNode("rect", {
            x: "472.754",
            y: "384.441",
            width: "37",
            height: "31",
            rx: "8",
            transform: "rotate(-33.6968 472.754 384.441)",
            fill: numBGColor(5),
            onClick: _cache[8] || (_cache[8] = ($event) => selectEdge(5)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(5) && checkRightSelection() })
          }, null, 10, _hoisted_10$3),
          createElementVNode("path", {
            d: "M495.764 395.733C495.356 396.005 494.918 396.233 494.45 396.416C493.992 396.594 493.532 396.721 493.07 396.798C492.618 396.869 492.193 396.876 491.797 396.82L491.495 395.156C492.119 395.189 492.702 395.14 493.243 395.01C493.788 394.865 494.299 394.632 494.779 394.313C495.355 393.928 495.709 393.487 495.839 392.99C495.972 392.478 495.876 391.978 495.55 391.49C495.219 390.993 494.797 390.697 494.286 390.603C493.768 390.499 493.238 390.628 492.697 390.989C492.315 391.244 492.015 391.553 491.796 391.917C491.58 392.266 491.449 392.674 491.401 393.142L490.27 393.896L486.462 388.185L492.292 384.297L493.233 385.708L489.027 388.513L491.015 391.495L490.603 391.77C490.626 391.306 490.773 390.862 491.042 390.439C491.32 390.01 491.698 389.636 492.178 389.316C492.781 388.914 493.387 388.689 493.997 388.641C494.606 388.594 495.186 388.714 495.736 389.001C496.281 389.279 496.754 389.72 497.157 390.323C497.565 390.935 497.784 391.571 497.814 392.231C497.846 392.876 497.69 393.506 497.344 394.122C497 394.722 496.474 395.259 495.764 395.733ZM500.542 392.354L496.059 385.632L497.444 384.708L502.441 387.184L502.076 381.619L503.461 380.696L507.943 387.419L506.479 388.395L503.239 383.536L503.545 383.332L503.901 388.537L503.129 389.052L498.46 386.723L498.766 386.519L502.006 391.378L500.542 392.354Z",
            fill: numTextColor(5),
            onClick: _cache[9] || (_cache[9] = ($event) => selectEdge(5)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(5) && checkRightSelection() })
          }, null, 10, _hoisted_11$3),
          createElementVNode("rect", {
            x: "542.949",
            y: "363.085",
            width: "37",
            height: "31",
            rx: "8",
            transform: "rotate(-33.6968 542.949 363.085)",
            fill: numBGColor(6),
            onClick: _cache[10] || (_cache[10] = ($event) => selectEdge(6)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(6) && checkRightSelection() })
          }, null, 10, _hoisted_12$3),
          createElementVNode("path", {
            d: "M566.225 374.199C565.045 374.986 563.859 375.187 562.667 374.803C561.483 374.412 560.4 373.48 559.418 372.007C558.708 370.942 558.271 369.932 558.108 368.976C557.94 368.012 558.027 367.133 558.37 366.341C558.722 365.542 559.324 364.858 560.176 364.29C560.682 363.953 561.245 363.712 561.866 363.567C562.482 363.413 563.079 363.386 563.659 363.487L563.939 365.146C563.409 365.063 562.907 365.078 562.43 365.191C561.948 365.294 561.512 365.476 561.121 365.737C560.323 366.269 559.895 366.945 559.839 367.765C559.783 368.584 560.113 369.531 560.829 370.605L561.495 371.603L561.335 371.71C561.104 371.21 560.998 370.723 561.015 370.25C561.027 369.768 561.159 369.321 561.41 368.91C561.656 368.489 562 368.131 562.444 367.836C562.994 367.469 563.568 367.272 564.166 367.245C564.763 367.218 565.333 367.351 565.874 367.644C566.424 367.931 566.895 368.368 567.285 368.953C567.688 369.557 567.911 370.19 567.956 370.852C568.003 371.5 567.883 372.119 567.596 372.707C567.304 373.287 566.847 373.784 566.225 374.199ZM565.227 372.903C565.555 372.684 565.796 372.421 565.95 372.113C566.104 371.805 566.162 371.478 566.123 371.132C566.087 370.771 565.957 370.422 565.732 370.085C565.502 369.739 565.229 369.485 564.916 369.322C564.611 369.153 564.287 369.081 563.944 369.104C563.595 369.119 563.256 369.236 562.928 369.455C562.599 369.674 562.361 369.942 562.213 370.258C562.06 370.566 561.998 370.896 562.027 371.248C562.057 371.6 562.187 371.949 562.418 372.295C562.643 372.633 562.915 372.887 563.234 373.058C563.548 373.221 563.876 373.291 564.22 373.267C564.563 373.243 564.898 373.122 565.227 372.903ZM570.737 370.998L566.255 364.275L567.639 363.352L572.636 365.828L572.272 360.263L573.656 359.34L578.139 366.062L576.674 367.039L573.434 362.18L573.741 361.976L574.096 367.181L573.324 367.696L568.655 365.367L568.962 365.162L572.202 370.021L570.737 370.998Z",
            fill: numTextColor(6),
            onClick: _cache[11] || (_cache[11] = ($event) => selectEdge(6)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(6) && checkRightSelection() })
          }, null, 10, _hoisted_13$3),
          createElementVNode("rect", {
            x: "613.145",
            y: "341.728",
            width: "37",
            height: "31",
            rx: "8",
            transform: "rotate(-33.6968 613.145 341.728)",
            fill: numBGColor(7),
            onClick: _cache[12] || (_cache[12] = ($event) => selectEdge(7)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(7) && checkRightSelection() })
          }, null, 10, _hoisted_14$3),
          createElementVNode("path", {
            d: "M633.15 354.83L632.004 343.248L632.404 343.847L627.425 347.167L626.466 345.729L633.029 341.353L633.881 342.631L635.027 353.578L633.15 354.83ZM640.933 349.64L636.45 342.918L637.834 341.995L642.831 344.47L642.467 338.905L643.851 337.982L648.334 344.705L646.87 345.681L643.63 340.822L643.936 340.618L644.291 345.823L643.519 346.338L638.851 344.009L639.157 343.805L642.397 348.664L640.933 349.64Z",
            fill: numTextColor(7),
            onClick: _cache[13] || (_cache[13] = ($event) => selectEdge(7)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(7) && checkRightSelection() })
          }, null, 10, _hoisted_15$3),
          createElementVNode("rect", {
            x: "683.34",
            y: "320.37",
            width: "37",
            height: "31",
            rx: "8",
            transform: "rotate(-33.6968 683.34 320.37)",
            fill: numBGColor(8),
            onClick: _cache[14] || (_cache[14] = ($event) => selectEdge(8)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(8) && checkRightSelection() })
          }, null, 10, _hoisted_16$3),
          createElementVNode("path", {
            d: "M706.283 331.706C705.183 332.439 704.165 332.778 703.23 332.722C702.304 332.66 701.563 332.212 701.007 331.378C700.569 330.721 700.405 330.042 700.514 329.341C700.624 328.64 700.971 328.036 701.557 327.53L701.743 327.81C701.083 328.07 700.437 328.11 699.804 327.93C699.18 327.744 698.669 327.353 698.273 326.759C697.746 325.969 697.623 325.16 697.904 324.331C698.188 323.488 698.836 322.729 699.848 322.055C700.868 321.374 701.818 321.068 702.696 321.136C703.578 321.189 704.282 321.61 704.809 322.4C705.205 322.995 705.377 323.618 705.324 324.268C705.28 324.913 705.006 325.474 704.503 325.95L704.326 325.684C705.015 325.353 705.695 325.277 706.367 325.458C707.033 325.629 707.584 326.043 708.022 326.7C708.579 327.534 708.703 328.393 708.395 329.278C708.097 330.157 707.393 330.966 706.283 331.706ZM705.396 330.374C706.043 329.942 706.447 329.5 706.607 329.048C706.761 328.586 706.678 328.116 706.358 327.636C706.039 327.157 705.639 326.904 705.16 326.878C704.681 326.851 704.117 327.054 703.469 327.486C702.83 327.912 702.427 328.354 702.258 328.813C702.098 329.265 702.178 329.731 702.498 330.211C702.817 330.69 703.22 330.947 703.705 330.983C704.193 331.003 704.757 330.801 705.396 330.374ZM702.582 326.155C703.132 325.788 703.466 325.385 703.584 324.948C703.712 324.504 703.63 324.064 703.34 323.63C703.056 323.204 702.688 322.968 702.235 322.924C701.785 322.865 701.286 323.019 700.735 323.386C700.194 323.747 699.86 324.149 699.733 324.593C699.608 325.022 699.688 325.449 699.972 325.875C700.262 326.31 700.632 326.557 701.082 326.616C701.54 326.67 702.04 326.516 702.582 326.155ZM711.128 328.283L706.645 321.56L708.03 320.637L713.027 323.113L712.662 317.548L714.047 316.625L718.529 323.347L717.065 324.324L713.825 319.465L714.131 319.261L714.487 324.466L713.715 324.981L709.046 322.652L709.352 322.448L712.592 327.306L711.128 328.283Z",
            fill: numTextColor(8),
            onClick: _cache[15] || (_cache[15] = ($event) => selectEdge(8)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(8) && checkRightSelection() })
          }, null, 10, _hoisted_17$3),
          createElementVNode("rect", {
            x: "753.535",
            y: "299.014",
            width: "37",
            height: "31",
            rx: "8",
            transform: "rotate(-33.6968 753.535 299.014)",
            fill: numBGColor(9),
            onClick: _cache[16] || (_cache[16] = ($event) => selectEdge(9)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(9) && checkRightSelection() })
          }, null, 10, _hoisted_18$3),
          createElementVNode("path", {
            d: "M775.773 310.82C775.267 311.157 774.704 311.398 774.082 311.543C773.455 311.679 772.855 311.702 772.281 311.61L772.001 309.95C772.531 310.033 773.036 310.023 773.519 309.919C774.001 309.816 774.437 309.634 774.828 309.373C775.618 308.846 776.041 308.173 776.097 307.354C776.162 306.528 775.836 305.579 775.12 304.505L774.454 303.507L774.614 303.4C774.848 303.885 774.954 304.372 774.934 304.86C774.913 305.348 774.78 305.802 774.534 306.222C774.292 306.628 773.949 306.978 773.505 307.274C772.964 307.635 772.39 307.832 771.783 307.865C771.186 307.891 770.612 307.761 770.061 307.474C769.514 307.173 769.048 306.733 768.663 306.157C768.255 305.544 768.027 304.914 767.98 304.266C767.941 303.612 768.068 302.996 768.361 302.416C768.648 301.827 769.098 301.328 769.71 300.92C770.899 300.127 772.086 299.926 773.269 300.316C774.455 300.692 775.543 301.621 776.531 303.103C777.235 304.159 777.672 305.169 777.84 306.133C778.018 307.092 777.931 307.971 777.579 308.769C777.227 309.568 776.625 310.252 775.773 310.82ZM773.012 305.641C773.341 305.422 773.582 305.159 773.736 304.851C773.889 304.544 773.951 304.214 773.922 303.862C773.886 303.501 773.756 303.152 773.531 302.815C773.3 302.468 773.028 302.214 772.715 302.051C772.401 301.889 772.073 301.819 771.729 301.843C771.38 301.858 771.042 301.975 770.713 302.194C770.385 302.413 770.147 302.68 769.999 302.997C769.845 303.305 769.783 303.634 769.813 303.986C769.851 304.333 769.986 304.679 770.217 305.025C770.441 305.362 770.709 305.619 771.02 305.797C771.333 305.96 771.662 306.029 772.005 306.005C772.348 305.982 772.684 305.86 773.012 305.641ZM781.323 306.926L776.841 300.204L778.225 299.281L783.222 301.756L782.857 296.192L784.242 295.268L788.725 301.991L787.26 302.967L784.02 298.108L784.326 297.904L784.682 303.11L783.91 303.624L779.241 301.295L779.548 301.091L782.788 305.95L781.323 306.926Z",
            fill: numTextColor(9),
            onClick: _cache[17] || (_cache[17] = ($event) => selectEdge(9)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(9) && checkRightSelection() })
          }, null, 10, _hoisted_19$3),
          createElementVNode("rect", {
            x: "824.57",
            y: "280.417",
            width: "47",
            height: "31",
            rx: "8",
            transform: "rotate(-33.6968 824.57 280.417)",
            fill: numBGColor(10),
            onClick: _cache[18] || (_cache[18] = ($event) => selectEdge(10)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(10) && checkRightSelection() })
          }, null, 10, _hoisted_20$3),
          createElementVNode("path", {
            d: "M844.909 293.297L843.959 291.873L846.116 290.435L841.766 283.912L842.591 283.362L840.923 286.859L839.84 285.235L841.3 282.146L842.525 281.329L847.833 289.29L849.856 287.94L850.806 289.365L844.909 293.297ZM855.496 286.43C854.395 287.164 853.27 287.312 852.119 286.874C850.962 286.428 849.866 285.428 848.83 283.875C847.783 282.304 847.284 280.912 847.334 279.699C847.384 278.486 847.96 277.513 849.06 276.779C850.169 276.039 851.289 275.882 852.419 276.308C853.558 276.728 854.648 277.719 855.69 279.281C856.732 280.843 857.236 282.244 857.204 283.483C857.174 284.708 856.605 285.69 855.496 286.43ZM854.555 285.019C855.158 284.616 855.423 284.043 855.349 283.297C855.269 282.543 854.832 281.571 854.039 280.382C853.246 279.192 852.526 278.429 851.878 278.092C851.224 277.746 850.595 277.774 849.992 278.177C849.397 278.573 849.13 279.143 849.189 279.885C849.251 280.613 849.679 281.571 850.472 282.761C851.265 283.95 851.994 284.726 852.66 285.09C853.328 285.439 853.96 285.415 854.555 285.019ZM860.34 283.007L855.858 276.285L857.242 275.361L862.239 277.837L861.874 272.272L863.259 271.349L867.742 278.072L866.277 279.048L863.037 274.189L863.343 273.985L863.699 279.19L862.927 279.705L858.258 277.376L858.565 277.172L861.805 282.031L860.34 283.007Z",
            fill: numTextColor(10),
            onClick: _cache[19] || (_cache[19] = ($event) => selectEdge(10)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(10) && checkRightSelection() })
          }, null, 10, _hoisted_21$3),
          createElementVNode("rect", {
            x: "903.926",
            y: "256.273",
            width: "47",
            height: "31",
            rx: "8",
            transform: "rotate(-33.6968 903.926 256.273)",
            fill: numBGColor(11),
            onClick: _cache[20] || (_cache[20] = ($event) => selectEdge(11)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(11) && checkRightSelection() })
          }, null, 10, _hoisted_22$3),
          createElementVNode("path", {
            d: "M924.265 269.154L923.315 267.729L925.471 266.291L921.122 259.768L921.947 259.218L920.279 262.715L919.196 261.091L920.655 258.002L921.88 257.186L927.188 265.146L929.212 263.797L930.162 265.221L924.265 269.154ZM932.246 263.831L931.297 262.407L933.453 260.969L929.103 254.446L929.929 253.895L928.261 257.392L927.178 255.768L928.637 252.68L929.862 251.863L935.17 259.823L937.194 258.474L938.143 259.899L932.246 263.831ZM939.696 258.863L935.213 252.141L936.597 251.218L941.594 253.693L941.23 248.129L942.614 247.206L947.097 253.928L945.633 254.904L942.393 250.046L942.699 249.841L943.055 255.047L942.283 255.562L937.614 253.232L937.92 253.028L941.16 257.887L939.696 258.863Z",
            fill: numTextColor(11),
            onClick: _cache[21] || (_cache[21] = ($event) => selectEdge(11)),
            class: normalizeClass({ "_edge-num": true, "--disabled": !checkEdgeSelection(11) && checkRightSelection() })
          }, null, 10, _hoisted_23$3),
          _hoisted_24$3,
          checkEdgeSelection(3) ? (openBlock(), createElementBlock("path", _hoisted_41$2)) : createCommentVNode("", true),
          checkEdgeSelection(4) ? (openBlock(), createElementBlock("path", _hoisted_42$2)) : createCommentVNode("", true),
          checkEdgeSelection(6) ? (openBlock(), createElementBlock("path", _hoisted_43$2)) : createCommentVNode("", true),
          checkEdgeSelection(8) ? (openBlock(), createElementBlock("path", _hoisted_44$2)) : createCommentVNode("", true),
          _hoisted_45$2,
          _hoisted_46$2,
          _hoisted_47$2,
          checkEdgeSelection(9) ? (openBlock(), createElementBlock("path", _hoisted_48$2)) : createCommentVNode("", true),
          checkEdgeSelection(6) && checkRightSelection() ? (openBlock(), createElementBlock("path", _hoisted_49$2)) : createCommentVNode("", true),
          checkEdgeSelection(4) && checkRightSelection() ? (openBlock(), createElementBlock("path", _hoisted_50$2)) : createCommentVNode("", true),
          checkEdgeSelection(3) && checkRightSelection() ? (openBlock(), createElementBlock("path", _hoisted_51$2)) : createCommentVNode("", true),
          checkEdgeSelection(9) && checkRightSelection() ? (openBlock(), createElementBlock("path", _hoisted_52$2)) : createCommentVNode("", true),
          checkEdgeSelection(8) && checkRightSelection() ? (openBlock(), createElementBlock("path", _hoisted_53$2)) : createCommentVNode("", true),
          checkEdgeSelection(3) && checkRightSelection() ? (openBlock(), createElementBlock("path", _hoisted_54$2)) : createCommentVNode("", true),
          checkEdgeSelection(3) && checkRightSelection() ? (openBlock(), createElementBlock("path", _hoisted_55$2)) : createCommentVNode("", true),
          checkEdgeSelection(3) && checkRightSelection() ? (openBlock(), createElementBlock("path", _hoisted_56$2)) : createCommentVNode("", true),
          checkEdgeSelection(3) && checkRightSelection() ? (openBlock(), createElementBlock("path", _hoisted_57$2)) : createCommentVNode("", true),
          checkEdgeSelection(4) && checkRightSelection() ? (openBlock(), createElementBlock("path", _hoisted_58$2)) : createCommentVNode("", true),
          checkEdgeSelection(4) && checkRightSelection() ? (openBlock(), createElementBlock("path", _hoisted_59$2)) : createCommentVNode("", true),
          checkEdgeSelection(4) && checkRightSelection() ? (openBlock(), createElementBlock("path", _hoisted_60$2)) : createCommentVNode("", true),
          checkEdgeSelection(4) && checkRightSelection() ? (openBlock(), createElementBlock("path", _hoisted_61$2)) : createCommentVNode("", true),
          checkEdgeSelection(6) && checkRightSelection() && selectedEdges.value.length == 1 ? (openBlock(), createElementBlock("path", _hoisted_62$2)) : createCommentVNode("", true),
          checkEdgeSelection(6) && checkRightSelection() && selectedEdges.value.length == 1 ? (openBlock(), createElementBlock("path", _hoisted_63$2)) : createCommentVNode("", true),
          checkEdgeSelection(6) && checkRightSelection() && selectedEdges.value.length == 1 ? (openBlock(), createElementBlock("path", _hoisted_64$2)) : createCommentVNode("", true),
          checkEdgeSelection(6) && checkRightSelection() && selectedEdges.value.length == 1 ? (openBlock(), createElementBlock("path", _hoisted_65$2)) : createCommentVNode("", true),
          _hoisted_66$2
        ]))
      ]);
    };
  }
};
const _hoisted_1$2 = { class: "section process" };
const _hoisted_2$2 = {
  key: 0,
  class: "step-1"
};
const _hoisted_3$2 = { class: "headline-24 step-1__title" };
const _hoisted_4$2 = { class: "main-text-16 step-1__text" };
const _hoisted_5$2 = { class: "step-1__base" };
const _hoisted_6$2 = /* @__PURE__ */ createElementVNode("span", { class: "step-1__base__brick" }, null, -1);
const _hoisted_7$2 = ["src"];
const _hoisted_8$2 = ["src"];
const _hoisted_9$2 = { class: "process-question-card process-question-card--1" };
const _hoisted_10$2 = { class: "process-question-card__title" };
const _hoisted_11$2 = { class: "process-question-card__body" };
const _hoisted_12$2 = {
  class: /* @__PURE__ */ normalizeClass({ "process-question-card__body__way": true, "--disabled": true })
};
const _hoisted_13$2 = { class: "process-question-card process-question-card--2" };
const _hoisted_14$2 = { class: "process-question-card__title" };
const _hoisted_15$2 = { class: "process-question-card__body" };
const _hoisted_16$2 = { class: "process-question-card process-question-card--3 --disabled" };
const _hoisted_17$2 = { class: "process-question-card__title" };
const _hoisted_18$2 = { class: "process-question-card__body" };
const _hoisted_19$2 = {
  class: /* @__PURE__ */ normalizeClass({ "process-question-card__body__way": true })
};
const _hoisted_20$2 = {
  class: /* @__PURE__ */ normalizeClass({ "process-question-card__body__way": true })
};
const _hoisted_21$2 = { class: "process-question-card process-question-card--4 --disabled" };
const _hoisted_22$2 = { class: "process-question-card__title" };
const _hoisted_23$2 = { class: "process-question-card__body" };
const _hoisted_24$2 = { class: "process-question-card__body__way" };
const _hoisted_25$2 = { class: "process-question-card__body__way" };
const _hoisted_26$2 = { class: "process-question-card process-question-card--5" };
const _hoisted_27$2 = { class: "process-question-card__title" };
const _hoisted_28$2 = { class: "process-question-card__body" };
const _hoisted_29$2 = { class: "process-question-card__body__way" };
const _hoisted_30$2 = { class: "process-question-card process-question-card--6 --disabled" };
const _hoisted_31$2 = { class: "process-question-card__title" };
const _hoisted_32$2 = { class: "process-question-card__body" };
const _hoisted_33$2 = { class: "process-question-card__body__way" };
const _hoisted_34$2 = { class: "process-question-card__body__way" };
const _hoisted_35$1 = /* @__PURE__ */ createElementVNode("rect", {
  width: "24",
  height: "24",
  rx: "4",
  fill: "#6C3BF0"
}, null, -1);
const _hoisted_36$1 = /* @__PURE__ */ createElementVNode("path", {
  "fill-rule": "evenodd",
  "clip-rule": "evenodd",
  d: "M12 10C10.8954 10 10 10.8954 10 12V18C10 19.1046 10.8954 20 12 20C13.1046 20 14 19.1046 14 18L14 12C14 10.8954 13.1046 10 12 10ZM12 5C10.8954 5 10 5.89543 10 7C10 8.10457 10.8954 9 12 9C13.1046 9 14 8.10457 14 7C14 5.89543 13.1046 5 12 5Z",
  fill: "white"
}, null, -1);
const _hoisted_37$1 = [
  _hoisted_35$1,
  _hoisted_36$1
];
const _hoisted_38$1 = {
  key: 0,
  class: "legend-popup"
};
const _hoisted_39$1 = { class: "legend-popup__data" };
const _hoisted_40$1 = { class: "legend-popup__data__head" };
const _hoisted_41$1 = { class: "legend-popup__data__head" };
const _hoisted_42$1 = { class: "legend-popup__data__head" };
const _hoisted_43$1 = { class: "legend-popup__data__body" };
const _hoisted_44$1 = { class: "legend-popup__data__body" };
const _hoisted_45$1 = { class: "legend-popup__data__body" };
const _hoisted_46$1 = { class: "legend-popup__data__body" };
const _hoisted_47$1 = { class: "legend-popup__data__body" };
const _hoisted_48$1 = { class: "legend-popup__data__body" };
const _hoisted_49$1 = { class: "legend-popup__data__body" };
const _hoisted_50$1 = { class: "legend-popup__data__body" };
const _hoisted_51$1 = { class: "legend-popup__data__body" };
const _hoisted_52$1 = { class: "legend-popup__data__body" };
const _hoisted_53$1 = { class: "legend-popup__data__body" };
const _hoisted_54$1 = { class: "legend-popup__data__body" };
const _hoisted_55$1 = { class: "legend-popup__data__body" };
const _hoisted_56$1 = { class: "legend-popup__data__body" };
const _hoisted_57$1 = { class: "legend-popup__data__body" };
const _hoisted_58$1 = { class: "legend-popup__data__body" };
const _hoisted_59$1 = { class: "legend-popup__data__body" };
const _hoisted_60$1 = { class: "legend-popup__data__body" };
const _hoisted_61$1 = ["src"];
const _hoisted_62$1 = { class: "dialog-final" };
const _hoisted_63$1 = { class: "dialog-final__content" };
const _hoisted_64$1 = { class: "dialog-final__content__title headline-32" };
const _hoisted_65$1 = ["innerHTML"];
const _hoisted_66$1 = {
  key: 1,
  class: "step-2"
};
const _hoisted_67$1 = { class: "process__header panel" };
const _hoisted_68$1 = { class: "process__header__title main-text-16" };
const _hoisted_69$1 = { class: "headline-24" };
const _hoisted_70$1 = { class: "step-2__columns" };
const _hoisted_71$2 = { class: "panel" };
const _hoisted_72$1 = ["innerHTML"];
const _hoisted_73 = ["innerHTML"];
const _hoisted_74 = ["src"];
const _hoisted_75 = {
  class: "panel",
  style: { minHeight: "526px" }
};
const _hoisted_76 = { class: "step-2-quiz" };
const _hoisted_77 = { class: "step-2-quiz__task-1" };
const _hoisted_78 = { class: "_container" };
const _hoisted_79 = { class: "step-2-quiz__task-title" };
const _hoisted_80 = { class: "step-2-quiz__task-text" };
const _hoisted_81$1 = { class: "step-2-quiz__task-answers" };
const _hoisted_82$1 = { class: "step-2-quiz__task-2" };
const _hoisted_83$1 = {
  key: 0,
  class: "_blank"
};
const _hoisted_84$1 = ["src"];
const _hoisted_85$1 = {
  key: 1,
  class: "_container"
};
const _hoisted_86$1 = ["innerHTML"];
const _hoisted_87$1 = { class: "step-2-quiz__task-text" };
const _hoisted_88$1 = ["src"];
const _hoisted_89$1 = { class: "step-2-quiz__task-answers" };
const _hoisted_90$1 = { class: "step-2-quiz__task-3" };
const _hoisted_91$1 = {
  key: 0,
  class: "_blank"
};
const _hoisted_92$1 = ["src"];
const _hoisted_93$1 = {
  key: 1,
  class: "_container"
};
const _hoisted_94$1 = { class: "step-2-quiz__task-title" };
const _hoisted_95$1 = { class: "step-2-quiz__task-text" };
const _hoisted_96$1 = /* @__PURE__ */ createElementVNode("br", null, null, -1);
const _hoisted_97$1 = { class: "step-2-quiz__task-answers" };
const _hoisted_98$1 = {
  key: 0,
  class: "_blank"
};
const _hoisted_99$1 = ["src"];
const _hoisted_100$1 = ["src"];
const _hoisted_101$1 = {
  key: 1,
  class: "_task-4-container"
};
const _hoisted_102$1 = { class: "step-2-quiz__task-title" };
const _hoisted_103$1 = ["innerHTML"];
const _hoisted_104$1 = { class: "step-2-quiz__task-answers" };
const _hoisted_105$1 = {
  key: 2,
  class: "step-3"
};
const _hoisted_106$1 = { class: "step-3__header" };
const _hoisted_107$1 = { class: "panel" };
const _hoisted_108$1 = { class: "main-text-16" };
const _hoisted_109$1 = { class: "headline-24" };
const _hoisted_110$1 = { class: "panel" };
const _hoisted_111$1 = ["innerHTML"];
const _hoisted_112$1 = { class: "step-3__body panel" };
const _hoisted_113$1 = { class: "dialog-final" };
const _hoisted_114$1 = { class: "dialog-final__content" };
const _hoisted_115$1 = { class: "dialog-final__content__title headline-32" };
const _hoisted_116$1 = ["innerHTML"];
const _sfc_main$3 = {
  __name: "Process",
  emits: ["next"],
  setup(__props, { emit }) {
    const $t = inject("translate");
    inject("log");
    const $m = inject("media");
    const taskStep = ref(1);
    const isLegendOpen = ref(false);
    const isStep1ModalOpen = ref(false);
    const isStep3ModalOpen = ref(false);
    const cardWays = ref(["", "", ""]);
    const cardAnswers = ref([0, 0, 0, 0]);
    const cardAnswersFinal = ref([0, 0, 0, 0]);
    const cardCorrectAnswers = [1, 1, 1, 1];
    const wrongAnswer = ref(false);
    const step2Stage = ref(1);
    const step2Task1Answer = ref("");
    const step2Task2Answer = ref("");
    const step2Task3Answer = ref("");
    const step2Task4Answer = ref("");
    const step3Ended = ref(false);
    const toggleLegend = () => {
      isLegendOpen.value = !isLegendOpen.value;
    };
    const hideError = () => {
      wrongAnswer.value = false;
    };
    const showError = () => {
      wrongAnswer.value = true;
    };
    const step1Ended = () => {
      if (cardAnswersFinal.value.reduce((partialSum, a) => partialSum + a, 0) > 3)
        return true;
    };
    const finishStep1 = () => {
      if (step1Ended())
        isStep1ModalOpen.value = true;
    };
    const selectCardWay = (cardNum, way) => {
      cardWays.value[cardNum - 1] = way;
    };
    const setCardAnswer = (cardNum, answer) => {
      wrongAnswer.value = false;
      cardAnswers.value[cardNum - 1] = answer;
    };
    const confirmCardAnswer = (qNum) => {
      wrongAnswer.value = false;
      if (cardAnswers.value[qNum - 1] == cardCorrectAnswers[qNum - 1]) {
        cardAnswersFinal.value[qNum - 1] = cardAnswers.value[qNum - 1];
        cardWays.value[1] = "";
        return true;
      } else
        wrongAnswer.value = true;
    };
    const startStep2 = () => {
      taskStep.value = 2;
    };
    const step2Task1SetAnswer = (num) => {
      step2Task1Answer.value = num;
    };
    const confirmStep2Task1Answer = () => {
      wrongAnswer.value = false;
      if (step2Task1Answer.value === 1) {
        step2Stage.value++;
      } else
        wrongAnswer.value = true;
    };
    const step2Task2SetAnswer = (num) => {
      step2Task2Answer.value = num;
    };
    const confirmStep2Task2Answer = () => {
      wrongAnswer.value = false;
      if (step2Task2Answer.value === 2) {
        step2Stage.value++;
      } else
        wrongAnswer.value = true;
    };
    const step2Task3SetAnswer = (num) => {
      step2Task3Answer.value = num;
    };
    const confirmStep2Task3Answer = () => {
      wrongAnswer.value = false;
      if (step2Task3Answer.value === 2) {
        step2Stage.value++;
      } else
        wrongAnswer.value = true;
    };
    const step2Task4SetAnswer = (num) => {
      step2Task4Answer.value = num;
    };
    const confirmStep2Task4Answer = () => {
      wrongAnswer.value = false;
      if (step2Task4Answer.value === 1) {
        step2Stage.value++;
      } else
        wrongAnswer.value = true;
    };
    const startStep3 = () => {
      taskStep.value = 3;
    };
    const toggleToFinish = () => {
      step3Ended.value = true;
    };
    const finishStep3 = () => {
      if (step3Ended.value)
        isStep3ModalOpen.value = true;
    };
    const nextStep = () => {
      window.localStorage.setItem("metalurg_block", "3");
      emit("next");
    };
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("main", null, [
        createElementVNode("section", _hoisted_1$2, [
          createVNode(Transition, { name: "fade" }, {
            default: withCtx(() => [
              wrongAnswer.value ? (openBlock(), createBlock(_sfc_main$7, {
                key: 0,
                width: 880,
                title: "\u041E\u0439, \u044D\u0442\u043E \u043D\u0435\u0432\u0435\u0440\u043D\u044B\u0439 \u0432\u0430\u0440\u0438\u0430\u043D\u0442!",
                text: "\u0417\u0430\u0433\u043B\u044F\u043D\u0438\u0442\u0435 \u0432 \u0441\u043F\u0440\u0430\u0432\u043E\u0447\u043D\u0438\u043A, \u0435\u0441\u043B\u0438 \u043D\u0435 \u043F\u043E\u043B\u0443\u0447\u0430\u0435\u0442\u0441\u044F \u0432\u044B\u0431\u0440\u0430\u0442\u044C \u043F\u0440\u0430\u0432\u0438\u043B\u044C\u043D\u044B\u0439 \u043E\u0442\u0432\u0435\u0442.",
                error: true,
                onClose: hideError
              }, null, 8, ["text"])) : createCommentVNode("", true)
            ]),
            _: 1
          }),
          taskStep.value === 1 ? (openBlock(), createElementBlock("div", _hoisted_2$2, [
            createElementVNode("div", _hoisted_3$2, toDisplayString(unref($t)("\u0412\u044B\u0431\u0435\u0440\u0438\u0442\u0435 \u0432\u0435\u0440\u043D\u044B\u0435 \u043D\u0430\u0441\u0442\u0440\u043E\u0439\u043A\u0438 \u043F\u0435\u0447\u0438")), 1),
            createElementVNode("div", _hoisted_4$2, toDisplayString(unref($t)(`\u041A\u0430\u0436\u0435\u0442\u0441\u044F, \u0443 \u043D\u0430\u0433\u0440\u0435\u0432\u0430\u0442\u0435\u043B\u044C\u043D\u043E\u0439 \u043F\u0435\u0447\u0438 \u0441\u0442\u043E\u044F\u043B\u0438 \u043D\u0430\u0441\u0442\u0440\u043E\u0439\u043A\u0438 \u0434\u043B\u044F \u0434\u0440\u0443\u0433\u043E\u0439 \u0441\u0442\u0430\u043B\u0438.
          \u041D\u0430\u0436\u0438\u043C\u0430\u0439\u0442\u0435 \u043D\u0430\xA0\u043F\u0430\u0440\u0430\u043C\u0435\u0442\u0440\u044B \u0438\xA0\u0432\u044B\u0431\u0438\u0440\u0430\u0439\u0442\u0435 \u043F\u0440\u0430\u0432\u0438\u043B\u044C\u043D\u044B\u0439 \u0440\u0435\u0436\u0438\u043C \u043D\u0430\u0433\u0440\u0435\u0432\u0430 \u0432\xA0\u043A\u0430\u0436\u0434\u043E\u0439 \u0437\u043E\u043D\u0435 \u043F\u0435\u0447\u0438.
          \u0412\u0430\u043C \u043F\u043E\u043C\u043E\u0436\u0435\u0442 \u0441\u043F\u0440\u0430\u0432\u043E\u0447\u043D\u0438\u043A!`)), 1),
            createElementVNode("div", _hoisted_5$2, [
              _hoisted_6$2,
              createElementVNode("img", {
                src: unref($m)("/images/process-step-1-scheme-top.svg"),
                alt: "scheme 1",
                class: "step-1__base__scheme-top"
              }, null, 8, _hoisted_7$2),
              createElementVNode("img", {
                src: unref($m)("/images/process-step-1-scheme-bottom.svg"),
                alt: "scheme 2",
                class: "step-1__base__scheme-bottom"
              }, null, 8, _hoisted_8$2),
              createElementVNode("div", _hoisted_9$2, [
                createElementVNode("div", _hoisted_10$2, toDisplayString(unref($t)("\u041F\u043E\u0441\u0430\u0434\u043E\u0447\u043D\u043E\u0435 \u043E\u043A\u043D\u043E")), 1),
                createElementVNode("div", _hoisted_11$2, [
                  createElementVNode("div", {
                    class: normalizeClass({ "process-question-card__body__way": true, "--q": !cardAnswersFinal.value[1 - 1] }),
                    onClick: _cache[0] || (_cache[0] = ($event) => selectCardWay(1, "t"))
                  }, toDisplayString(unref($t)(cardAnswersFinal.value[1 - 1] ? "680\u2103" : "???")), 3),
                  createElementVNode("div", _hoisted_12$2, toDisplayString(unref($t)("0\u0447")), 1),
                  cardWays.value[1 - 1] == "t" && !cardAnswersFinal.value[1 - 1] ? (openBlock(), createBlock(_sfc_main$5, {
                    key: 0,
                    label: unref($t)("680\u2103"),
                    active: cardAnswers.value[1 - 1] === 1,
                    onButtonClick: _cache[1] || (_cache[1] = ($event) => setCardAnswer(1, 1))
                  }, null, 8, ["label", "active"])) : createCommentVNode("", true),
                  cardWays.value[1 - 1] == "t" && !cardAnswersFinal.value[1 - 1] ? (openBlock(), createBlock(_sfc_main$5, {
                    key: 1,
                    label: unref($t)("820\u2103"),
                    active: cardAnswers.value[1 - 1] === 2,
                    onButtonClick: _cache[2] || (_cache[2] = ($event) => setCardAnswer(1, 2))
                  }, null, 8, ["label", "active"])) : createCommentVNode("", true),
                  cardWays.value[1 - 1] != "" && !cardAnswersFinal.value[1 - 1] ? (openBlock(), createElementBlock("div", {
                    key: 2,
                    class: "button-simple",
                    onClick: _cache[3] || (_cache[3] = ($event) => confirmCardAnswer(1))
                  }, toDisplayString(unref($t)("\u0412\u044B\u0431\u0440\u0430\u0442\u044C")), 1)) : createCommentVNode("", true)
                ])
              ]),
              createElementVNode("div", _hoisted_13$2, [
                createElementVNode("div", _hoisted_14$2, toDisplayString(unref($t)("\u041C\u0435\u0442\u043E\u0434\u0438\u0447\u0435\u0441\u043A\u0430\u044F \u0437\u043E\u043D\u0430, \u0432\u0435\u0440\u0445")), 1),
                createElementVNode("div", _hoisted_15$2, [
                  createElementVNode("div", {
                    class: normalizeClass({
                      "process-question-card__body__way": true,
                      "--q": !cardAnswersFinal.value[2 - 1],
                      "--disabled": cardWays.value[2 - 1] == "h" && (!cardAnswersFinal.value[2 - 1] || !cardAnswersFinal.value[3 - 1])
                    }),
                    onClick: _cache[4] || (_cache[4] = ($event) => selectCardWay(2, "t"))
                  }, toDisplayString(unref($t)(cardAnswersFinal.value[2 - 1] ? "1000\u2103" : "???")), 3),
                  createElementVNode("div", {
                    class: normalizeClass({
                      "process-question-card__body__way": true,
                      "--q": !cardAnswersFinal.value[3 - 1],
                      "--disabled": cardWays.value[2 - 1] == "t" && (!cardAnswersFinal.value[2 - 1] || !cardAnswersFinal.value[3 - 1])
                    }),
                    onClick: _cache[5] || (_cache[5] = ($event) => selectCardWay(2, "h"))
                  }, toDisplayString(unref($t)(cardAnswersFinal.value[3 - 1] ? "6\u0447" : "???")), 3),
                  cardWays.value[2 - 1] == "t" && !cardAnswersFinal.value[2 - 1] ? (openBlock(), createBlock(_sfc_main$5, {
                    key: 0,
                    label: unref($t)("1000\u2103"),
                    active: cardAnswers.value[2 - 1] === 1,
                    onButtonClick: _cache[6] || (_cache[6] = ($event) => setCardAnswer(2, 1))
                  }, null, 8, ["label", "active"])) : createCommentVNode("", true),
                  cardWays.value[2 - 1] == "t" && !cardAnswersFinal.value[2 - 1] ? (openBlock(), createBlock(_sfc_main$5, {
                    key: 1,
                    label: unref($t)("1400\u2103"),
                    active: cardAnswers.value[2 - 1] === 2,
                    onButtonClick: _cache[7] || (_cache[7] = ($event) => setCardAnswer(2, 2))
                  }, null, 8, ["label", "active"])) : createCommentVNode("", true),
                  cardWays.value[2 - 1] == "h" && !cardAnswersFinal.value[3 - 1] ? (openBlock(), createBlock(_sfc_main$5, {
                    key: 2,
                    label: unref($t)("6\u0447"),
                    active: cardAnswers.value[3 - 1] === 1,
                    onButtonClick: _cache[8] || (_cache[8] = ($event) => setCardAnswer(3, 1))
                  }, null, 8, ["label", "active"])) : createCommentVNode("", true),
                  cardWays.value[2 - 1] == "h" && !cardAnswersFinal.value[3 - 1] ? (openBlock(), createBlock(_sfc_main$5, {
                    key: 3,
                    label: unref($t)("4\u0447"),
                    active: cardAnswers.value[3 - 1] === 2,
                    onButtonClick: _cache[9] || (_cache[9] = ($event) => setCardAnswer(3, 2))
                  }, null, 8, ["label", "active"])) : createCommentVNode("", true),
                  cardWays.value[2 - 1] == "t" && !cardAnswersFinal.value[2 - 1] ? (openBlock(), createElementBlock("div", {
                    key: 4,
                    class: "button-simple",
                    onClick: _cache[10] || (_cache[10] = ($event) => confirmCardAnswer(2))
                  }, toDisplayString(unref($t)("\u0412\u044B\u0431\u0440\u0430\u0442\u044C")), 1)) : createCommentVNode("", true),
                  cardWays.value[2 - 1] == "h" && !cardAnswersFinal.value[3 - 1] ? (openBlock(), createElementBlock("div", {
                    key: 5,
                    class: "button-simple",
                    onClick: _cache[11] || (_cache[11] = ($event) => confirmCardAnswer(3))
                  }, toDisplayString(unref($t)("\u0412\u044B\u0431\u0440\u0430\u0442\u044C")), 1)) : createCommentVNode("", true)
                ])
              ]),
              createElementVNode("div", _hoisted_16$2, [
                createElementVNode("div", _hoisted_17$2, toDisplayString(unref($t)("\u0421\u0432\u0430\u0440\u043E\u0447\u043D\u0430\u044F \u0437\u043E\u043D\u0430, \u0432\u0435\u0440\u0445")), 1),
                createElementVNode("div", _hoisted_18$2, [
                  createElementVNode("div", _hoisted_19$2, toDisplayString(unref($t)("1250\u2103")), 1),
                  createElementVNode("div", _hoisted_20$2, toDisplayString(unref($t)("9\u0447")), 1)
                ])
              ]),
              createElementVNode("div", _hoisted_21$2, [
                createElementVNode("div", _hoisted_22$2, toDisplayString(unref($t)("\u0422\u043E\u043C\u0438\u043B\u044C\u043D\u0430\u044F \u0437\u043E\u043D\u0430")), 1),
                createElementVNode("div", _hoisted_23$2, [
                  createElementVNode("div", _hoisted_24$2, toDisplayString(unref($t)("1250\u2103")), 1),
                  createElementVNode("div", _hoisted_25$2, toDisplayString(unref($t)("9\u0447")), 1)
                ])
              ]),
              createElementVNode("div", _hoisted_26$2, [
                createElementVNode("div", _hoisted_27$2, toDisplayString(unref($t)("\u041C\u0435\u0442\u043E\u0434\u0438\u0447\u0435\u0441\u043A\u0430\u044F \u0437\u043E\u043D\u0430, \u043D\u0438\u0437")), 1),
                createElementVNode("div", _hoisted_28$2, [
                  createElementVNode("div", _hoisted_29$2, toDisplayString(unref($t)("870\u2103")), 1),
                  createElementVNode("div", {
                    class: normalizeClass({ "process-question-card__body__way": true, "--q": !cardAnswersFinal.value[4 - 1] }),
                    onClick: _cache[12] || (_cache[12] = ($event) => selectCardWay(3, "h"))
                  }, toDisplayString(unref($t)(cardAnswersFinal.value[4 - 1] ? "7\u0447" : "???")), 3),
                  cardWays.value[3 - 1] == "h" && !cardAnswersFinal.value[4 - 1] ? (openBlock(), createBlock(_sfc_main$5, {
                    key: 0,
                    label: unref($t)("7\u0447"),
                    active: cardAnswers.value[4 - 1] === 1,
                    onButtonClick: _cache[13] || (_cache[13] = ($event) => setCardAnswer(4, 1))
                  }, null, 8, ["label", "active"])) : createCommentVNode("", true),
                  cardWays.value[3 - 1] == "h" && !cardAnswersFinal.value[4 - 1] ? (openBlock(), createBlock(_sfc_main$5, {
                    key: 1,
                    label: unref($t)("9\u0447"),
                    active: cardAnswers.value[4 - 1] === 2,
                    onButtonClick: _cache[14] || (_cache[14] = ($event) => setCardAnswer(4, 2))
                  }, null, 8, ["label", "active"])) : createCommentVNode("", true),
                  cardWays.value[3 - 1] != "" && !cardAnswersFinal.value[4 - 1] ? (openBlock(), createElementBlock("div", {
                    key: 2,
                    class: "button-simple",
                    onClick: _cache[15] || (_cache[15] = ($event) => confirmCardAnswer(4))
                  }, toDisplayString(unref($t)("\u0412\u044B\u0431\u0440\u0430\u0442\u044C")), 1)) : createCommentVNode("", true)
                ])
              ]),
              createElementVNode("div", _hoisted_30$2, [
                createElementVNode("div", _hoisted_31$2, toDisplayString(unref($t)("\u0421\u0432\u0430\u0440\u043E\u0447\u043D\u0430\u044F \u0437\u043E\u043D\u0430, \u043D\u0438\u0437")), 1),
                createElementVNode("div", _hoisted_32$2, [
                  createElementVNode("div", _hoisted_33$2, toDisplayString(unref($t)("1200\u2103")), 1),
                  createElementVNode("div", _hoisted_34$2, toDisplayString(unref($t)("9\u0447")), 1)
                ])
              ])
            ]),
            createElementVNode("span", {
              class: normalizeClass({ "step-1__next": true, "button-final": true, "button-final--disabled": !step1Ended() }),
              onClick: finishStep1
            }, toDisplayString(unref($t)("\u0413\u043E\u0442\u043E\u0432\u043E")), 3),
            (openBlock(), createElementBlock("svg", {
              class: "step-1__info-activate",
              onClick: toggleLegend,
              xmlns: "http://www.w3.org/2000/svg",
              width: "24",
              height: "24",
              viewBox: "0 0 24 24",
              fill: "none"
            }, _hoisted_37$1)),
            createVNode(Transition, { name: "fade" }, {
              default: withCtx(() => [
                isLegendOpen.value ? (openBlock(), createElementBlock("div", _hoisted_38$1, [
                  createElementVNode("div", _hoisted_39$1, [
                    createElementVNode("div", _hoisted_40$1, toDisplayString(unref($t)("\u0417\u043E\u043D\u0430 \u043F\u0435\u0447\u0438")), 1),
                    createElementVNode("div", _hoisted_41$1, toDisplayString(unref($t)("\u0412\u0440\u0435\u043C\u044F \u043D\u0430\u0433\u0440\u0435\u0432\u0430/\u0447")), 1),
                    createElementVNode("div", _hoisted_42$1, toDisplayString(unref($t)("\u0422\u0435\u043C\u043F\u0435\u0440\u0430\u0442\u0443\u0440\u0430")), 1),
                    createElementVNode("div", _hoisted_43$1, toDisplayString(unref($t)("\u0423 \u043F\u043E\u0441\u0430\u0434\u043E\u0447\u043D\u043E\u0433\u043E \u043E\u043A\u043D\u0430")), 1),
                    createElementVNode("div", _hoisted_44$1, toDisplayString(unref($t)("-")), 1),
                    createElementVNode("div", _hoisted_45$1, toDisplayString(unref($t)("\u041D\u0435 \u0431\u043E\u043B\u0435\u0435 700")), 1),
                    createElementVNode("div", _hoisted_46$1, toDisplayString(unref($t)("\u041C\u0435\u0442\u043E\u0434\u0438\u0447\u0435\u0441\u043A\u0430\u044F \u0432\u0435\u0440\u0445")), 1),
                    createElementVNode("div", _hoisted_47$1, toDisplayString(unref($t)("\u041D\u0435 \u043C\u0435\u043D\u0435\u0435 6,0 \u0447\u0430\u0441\u043E\u0432")), 1),
                    createElementVNode("div", _hoisted_48$1, toDisplayString(unref($t)("850-1030")), 1),
                    createElementVNode("div", _hoisted_49$1, toDisplayString(unref($t)("\u041C\u0435\u0442\u043E\u0434\u0438\u0447\u0435\u0441\u043A\u0430\u044F \u043D\u0438\u0437")), 1),
                    createElementVNode("div", _hoisted_50$1, toDisplayString(unref($t)("\u041D\u0435 \u043C\u0435\u043D\u0435\u0435 6,0 \u0447\u0430\u0441\u043E\u0432")), 1),
                    createElementVNode("div", _hoisted_51$1, toDisplayString(unref($t)("850-1050")), 1),
                    createElementVNode("div", _hoisted_52$1, toDisplayString(unref($t)("\u0421\u0432\u0430\u0440\u043E\u0447\u043D\u0430\u044F \u0432\u0435\u0440\u0445")), 1),
                    createElementVNode("div", _hoisted_53$1, toDisplayString(unref($t)("\u041D\u0435 \u043C\u0435\u043D\u0435\u0435 9,0 \u0447\u0430\u0441\u043E\u0432")), 1),
                    createElementVNode("div", _hoisted_54$1, toDisplayString(unref($t)("1230-1270")), 1),
                    createElementVNode("div", _hoisted_55$1, toDisplayString(unref($t)("\u0421\u0432\u0430\u0440\u043E\u0447\u043D\u0430\u044F \u043D\u0438\u0437")), 1),
                    createElementVNode("div", _hoisted_56$1, toDisplayString(unref($t)("\u041D\u0435 \u043C\u0435\u043D\u0435\u0435 9,0 \u0447\u0430\u0441\u043E\u0432")), 1),
                    createElementVNode("div", _hoisted_57$1, toDisplayString(unref($t)("1240-1290")), 1),
                    createElementVNode("div", _hoisted_58$1, toDisplayString(unref($t)("\u0422\u043E\u043C\u0438\u043B\u044C\u043D\u0430\u044F")), 1),
                    createElementVNode("div", _hoisted_59$1, toDisplayString(unref($t)("\u041D\u0435 \u043C\u0435\u043D\u0435\u0435 3,0 \u0447\u0430\u0441\u043E\u0432")), 1),
                    createElementVNode("div", _hoisted_60$1, toDisplayString(unref($t)("1240-1280")), 1)
                  ]),
                  createElementVNode("img", {
                    src: unref($m)("/images/ui/close.svg"),
                    alt: "Close icon",
                    class: "legend-popup__close",
                    onClick: toggleLegend
                  }, null, 8, _hoisted_61$1)
                ])) : createCommentVNode("", true)
              ]),
              _: 1
            }),
            createVNode(Transition, { name: "fade" }, {
              default: withCtx(() => [
                isStep1ModalOpen.value ? (openBlock(), createBlock(_sfc_main$b, {
                  key: 0,
                  width: 880
                }, {
                  default: withCtx(() => [
                    createElementVNode("div", _hoisted_62$1, [
                      createElementVNode("div", _hoisted_63$1, [
                        createElementVNode("div", _hoisted_64$1, toDisplayString(unref($t)("\u042D\u0442\u043E \u0431\u044B\u043B\u043E \u0433\u043E\u0440\u044F\u0447\u043E!")), 1),
                        createElementVNode("div", {
                          class: "dialog-final__content__text main-text-16",
                          innerHTML: unref($t)(`
                <p>
                  \u0412\u044B \u043E\u0442\u043B\u0438\u0447\u043D\u043E \u0441\u043F\u0440\u0430\u0432\u0438\u043B\u0438\u0441\u044C \u0441 \u043F\u043E\u0434\u0431\u043E\u0440\u043E\u043C \u0442\u0435\u043C\u043F\u0435\u0440\u0430\u0442\u0443\u0440. \u041A\u0441\u0442\u0430\u0442\u0438, \u0432 \u0440\u0435\u0430\u043B\u044C\u043D\u043E\u0441\u0442\u0438 \u043C\u0435\u0442\u0430\u043B\u043B\u0443\u0440\u0433\u0438 \u0442\u043E\u0436\u0435 \u043F\u043E\u043B\u044C\u0437\u0443\u044E\u0442\u0441\u044F \u0441\u043F\u0440\u0430\u0432\u043E\u0447\u043D\u044B\u043C\u0438 \u043C\u0430\u0442\u0435\u0440\u0438\u0430\u043B\u0430\u043C\u0438 \u0438 \u0442\u0430\u0431\u043B\u0438\u0446\u0430\u043C\u0438.
                  </p>
                `)
                        }, null, 8, _hoisted_65$1),
                        createElementVNode("span", {
                          class: "dialog-final__content__btn button-final",
                          onClick: startStep2
                        }, toDisplayString(unref($t)("\u041F\u0440\u043E\u0434\u043E\u043B\u0436\u0438\u0442\u044C")), 1)
                      ]),
                      createElementVNode("div", {
                        class: "dialog-final__art",
                        style: normalizeStyle(`background: url(${unref($m)(
                          "/images/process-modal-1.png"
                        )}) center/cover no-repeat;`)
                      }, null, 4)
                    ])
                  ]),
                  _: 1
                })) : createCommentVNode("", true)
              ]),
              _: 1
            })
          ])) : createCommentVNode("", true),
          taskStep.value === 2 ? (openBlock(), createElementBlock("div", _hoisted_66$1, [
            createElementVNode("div", _hoisted_67$1, [
              createElementVNode("div", _hoisted_68$1, toDisplayString(unref($t)("\u0427\u0430\u0441\u0442\u044C 2")), 1),
              createElementVNode("div", _hoisted_69$1, toDisplayString(unref($t)("\u041F\u043E\u0441\u0447\u0438\u0442\u0430\u0439\u0442\u0435 \u0434\u043B\u0438\u043D\u0443 \u0438\u0442\u043E\u0433\u043E\u0432\u043E\u0433\u043E \u0438\u0437\u0434\u0435\u043B\u0438\u044F")), 1)
            ]),
            createElementVNode("div", _hoisted_70$1, [
              createElementVNode("div", _hoisted_71$2, [
                createElementVNode("div", {
                  class: "step-2__left-text",
                  innerHTML: unref($t)(`<strong>\u0417\u0430\u043A\u043E\u043D \u043F\u043E\u0441\u0442\u043E\u044F\u043D\u0441\u0442\u0432\u0430 \u043E\u0431\u044A\u0451\u043C\u043E\u0432</strong>\xA0\u2014 \u0437\u0430\u043A\u043E\u043D, \u0441\u043E\u0433\u043B\u0430\u0441\u043D\u043E \u043A\u043E\u0442\u043E\u0440\u043E\u043C\u0443 \u043E\u0431\u044A\u0451\u043C \u0442\u0435\u043B\u0430 \u043F\u0440\u0438 \u0435\u0433\u043E \u043F\u043B\u0430\u0441\u0442\u0438\u0447\u0435\u0441\u043A\u043E\u0439 \u0434\u0435\u0444\u043E\u0440\u043C\u0430\u0446\u0438\u0438 \u043E\u0441\u0442\u0430\u0451\u0442\u0441\u044F \u043F\u0440\u0430\u043A\u0442\u0438\u0447\u0435\u0441\u043A\u0438 \u043D\u0435\u0438\u0437\u043C\u0435\u043D\u043D\u044B\u043C, \u0430 \u0437\u043D\u0430\u0447\u0438\u0442 \u043C\u0435\u043D\u044F\u0435\u0442\u0441\u044F \u0442\u043E\u043B\u044C\u043A\u043E \u0444\u043E\u0440\u043C\u0430.`)
                }, null, 8, _hoisted_72$1),
                createElementVNode("div", {
                  class: "step-2__left-order",
                  innerHTML: unref($t)(`<strong>\u0417\u0430\u0433\u043E\u0442\u043E\u0432\u043A\u0430:</strong> 340\u0445340\u04453000 \u043C\u043C<br><strong>\u0417\u0430\u043A\u0430\u0437:</strong> <span>\u043F\u0440\u043E\u043A\u0430\u0442 \u043A\u0432\u0430\u0434\u0440\u0430\u0442\u043D\u043E\u0433\u043E \u043F\u0440\u043E\u0444\u0438\u043B\u044F 170\u0445170 \u043C\u043C, \u0434\u043B\u0438\u043D\u043E\u0439 3-6 \u043C</span>`)
                }, null, 8, _hoisted_73),
                createElementVNode("img", {
                  class: "step-2__left-img",
                  src: unref($m)("/images/process-step-2-img.svg"),
                  alt: "image"
                }, null, 8, _hoisted_74)
              ]),
              createElementVNode("div", _hoisted_75, [
                createElementVNode("div", _hoisted_76, [
                  createElementVNode("div", _hoisted_77, [
                    createElementVNode("div", _hoisted_78, [
                      createElementVNode("div", _hoisted_79, toDisplayString(unref($t)("\u041F\u043E\u0441\u0447\u0438\u0442\u0430\u0439\u0442\u0435 \u043E\u0431\u0449\u0438\u0439 \u043E\u0431\u044A\u0451\u043C \u043C\u0435\u0442\u0430\u043B\u043B\u0438\u0447\u0435\u0441\u043A\u043E\u0439 \u0437\u0430\u0433\u043E\u0442\u043E\u0432\u043A\u0438:")), 1),
                      createElementVNode("div", _hoisted_80, toDisplayString(unref($t)("V\u0437\u0430\u0433 = 340 * 340 * 3000 = ")), 1),
                      createElementVNode("div", _hoisted_81$1, [
                        createVNode(_sfc_main$5, {
                          label: unref($t)("346 800 000 \u043C\u043C<sup>3</sup>"),
                          active: step2Task1Answer.value === 1,
                          fixed: step2Task1Answer.value === 1 && step2Stage.value > 1,
                          onButtonClick: _cache[16] || (_cache[16] = ($event) => step2Task1SetAnswer(1))
                        }, null, 8, ["label", "active", "fixed"]),
                        createVNode(_sfc_main$5, {
                          label: unref($t)("346 800 \u043C\u043C<sup>3</sup>"),
                          active: step2Task1Answer.value === 2,
                          disabled: step2Stage.value > 1,
                          onButtonClick: _cache[17] || (_cache[17] = ($event) => step2Task1SetAnswer(2))
                        }, null, 8, ["label", "active", "disabled"]),
                        createVNode(_sfc_main$5, {
                          label: unref($t)("320 000 000 \u043C\u043C<sup>3</sup>"),
                          active: step2Task1Answer.value === 3,
                          disabled: step2Stage.value > 1,
                          onButtonClick: _cache[18] || (_cache[18] = ($event) => step2Task1SetAnswer(3))
                        }, null, 8, ["label", "active", "disabled"]),
                        createElementVNode("div", {
                          class: normalizeClass({ "button-simple": true, "--disabled": step2Stage.value > 1 }),
                          onClick: confirmStep2Task1Answer
                        }, toDisplayString(unref($t)(step2Stage.value > 1 ? "\u0413\u043E\u0442\u043E\u0432\u043E" : "\u0412\u044B\u0431\u0440\u0430\u0442\u044C")), 3)
                      ])
                    ])
                  ]),
                  createElementVNode("div", _hoisted_82$1, [
                    step2Stage.value < 2 ? (openBlock(), createElementBlock("div", _hoisted_83$1, [
                      createElementVNode("img", {
                        src: unref($m)("/images/process-step-2-blank-task-123.svg"),
                        alt: "image"
                      }, null, 8, _hoisted_84$1)
                    ])) : (openBlock(), createElementBlock("div", _hoisted_85$1, [
                      createElementVNode("div", {
                        class: "step-2-quiz__task-title",
                        innerHTML: unref($t)("\u041D\u0430\u0439\u0434\u0451\u043C \u043A\u043E\u044D\u0444\u0444\u0438\u0446\u0438\u0435\u043D\u0442 \u0434\u0435\u0444\u043E\u0440\u043C\u0430\u0446\u0438\u0438. \u0420\u0430\u0437\u0434\u0435\u043B\u0438\u0442\u0435 \u0440\u0430\u0437\u043C\u0435\u0440\u044B <span>\u0434\u043E \u0434\u0435\u0444\u043E\u0440\u043C\u0430\u0446\u0438\u0438</span> \u043D\u0430 \u0440\u0430\u0437\u043C\u0435\u0440\u044B <span>\u043F\u043E\u0441\u043B\u0435:</span>")
                      }, null, 8, _hoisted_86$1),
                      createElementVNode("div", _hoisted_87$1, [
                        createElementVNode("img", {
                          src: unref($m)("/images/process-step-2-task-2-q.svg"),
                          alt: "image"
                        }, null, 8, _hoisted_88$1)
                      ]),
                      createElementVNode("div", _hoisted_89$1, [
                        createVNode(_sfc_main$5, {
                          label: unref($t)("8"),
                          active: step2Task2Answer.value === 1,
                          disabled: step2Stage.value > 2,
                          onButtonClick: _cache[19] || (_cache[19] = ($event) => step2Task2SetAnswer(1))
                        }, null, 8, ["label", "active", "disabled"]),
                        createVNode(_sfc_main$5, {
                          label: unref($t)("4"),
                          active: step2Task2Answer.value === 2,
                          fixed: step2Task2Answer.value === 2 && step2Stage.value > 2,
                          onButtonClick: _cache[20] || (_cache[20] = ($event) => step2Task2SetAnswer(2))
                        }, null, 8, ["label", "active", "fixed"]),
                        createVNode(_sfc_main$5, {
                          label: unref($t)("2"),
                          active: step2Task2Answer.value === 3,
                          disabled: step2Stage.value > 2,
                          onButtonClick: _cache[21] || (_cache[21] = ($event) => step2Task2SetAnswer(3))
                        }, null, 8, ["label", "active", "disabled"]),
                        createElementVNode("div", {
                          class: normalizeClass({ "button-simple": true, "--disabled": step2Stage.value > 2 }),
                          onClick: confirmStep2Task2Answer
                        }, toDisplayString(unref($t)(step2Stage.value > 2 ? "\u0413\u043E\u0442\u043E\u0432\u043E" : "\u0412\u044B\u0431\u0440\u0430\u0442\u044C")), 3)
                      ])
                    ]))
                  ]),
                  createElementVNode("div", _hoisted_90$1, [
                    step2Stage.value < 3 ? (openBlock(), createElementBlock("div", _hoisted_91$1, [
                      createElementVNode("img", {
                        src: unref($m)("/images/process-step-2-blank-task-123.svg"),
                        alt: "image"
                      }, null, 8, _hoisted_92$1)
                    ])) : (openBlock(), createElementBlock("div", _hoisted_93$1, [
                      createElementVNode("div", _hoisted_94$1, toDisplayString(unref($t)("\u041A\u0430\u043A\u043E\u0439 \u0434\u043B\u0438\u043D\u044B \u043F\u043E\u043B\u0443\u0447\u0438\u0442\u0441\u044F \u0434\u0435\u0442\u0430\u043B\u044C \u043F\u043E\u0441\u043B\u0435 \u043F\u0440\u043E\u043A\u0430\u0442\u0430, \u0435\u0441\u043B\u0438 \u0434\u043B\u0438\u043D\u0430 \u0437\u0430\u0433\u043E\u0442\u043E\u0432\u043A\u0438 \u0440\u0430\u0432\u043D\u044F\u043B\u0430\u0441\u044C 3000 \u043C\u043C?")), 1),
                      createElementVNode("div", _hoisted_95$1, [
                        createTextVNode(toDisplayString(unref($t)("L\u043F\u0440 = L\u0437\u0430\u0433 * \u041A\u0434")), 1),
                        _hoisted_96$1,
                        createTextVNode(" " + toDisplayString(unref($t)("L\u043F\u0440 = 3000 \u043C\u043C * 4 =")), 1)
                      ]),
                      createElementVNode("div", _hoisted_97$1, [
                        createVNode(_sfc_main$5, {
                          label: unref($t)("120 \u043C"),
                          active: step2Task3Answer.value === 1,
                          disabled: step2Stage.value > 3,
                          onButtonClick: _cache[22] || (_cache[22] = ($event) => step2Task3SetAnswer(1))
                        }, null, 8, ["label", "active", "disabled"]),
                        createVNode(_sfc_main$5, {
                          label: unref($t)("12 \u043C"),
                          active: step2Task3Answer.value === 2,
                          fixed: step2Task3Answer.value === 2 && step2Stage.value > 3,
                          onButtonClick: _cache[23] || (_cache[23] = ($event) => step2Task3SetAnswer(2))
                        }, null, 8, ["label", "active", "fixed"]),
                        createVNode(_sfc_main$5, {
                          label: unref($t)("12 000 \u043C"),
                          active: step2Task3Answer.value === 3,
                          disabled: step2Stage.value > 3,
                          onButtonClick: _cache[24] || (_cache[24] = ($event) => step2Task3SetAnswer(3))
                        }, null, 8, ["label", "active", "disabled"]),
                        createElementVNode("div", {
                          class: normalizeClass({ "button-simple": true, "--disabled": step2Stage.value > 3 }),
                          onClick: confirmStep2Task3Answer
                        }, toDisplayString(unref($t)(step2Stage.value > 3 ? "\u0413\u043E\u0442\u043E\u0432\u043E" : "\u0412\u044B\u0431\u0440\u0430\u0442\u044C")), 3)
                      ])
                    ]))
                  ]),
                  createElementVNode("div", {
                    class: normalizeClass({ "step-2-quiz__task-4": true, "step-2-quiz__task-4--active": step2Stage.value >= 4 })
                  }, [
                    step2Stage.value < 4 ? (openBlock(), createElementBlock("div", _hoisted_98$1, [
                      createElementVNode("img", {
                        src: unref($m)("/images/process-step-2-blank-task-4.svg"),
                        alt: "image"
                      }, null, 8, _hoisted_99$1),
                      createElementVNode("img", {
                        class: "_btn",
                        src: unref($m)("/images/process-step-2-blank-task-4-button.svg"),
                        alt: "image"
                      }, null, 8, _hoisted_100$1)
                    ])) : (openBlock(), createElementBlock("div", _hoisted_101$1, [
                      createElementVNode("div", _hoisted_102$1, toDisplayString(unref($t)("\u041F\u0440\u043E\u0432\u0435\u0440\u0438\u043C, \u0432\u0441\u0451 \u043B\u0438 \u043F\u0440\u0430\u0432\u0438\u043B\u044C\u043D\u043E? \u0420\u0430\u0437\u0434\u0435\u043B\u0438\u043C \u043E\u0431\u044A\u0451\u043C \u043D\u0430 \u0434\u043B\u0438\u043D\u0443 \u0438 \u043F\u043E\u043B\u0443\u0447\u0438\u043C \u043F\u043B\u043E\u0449\u0430\u0434\u044C \u0441\u0435\u0447\u0435\u043D\u0438\u044F:")), 1),
                      createElementVNode("div", {
                        class: "step-2-quiz__task-text",
                        innerHTML: unref($t)("346 800 000 \u043C\u043C<sup>3</sup> / 12 000 \u043C\u043C = ")
                      }, null, 8, _hoisted_103$1),
                      createElementVNode("div", _hoisted_104$1, [
                        createVNode(_sfc_main$5, {
                          label: unref($t)("28 900 \u043C\u043C<sup>2</sup>"),
                          active: step2Task4Answer.value === 1,
                          fixed: step2Task4Answer.value === 1 && step2Stage.value > 4,
                          onButtonClick: _cache[25] || (_cache[25] = ($event) => step2Task4SetAnswer(1))
                        }, null, 8, ["label", "active", "fixed"]),
                        createVNode(_sfc_main$5, {
                          label: unref($t)("32 500 \u043C\u043C<sup>2</sup>"),
                          active: step2Task4Answer.value === 2,
                          disabled: step2Stage.value > 4,
                          onButtonClick: _cache[26] || (_cache[26] = ($event) => step2Task4SetAnswer(2))
                        }, null, 8, ["label", "active", "disabled"]),
                        createVNode(_sfc_main$5, {
                          label: unref($t)("46 500 \u043C\u043C<sup>2</sup>"),
                          active: step2Task4Answer.value === 3,
                          disabled: step2Stage.value > 4,
                          onButtonClick: _cache[27] || (_cache[27] = ($event) => step2Task4SetAnswer(3))
                        }, null, 8, ["label", "active", "disabled"]),
                        createElementVNode("div", {
                          class: normalizeClass({ "button-simple": true, "--disabled": step2Stage.value > 4 }),
                          onClick: confirmStep2Task4Answer
                        }, toDisplayString(unref($t)(step2Stage.value > 4 ? "\u0413\u043E\u0442\u043E\u0432\u043E" : "\u041F\u043E\u0434\u0442\u0432\u0435\u0440\u0434\u0438\u0442\u044C")), 3)
                      ])
                    ]))
                  ], 2)
                ])
              ])
            ]),
            step2Stage.value > 4 ? (openBlock(), createBlock(_sfc_main$8, {
              key: 0,
              title: unref($t)("\u0413\u043E\u0442\u043E\u0432\u043E!"),
              text: unref($t)("\u0412\u044B \u043F\u0440\u043E\u0432\u0435\u0440\u0438\u043B\u0438 \u0432\u0441\u0435 \u0440\u0430\u0437\u043C\u0435\u0440\u044B \u0438 \u0443\u0431\u0435\u0434\u0438\u043B\u0438\u0441\u044C, \u0447\u0442\u043E \u043F\u043E\u0441\u043B\u0435 \u043F\u0440\u043E\u043A\u0430\u0442\u0430 \u043C\u0435\u0442\u0430\u043B\u043B\u0438\u0447\u0435\u0441\u043A\u0430\u044F \u0434\u0435\u0442\u0430\u043B\u044C \u0438\u0434\u0435\u0430\u043B\u044C\u043D\u0430. \u0422\u0435\u043F\u0435\u0440\u044C \u0435\u0451 \u043D\u0443\u0436\u043D\u043E \u043D\u0430\u0440\u0435\u0437\u0430\u0442\u044C."),
              label: unref($t)("\u0414\u0430\u043B\u0435\u0435"),
              onLabelClick: startStep3
            }, null, 8, ["title", "text", "label"])) : createCommentVNode("", true)
          ])) : createCommentVNode("", true),
          taskStep.value === 3 ? (openBlock(), createElementBlock("div", _hoisted_105$1, [
            createElementVNode("div", _hoisted_106$1, [
              createElementVNode("div", _hoisted_107$1, [
                createElementVNode("div", _hoisted_108$1, toDisplayString(unref($t)("\u0427\u0430\u0441\u0442\u044C 2")), 1),
                createElementVNode("div", _hoisted_109$1, toDisplayString(unref($t)("\u042D\u0442\u0430\u043F \u0440\u0435\u0437\u043A\u0438")), 1)
              ]),
              createElementVNode("div", _hoisted_110$1, [
                createElementVNode("div", {
                  class: "headline-20",
                  innerHTML: unref($t)("\u041D\u0430\u0440\u0435\u0436\u044C\u0442\u0435 \u043D\u0430 \u0440\u0430\u0432\u043D\u044B\u0435 \u0447\u0430\u0441\u0442\u0438<br> \u043E\u0442 3 \u0434\u043E 6 \u043C\u0435\u0442\u0440\u043E\u0432")
                }, null, 8, _hoisted_111$1)
              ])
            ]),
            createElementVNode("div", _hoisted_112$1, [
              createElementVNode("span", {
                class: normalizeClass({ "step-3__next": true, "button-final": true, "button-final--disabled": !step3Ended.value }),
                onClick: finishStep3
              }, toDisplayString(unref($t)("\u0413\u043E\u0442\u043E\u0432\u043E")), 3),
              createVNode(_sfc_main$4, {
                onFinish: toggleToFinish,
                onWrongCase: showError
              })
            ]),
            createVNode(Transition, { name: "fade" }, {
              default: withCtx(() => [
                isStep3ModalOpen.value ? (openBlock(), createBlock(_sfc_main$b, {
                  key: 0,
                  width: 880
                }, {
                  default: withCtx(() => [
                    createElementVNode("div", _hoisted_113$1, [
                      createElementVNode("div", _hoisted_114$1, [
                        createElementVNode("div", _hoisted_115$1, toDisplayString(unref($t)("\u041A\u0430\u043A\u043E\u0439 \u0445\u043E\u043B\u043E\u0434\u043D\u044B\u0439 \u0440\u0430\u0441\u0447\u0451\u0442!")), 1),
                        createElementVNode("div", {
                          class: "dialog-final__content__text main-text-16",
                          innerHTML: unref($t)(`
                <p>
                  \u041A\u0441\u0442\u0430\u0442\u0438 \u043E \u0445\u043E\u043B\u043E\u0434\u043D\u043E\u043C \u2014 \u043F\u043E\u0441\u043B\u0435 \u043F\u0440\u043E\u043A\u0430\u0442\u0430<br>
                  \u0440\u0430\u0441\u043A\u0430\u043B\u0451\u043D\u043D\u044B\u0439 \u043C\u0435\u0442\u0430\u043B\u043B \u043D\u0430\u0434\u043E \u043E\u0441\u0442\u0443\u0434\u0438\u0442\u044C.<br>
                  \u0414\u0430\u0432\u0430\u0439\u0442\u0435 \u0440\u0435\u0448\u0438\u043C, \u043A\u0430\u043A \u044D\u0442\u043E \u043B\u0443\u0447\u0448\u0435 \u0441\u0434\u0435\u043B\u0430\u0442\u044C?
                </p>
                `)
                        }, null, 8, _hoisted_116$1),
                        createElementVNode("span", {
                          class: "dialog-final__content__btn button-final",
                          onClick: nextStep
                        }, toDisplayString(unref($t)("\u041F\u0440\u043E\u0434\u043E\u043B\u0436\u0438\u0442\u044C")), 1)
                      ]),
                      createElementVNode("div", {
                        class: "dialog-final__art",
                        style: normalizeStyle(`background: url(${unref($m)(
                          "/images/process-modal-3.png"
                        )}) center/cover no-repeat;`)
                      }, null, 4)
                    ])
                  ]),
                  _: 1
                })) : createCommentVNode("", true)
              ]),
              _: 1
            })
          ])) : createCommentVNode("", true)
        ])
      ]);
    };
  }
};
const _hoisted_1$1 = {
  key: 0,
  class: "section cooling"
};
const _hoisted_2$1 = { class: "cooling__left panel" };
const _hoisted_3$1 = { class: "main-text-16" };
const _hoisted_4$1 = { class: "cooling__left__title headline-24" };
const _hoisted_5$1 = { class: "cooling__left__text main-text-16" };
const _hoisted_6$1 = { class: "cooling__left__order main-text-16" };
const _hoisted_7$1 = { class: "_title" };
const _hoisted_8$1 = { class: "cooling__left__hint main-text-16" };
const _hoisted_9$1 = { class: "cooling__right panel" };
const _hoisted_10$1 = /* @__PURE__ */ createStaticVNode('<div class="cooling__right__head"><svg xmlns="http://www.w3.org/2000/svg" width="100%" viewBox="0 0 936 46" fill="none"><path d="M1227.35 43H-116.808L-502.927 3H777.228L1227.35 43Z" fill="white"></path><path d="M1227.35 43H-116.808L-502.927 3H777.228L1227.35 43Z" fill="url(#paint0_linear_354_17171)"></path><path d="M1227.35 43H-116.808L-502.927 3H777.228L1227.35 43Z" stroke="#380BB3" stroke-width="6"></path><defs><linearGradient id="paint0_linear_354_17171" x1="600.28" y1="-103.702" x2="-19.3036" y2="286.168" gradientUnits="userSpaceOnUse"><stop stop-color="#E0E3E5"></stop><stop offset="1" stop-color="#9AA3A8"></stop></linearGradient></defs></svg></div>', 1);
const _hoisted_11$1 = { class: "cooling__right__content" };
const _hoisted_12$1 = { class: "cooling-mark-cards" };
const _hoisted_13$1 = { class: "cooling-mark-cards__items" };
const _hoisted_14$1 = { class: "cooling-mark-cards__items__item" };
const _hoisted_15$1 = { class: "_hover" };
const _hoisted_16$1 = { class: "_num" };
const _hoisted_17$1 = ["innerHTML"];
const _hoisted_18$1 = { class: "cooling-mark-cards__items__item" };
const _hoisted_19$1 = { class: "_hover" };
const _hoisted_20$1 = { class: "_num" };
const _hoisted_21$1 = { class: "_text" };
const _hoisted_22$1 = { class: "cooling-mark-cards__items__item" };
const _hoisted_23$1 = { class: "_hover" };
const _hoisted_24$1 = { class: "_num" };
const _hoisted_25$1 = { class: "_text" };
const _hoisted_26$1 = { class: "cooling-mark-cards__items__item" };
const _hoisted_27$1 = { class: "_hover" };
const _hoisted_28$1 = { class: "_num" };
const _hoisted_29$1 = { class: "_text" };
const _hoisted_30$1 = { class: "cooling-mark-cards__items__item" };
const _hoisted_31$1 = { class: "_hover" };
const _hoisted_32$1 = { class: "_num" };
const _hoisted_33$1 = { class: "_text" };
const _hoisted_34$1 = { class: "cooling-mark-cards__items__item" };
const _hoisted_35 = { class: "_hover" };
const _hoisted_36 = { class: "_num" };
const _hoisted_37 = { class: "_text" };
const _hoisted_38 = { class: "cooling-mark-cards__title" };
const _hoisted_39 = { class: "cooling-mark-cards__text" };
const _hoisted_40 = {
  key: 1,
  class: "section"
};
const _hoisted_41 = { class: "panel cooling-task" };
const _hoisted_42 = { class: "cooling-task__left" };
const _hoisted_43 = { class: "main-text-16" };
const _hoisted_44 = { class: "cooling-task__title headline-24" };
const _hoisted_45 = { class: "cooling-task__order main-text-16" };
const _hoisted_46 = { class: "cooling-task__choose" };
const _hoisted_47 = { class: "cooling-task__choose__title headline-16" };
const _hoisted_48 = { class: "cooling-task__hint" };
const _hoisted_49 = { class: "cooling-task__right" };
const _hoisted_50 = ["src"];
const _hoisted_51 = { class: "cooling-task-info-blocks" };
const _hoisted_52 = { class: "cooling-task-info-blocks__item cooling-task-info-blocks__item--1" };
const _hoisted_53 = { class: "cooling-task-info-blocks__item__tooltip" };
const _hoisted_54 = { class: "headline-16" };
const _hoisted_55 = { class: "headline-12" };
const _hoisted_56 = { class: "cooling-task-info-blocks__item cooling-task-info-blocks__item--2" };
const _hoisted_57 = { class: "cooling-task-info-blocks__item__tooltip" };
const _hoisted_58 = { class: "headline-16" };
const _hoisted_59 = { class: "headline-12" };
const _hoisted_60 = { class: "cooling-task-info-blocks__item cooling-task-info-blocks__item--3" };
const _hoisted_61 = { class: "cooling-task-info-blocks__item__tooltip" };
const _hoisted_62 = { class: "headline-16" };
const _hoisted_63 = { class: "headline-12" };
const _hoisted_64 = { class: "cooling-task-info-blocks__item cooling-task-info-blocks__item--4" };
const _hoisted_65 = { class: "cooling-task-info-blocks__item__tooltip" };
const _hoisted_66 = { class: "headline-16" };
const _hoisted_67 = { class: "headline-12" };
const _hoisted_68 = { class: "dialog-cooling-final" };
const _hoisted_69 = { class: "dialog-cooling-final__content" };
const _hoisted_70 = { class: "dialog-cooling-final__content__title headline-32" };
const _hoisted_71$1 = { class: "dialog-cooling-final__content__text main-text-16" };
const _sfc_main$2 = {
  __name: "Cooling",
  emits: ["next"],
  setup(__props, { emit }) {
    const $t = inject("translate");
    inject("log");
    const $m = inject("media");
    const localStep = ref(1);
    const taskAnswer = ref(0);
    const isModalOpen = ref(false);
    const wrongAnswer = ref(false);
    const startTask = () => {
      localStep.value = 2;
    };
    const setTaskAnswer = (num) => {
      wrongAnswer.value = false;
      taskAnswer.value = num;
    };
    const confirmTaskAnswer = () => {
      wrongAnswer.value = false;
      if (taskAnswer.value !== 3) {
        wrongAnswer.value = true;
        return false;
      } else
        isModalOpen.value = true;
    };
    const nextStep = () => {
      window.localStorage.setItem("metalurg_block", "4");
      emit("next");
    };
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("main", null, [
        localStep.value === 1 ? (openBlock(), createElementBlock("section", _hoisted_1$1, [
          createElementVNode("div", _hoisted_2$1, [
            createElementVNode("div", _hoisted_3$1, toDisplayString(unref($t)("\u0427\u0430\u0441\u0442\u044C 3")), 1),
            createElementVNode("div", _hoisted_4$1, toDisplayString(unref($t)("\u041E\u0445\u043B\u0430\u0436\u0434\u0435\u043D\u0438\u0435")), 1),
            createElementVNode("div", _hoisted_5$1, toDisplayString(unref($t)("\u0412\u0430\u0448\u0430 \u0441\u0442\u0430\u043B\u044C \u0447\u0443\u0432\u0441\u0442\u0432\u0438\u0442\u0435\u043B\u044C\u043D\u0430 \u043A \u0444\u043B\u043E\u043A\u0435\u043D\u0430\u043C \u2014 \u0432\u043D\u0443\u0442\u0440\u0435\u043D\u043D\u0438\u043C \u0434\u0435\u0444\u0435\u043A\u0442\u0430\u043C, \u0432\u043E\u0437\u043D\u0438\u043A\u0430\u044E\u0449\u0438\u043C \u043F\u0440\u0438\xA0\u0440\u0435\u0437\u043A\u043E\u043C \u043D\u0435\u0440\u0430\u0432\u043D\u043E\u043C\u0435\u0440\u043D\u043E\u043C \u043E\u0445\u043B\u0430\u0436\u0434\u0435\u043D\u0438\u0438. \u041E\u0442\u043D\u043E\u0441\u0438\u0442\u0441\u044F \u043A \u043B\u0435\u0433\u0438\u0440\u043E\u0432\u0430\u043D\u043D\u044B\u043C \u0441\u0430\u043C\u043E\u0437\u0430\u043A\u0430\u043B\u0438\u0432\u0430\u044E\u0449\u0438\u043C\u0441\u044F \u043C\u0430\u0440\u043A\u0430\u043C \u0441\u0442\u0430\u043B\u0438.")), 1),
            createElementVNode("div", _hoisted_6$1, [
              createElementVNode("div", _hoisted_7$1, toDisplayString(unref($t)("\u0417\u0430\u043A\u0430\u0437:")), 1),
              createElementVNode("div", null, toDisplayString(unref($t)("\u0441\u0442\u0430\u043B\u044C 4\u04255\u041C\u0424\u0421, 200 \u0442\u043E\u043D\u043D")), 1)
            ]),
            createElementVNode("div", _hoisted_8$1, toDisplayString(unref($t)("\u041D\u0430\u0432\u0435\u0434\u0438\u0442\u0435 \u043C\u044B\u0448\u043A\u043E\u0439 \u043D\u0430\xA0\u0431\u0443\u043A\u0432\u044B \u0438 \u0446\u0438\u0444\u0440\u044B, \u0447\u0442\u043E\u0431\u044B \u0443\u0437\u043D\u0430\u0442\u044C \u0441\u0432\u043E\u0439\u0441\u0442\u0432\u0430 \u0441\u0442\u0430\u043B\u0438")), 1)
          ]),
          createElementVNode("div", _hoisted_9$1, [
            _hoisted_10$1,
            createElementVNode("div", _hoisted_11$1, [
              createElementVNode("div", _hoisted_12$1, [
                createElementVNode("div", _hoisted_13$1, [
                  createElementVNode("div", _hoisted_14$1, [
                    createTextVNode(toDisplayString(unref($t)("4")) + " ", 1),
                    createElementVNode("div", _hoisted_15$1, [
                      createElementVNode("div", _hoisted_16$1, toDisplayString(unref($t)("4")), 1),
                      createElementVNode("div", {
                        class: "_text",
                        style: { minWidth: "258px" },
                        innerHTML: unref($t)("\u041F\u0435\u0440\u0432\u0430\u044F \u0446\u0438\u0444\u0440\u0430 \u0443\u043A\u0430\u0437\u044B\u0432\u0430\u0435\u0442 \u043C\u0430\u0441\u0441\u043E\u0432\u0443\u044E \u0434\u043E\u043B\u044E <strong>\u0443\u0433\u043B\u0435\u0440\u043E\u0434\u0430</strong> \u0432 \u0434\u0435\u0441\u044F\u0442\u044B\u0445 \u0434\u043E\u043B\u044F\u0445 \u043F\u0440\u043E\u0446\u0435\u043D\u0442\u0430, \u0442.\u0435.\xA0\u043C\u0430\u0441\u0441\u043E\u0432\u0430\u044F \u0434\u043E\u043B\u044F \u0443\u0433\u043B\u0435\u0440\u043E\u0434\u0430 \u0432 \u0441\u0442\u0430\u043B\u0438 \u043F\u0440\u0438\u043C\u0435\u0440\u043D\u043E \u0440\u0430\u0432\u043D\u0430 0,4%")
                      }, null, 8, _hoisted_17$1)
                    ])
                  ]),
                  createElementVNode("div", _hoisted_18$1, [
                    createTextVNode(toDisplayString(unref($t)("\u0425")) + " ", 1),
                    createElementVNode("div", _hoisted_19$1, [
                      createElementVNode("div", _hoisted_20$1, toDisplayString(unref($t)("\u0425")), 1),
                      createElementVNode("div", _hoisted_21$1, toDisplayString(unref($t)("C\u0442\u0430\u043B\u044C \u043B\u0435\u0433\u0438\u0440\u043E\u0432\u0430\u043D\u0430 \u0445\u0440\u043E\u043C\u043E\u043C")), 1)
                    ])
                  ]),
                  createElementVNode("div", _hoisted_22$1, [
                    createTextVNode(toDisplayString(unref($t)("5")) + " ", 1),
                    createElementVNode("div", _hoisted_23$1, [
                      createElementVNode("div", _hoisted_24$1, toDisplayString(unref($t)("5")), 1),
                      createElementVNode("div", _hoisted_25$1, toDisplayString(unref($t)("\u041C\u0430\u0441\u0441\u043E\u0432\u0430\u044F \u0434\u043E\u043B\u044F  \u0445\u0440\u043E\u043C\u0430 \u2248 5%")), 1)
                    ])
                  ]),
                  createElementVNode("div", _hoisted_26$1, [
                    createTextVNode(toDisplayString(unref($t)("\u041C")) + " ", 1),
                    createElementVNode("div", _hoisted_27$1, [
                      createElementVNode("div", _hoisted_28$1, toDisplayString(unref($t)("\u041C")), 1),
                      createElementVNode("div", _hoisted_29$1, toDisplayString(unref($t)("\u0421\u0442\u0430\u043B\u044C \u043B\u0435\u0433\u0438\u0440\u043E\u0432\u0430\u043D\u0430 \u043C\u043E\u043B\u0438\u0431\u0434\u0435\u043D\u043E\u043C")), 1)
                    ])
                  ]),
                  createElementVNode("div", _hoisted_30$1, [
                    createTextVNode(toDisplayString(unref($t)("\u0424")) + " ", 1),
                    createElementVNode("div", _hoisted_31$1, [
                      createElementVNode("div", _hoisted_32$1, toDisplayString(unref($t)("\u0424")), 1),
                      createElementVNode("div", _hoisted_33$1, toDisplayString(unref($t)("\u0421\u0442\u0430\u043B\u044C \u043B\u0435\u0433\u0438\u0440\u043E\u0432\u0430\u043D\u0430 \u0432\u0430\u043D\u0430\u0434\u0438\u0435\u043C")), 1)
                    ])
                  ]),
                  createElementVNode("div", _hoisted_34$1, [
                    createTextVNode(toDisplayString(unref($t)("\u0421")) + " ", 1),
                    createElementVNode("div", _hoisted_35, [
                      createElementVNode("div", _hoisted_36, toDisplayString(unref($t)("\u0421")), 1),
                      createElementVNode("div", _hoisted_37, toDisplayString(unref($t)("\u0421\u0442\u0430\u043B\u044C \u043B\u0435\u0433\u0438\u0440\u043E\u0432\u0430\u043D\u0430 \u043A\u0440\u0435\u043C\u043D\u0438\u0435\u043C")), 1)
                    ])
                  ])
                ]),
                createElementVNode("div", _hoisted_38, toDisplayString(unref($t)("\u041C\u0430\u0440\u043A\u0430 \u0441\u0442\u0430\u043B\u0438")), 1),
                createElementVNode("div", _hoisted_39, toDisplayString(unref($t)("\u041A\u0430\u0436\u0434\u0430\u044F \u0431\u0443\u043A\u0432\u0430 \u043E\u0431\u043E\u0437\u043D\u0430\u0447\u0430\u0435\u0442 \u043A\u0430\u043A\u043E\u0439-\u0442\u043E \u044D\u043B\u0435\u043C\u0435\u043D\u0442, \u0446\u0438\u0444\u0440\u0430 \u2014 \u0435\u0433\u043E \u043E\u0431\u044A\u0451\u043C \u0432\xA0\u043C\u0435\u0442\u0430\u043B\u043B\u0435. \u0415\u0441\u043B\u0438 \u0432\u0441\u043B\u0435\u0434 \u0437\u0430\xA0\u0431\u0443\u043A\u0432\u043E\u0439 \u043D\u0435\u0442 \u0446\u0438\u0444\u0440\u044B, \u044D\u0442\u043E \u043E\u0437\u043D\u0430\u0447\u0430\u0435\u0442, \u0447\u0442\u043E \u043C\u0430\u0441\u0441\u043E\u0432\u0430\u044F \u0434\u043E\u043B\u044F \u044D\u0442\u043E\u0433\u043E \u043B\u0435\u0433\u0438\u0440\u0443\u044E\u0449\u0435\u0433\u043E \u044D\u043B\u0435\u043C\u0435\u043D\u0442\u0430 \u043F\u0440\u0438\u043C\u0435\u0440\u043D\u043E \u0440\u0430\u0432\u043D\u0430 1%.")), 1)
              ])
            ]),
            createElementVNode("span", {
              class: "cooling__continue-btn button-final",
              onClick: startTask
            }, toDisplayString(unref($t)("\u041F\u0440\u043E\u0434\u043E\u043B\u0436\u0438\u0442\u044C")), 1)
          ])
        ])) : createCommentVNode("", true),
        localStep.value === 2 ? (openBlock(), createElementBlock("section", _hoisted_40, [
          createElementVNode("div", _hoisted_41, [
            createElementVNode("div", _hoisted_42, [
              createElementVNode("div", _hoisted_43, toDisplayString(unref($t)("\u0427\u0430\u0441\u0442\u044C 3")), 1),
              createElementVNode("div", _hoisted_44, toDisplayString(unref($t)("\u041E\u0445\u043B\u0430\u0436\u0434\u0435\u043D\u0438\u0435")), 1),
              createElementVNode("div", _hoisted_45, [
                createElementVNode("strong", null, toDisplayString(unref($t)("\u0417\u0430\u043A\u0430\u0437:")), 1),
                createTextVNode(" " + toDisplayString(unref($t)("\u0441\u0442\u0430\u043B\u044C 4\u04255\u041C\u0424\u0421, 200 \u0442\u043E\u043D\u043D")), 1)
              ]),
              createElementVNode("div", _hoisted_46, [
                createElementVNode("div", _hoisted_47, toDisplayString(unref($t)("\u0412\u044B\u0431\u0435\u0440\u0438\u0442\u0435 \u043E\u0434\u0438\u043D \u0438\u0437 \u0441\u043F\u043E\u0441\u043E\u0431\u043E\u0432 \u043E\u0445\u043B\u0430\u0436\u0434\u0435\u043D\u0438\u044F")), 1),
                createVNode(_sfc_main$5, {
                  label: unref($t)("\u041E\u0445\u043B\u0430\u0436\u0434\u0435\u043D\u0438\u0435 \u043D\u0430 \u0432\u043E\u0437\u0434\u0443\u0445\u0435"),
                  active: taskAnswer.value === 1,
                  wrong: wrongAnswer.value && taskAnswer.value === 1,
                  onButtonClick: _cache[0] || (_cache[0] = ($event) => setTaskAnswer(1))
                }, null, 8, ["label", "active", "wrong"]),
                createVNode(_sfc_main$5, {
                  label: unref($t)("\u0412 \u0441\u0442\u0443\u0434\u0451\u043D\u043E\u0439 \u0432\u043E\u0434\u0435"),
                  active: taskAnswer.value === 2,
                  wrong: wrongAnswer.value && taskAnswer.value === 2,
                  onButtonClick: _cache[1] || (_cache[1] = ($event) => setTaskAnswer(2))
                }, null, 8, ["label", "active", "wrong"]),
                createVNode(_sfc_main$5, {
                  label: unref($t)("\u041E\u0445\u043B\u0430\u0436\u0434\u0435\u043D\u0438\u0435 \u0432 \u043A\u043E\u043B\u043E\u0434\u0446\u0430\u0445"),
                  active: taskAnswer.value === 3,
                  onButtonClick: _cache[2] || (_cache[2] = ($event) => setTaskAnswer(3))
                }, null, 8, ["label", "active"]),
                createVNode(_sfc_main$5, {
                  label: unref($t)("\u0412 \u0442\u0435\u0440\u043C\u043E\u043F\u0435\u0447\u0438"),
                  active: taskAnswer.value === 4,
                  wrong: wrongAnswer.value && taskAnswer.value === 4,
                  onButtonClick: _cache[3] || (_cache[3] = ($event) => setTaskAnswer(4))
                }, null, 8, ["label", "active", "wrong"]),
                createElementVNode("div", {
                  class: "button-simple",
                  onClick: confirmTaskAnswer
                }, toDisplayString(unref($t)("\u0412\u044B\u0431\u0440\u0430\u0442\u044C")), 1)
              ]),
              createElementVNode("div", _hoisted_48, toDisplayString(unref($t)("\u041D\u0430\u0436\u0438\u043C\u0438\u0442\u0435 \u043D\u0430 \u043A\u0430\u0436\u0434\u044B\u0439 \u0441\u043F\u043E\u0441\u043E\u0431, \u0447\u0442\u043E\u0431\u044B \u0443\u0437\u043D\u0430\u0442\u044C \u043E \u043D\u0451\u043C \u043F\u043E\u0434\u0440\u043E\u0431\u043D\u0435\u0435 ")), 1)
            ]),
            createElementVNode("div", _hoisted_49, [
              createElementVNode("img", {
                src: unref($m)("/images/cooling-space.svg")
              }, null, 8, _hoisted_50),
              createElementVNode("div", _hoisted_51, [
                createElementVNode("div", _hoisted_52, [
                  createElementVNode("div", _hoisted_53, [
                    createElementVNode("div", _hoisted_54, toDisplayString(unref($t)("\u0412 \u0441\u0442\u0443\u0434\u0451\u043D\u043E\u0439 \u0432\u043E\u0434\u0435")), 1),
                    createElementVNode("div", _hoisted_55, toDisplayString(unref($t)(`\u0412\u043E\u0434\u043E\u0439 \u0438 \u0432\u043E\u0434\u043D\u044B\u043C\u0438 \u0440\u0430\u0441\u0442\u0432\u043E\u0440\u0430\u043C\u0438 \u0434\u043B\u044F \u043E\u0445\u043B\u0430\u0436\u0434\u0435\u043D\u0438\u044F \u043F\u043E\u043B\u044C\u0437\u0443\u044E\u0442\u0441\u044F,
                  \u0437\u0430\u043A\u0430\u043B\u0438\u0432\u0430\u044F \u0438\u0437\u0434\u0435\u043B\u0438\u044F \u0438\u0437 \u0443\u0433\u043B\u0435\u0440\u043E\u0434\u0438\u0441\u0442\u044B\u0445 \u0441\u0442\u0430\u043B\u0435\u0439, \u0434\u0438\u0430\u043C\u0435\u0442\u0440
                  \u0438\u043B\u0438 \u0442\u043E\u043B\u0449\u0438\u043D\u0430 \u043A\u043E\u0442\u043E\u0440\u044B\u0445 \u043D\u0430\u0445\u043E\u0434\u0438\u0442\u0441\u044F \u0432 \u043F\u0440\u0435\u0434\u0435\u043B\u0430\u0445 8\u201312 \u043C\u043C.`)), 1)
                  ])
                ]),
                createElementVNode("div", _hoisted_56, [
                  createElementVNode("div", _hoisted_57, [
                    createElementVNode("div", _hoisted_58, toDisplayString(unref($t)("\u0412 \u0442\u0435\u0440\u043C\u043E\u043F\u0435\u0447\u0438")), 1),
                    createElementVNode("div", _hoisted_59, toDisplayString(unref($t)(`\u042D\u0442\u043E\u0442 \u0441\u043F\u043E\u0441\u043E\u0431 \u043F\u043E \u0441\u0432\u043E\u0435\u043C\u0443 \u043F\u0440\u0438\u043D\u0446\u0438\u043F\u0443 \u0431\u043B\u0438\u0436\u0435 \u043A \u0442\u0435\u0440\u043C\u043E\u043E\u0431\u0440\u0430\u0431\u043E\u0442\u043A\u0435.
                  \u041F\u0440\u0438\u043C\u0435\u043D\u044F\u0435\u0442\u0441\u044F \u0434\u043B\u044F \u043E\u0445\u043B\u0430\u0436\u0434\u0435\u043D\u0438\u044F \u043D\u0435\u0431\u043E\u043B\u044C\u0448\u043E\u0433\u043E \u043A\u043E\u043B\u0438\u0447\u0435\u0441\u0442\u0432\u0430 \u043F\u0440\u043E\u043A\u0430\u0442\u0430 (\u0434\u043E 20 \u0442\u043E\u043D\u043D) \u043D\u0430 \u043C\u0430\u043B\u044B\u0445 \u0441\u0442\u0430\u043D\u0430\u0445,
                  \u0442\u0440\u0435\u0431\u0443\u0435\u0442 \u0431\u043E\u043B\u044C\u0448\u0438\u0445 \u0437\u0430\u0442\u0440\u0430\u0442 \u044D\u043D\u0435\u0440\u0433\u0438\u0438 \u043D\u0430 \u043F\u043E\u0434\u0434\u0435\u0440\u0436\u0430\u043D\u0438\u0435 \u0442\u0435\u043C\u043F\u0435\u0440\u0430\u0442\u0443\u0440\u044B \u0432 \u043F\u0435\u0447\u0438.`)), 1)
                  ])
                ]),
                createElementVNode("div", _hoisted_60, [
                  createElementVNode("div", _hoisted_61, [
                    createElementVNode("div", _hoisted_62, toDisplayString(unref($t)("\u041E\u0445\u043B\u0430\u0436\u0434\u0435\u043D\u0438\u0435 \u043D\u0430 \u0432\u043E\u0437\u0434\u0443\u0445\u0435")), 1),
                    createElementVNode("div", _hoisted_63, toDisplayString(unref($t)(`\u0418\u0441\u043F\u043E\u043B\u044C\u0437\u0443\u044E\u0442 \u0434\u043B\u044F \u043D\u0435\u043B\u0435\u0433\u0438\u0440\u043E\u0432\u0430\u043D\u043D\u044B\u0445 \u0438 \u043D\u0438\u0437\u043A\u043E\u043B\u0435\u0433\u0438\u0440\u043E\u0432\u0430\u043D\u043D\u044B\u0445 \u043C\u0430\u0440\u043E\u043A \u0441\u0442\u0430\u043B\u0438, \u043D\u0430\u043F\u0440\u0438\u043C\u0435\u0440,
                  3\u0421\u041F, \u042130, \u042145. \u0411\u043E\u043B\u044C\u0448\u0438\u043D\u0441\u0442\u0432\u043E \u0440\u044F\u0434\u043E\u0432\u044B\u0445 \u0441\u0442\u0430\u043B\u0435\u0439 \u043E\u0445\u043B\u0430\u0436\u0434\u0430\u0435\u0442\u0441\u044F \u043D\u0430 \u0432\u043E\u0437\u0434\u0443\u0445\u0435.
                  \u0421\u043A\u043E\u0440\u043E\u0441\u0442\u044C \u043E\u0445\u043B\u0430\u0436\u0434\u0435\u043D\u0438\u044F \u043D\u0430 \u043A\u0430\u0447\u0435\u0441\u0442\u0432\u043E \u0442\u0430\u043A\u043E\u0439 \u0441\u0442\u0430\u043B\u0438 \u043D\u0435 \u0432\u043B\u0438\u044F\u0435\u0442,
                  \u0433\u043B\u0430\u0432\u043D\u043E\u0435 \u2014 \u043F\u043E\u043B\u043E\u0436\u0438\u0442\u044C \u043F\u0440\u043E\u043A\u0430\u0442 \u043D\u0430 \u0441\u0443\u0445\u0443\u044E \u0437\u0435\u043C\u043B\u044E.`)), 1)
                  ])
                ]),
                createElementVNode("div", _hoisted_64, [
                  createElementVNode("div", _hoisted_65, [
                    createElementVNode("div", _hoisted_66, toDisplayString(unref($t)("\u041E\u0445\u043B\u0430\u0436\u0434\u0435\u043D\u0438\u0435 \u0432 \u043A\u043E\u043B\u043E\u0434\u0446\u0430\u0445")), 1),
                    createElementVNode("div", _hoisted_67, toDisplayString(unref($t)(`\u042D\u0442\u043E\u0442 \u0441\u043F\u043E\u0441\u043E\u0431 \u0438\u0441\u043F\u043E\u043B\u044C\u0437\u0443\u044E\u0442 \u0434\u043B\u044F \u0441\u0442\u0430\u043B\u0438 \u0441 \u0432\u044B\u0441\u043E\u043A\u043E\u0439 \u0441\u043A\u043B\u043E\u043D\u043D\u043E\u0441\u0442\u044C\u044E \u043A\xA0\u043E\u0431\u0440\u0430\u0437\u043E\u0432\u0430\u043D\u0438\u044E \u0444\u043B\u043E\u043A\u0435\u043D\u043E\u0432.
                  \u041A\u043E\u043B\u043E\u0434\u0435\u0446\xA0\u2014 \u043F\u043E \u0441\u0443\u0442\u0438 \u044F\u043C\u0430 \u0441\xA0\u043A\u0440\u044B\u0448\u043A\u043E\u0439, \u0441\u043E\u0437\u0434\u0430\u044E\u0449\u0430\u044F \u044D\u0444\u0444\u0435\u043A\u0442 \u201C\u0442\u0435\u0440\u043C\u043E\u0441\u0430\u201D, \u0438 \u043E\u0445\u043B\u0430\u0436\u0434\u0435\u043D\u0438\u0435 \u0432\xA0\u043D\u0451\u043C \u0438\u0434\u0451\u0442 \u043C\u0435\u0434\u043B\u0435\u043D\u043D\u0435\u0435,
                  \u0447\u0435\u043C \u043D\u0430 \u0432\u043E\u0437\u0434\u0443\u0445\u0435.`)), 1)
                  ])
                ])
              ])
            ])
          ])
        ])) : createCommentVNode("", true),
        createVNode(Transition, { name: "fade" }, {
          default: withCtx(() => [
            isModalOpen.value ? (openBlock(), createBlock(_sfc_main$b, {
              key: 0,
              width: 880,
              class: "modal-cooling"
            }, {
              default: withCtx(() => [
                createElementVNode("div", _hoisted_68, [
                  createElementVNode("div", _hoisted_69, [
                    createElementVNode("div", _hoisted_70, toDisplayString(unref($t)("\u0412 \u0442\u043E\u0447\u043A\u0443!")), 1),
                    createElementVNode("div", _hoisted_71$1, [
                      createElementVNode("p", null, toDisplayString(unref($t)("\u0422\u043E\u043B\u044C\u043A\u043E \u0432\u043E\u0442 \u043F\u043E\u0441\u043B\u0435 \u043E\u0445\u043B\u0430\u0436\u0434\u0435\u043D\u0438\u044F \u0441\u0442\u0430\u043B\u044C \u043D\u0443\u0436\u043D\u043E \u0441\u043D\u043E\u0432\u0430\u2026 \u043D\u0430\u0433\u0440\u0435\u0432\u0430\u0442\u044C! \u0422\u043E\u043B\u044C\u043A\u043E \u0442\u0430\u043A \u043C\u043E\u0436\u043D\u043E \u0441\u043D\u044F\u0442\u044C \u0432\u043D\u0443\u0442\u0440\u0435\u043D\u043D\u0435\u0435 \u043D\u0430\u043F\u0440\u044F\u0436\u0435\u043D\u0438\u0435 \u043C\u0435\u0442\u0430\u043B\u043B\u0430 \u0438 \u0434\u043E\u0431\u0438\u0442\u044C\u0441\u044F \u043D\u0443\u0436\u043D\u043E\u0439 \u0442\u0432\u0451\u0440\u0434\u043E\u0441\u0442\u0438.")), 1)
                    ]),
                    createElementVNode("span", {
                      class: "dialog-final__content__btn button-final",
                      onClick: nextStep
                    }, "\u0414\u0430\u043B\u0435\u0435")
                  ]),
                  createElementVNode("div", {
                    class: "dialog-cooling-final__art",
                    style: normalizeStyle(`background: url(${unref($m)(
                      "/images/cooling-final.png"
                    )}) center/cover no-repeat;`)
                  }, null, 4)
                ])
              ]),
              _: 1
            })) : createCommentVNode("", true)
          ]),
          _: 1
        })
      ]);
    };
  }
};
const _hoisted_1 = { class: "section heat" };
const _hoisted_2 = { class: "heat__header panel" };
const _hoisted_3 = { class: "heat__header__title main-text-16" };
const _hoisted_4 = { class: "headline-24" };
const _hoisted_5 = {
  key: 0,
  class: "heat__block-1 panel"
};
const _hoisted_6 = { class: "headline-20" };
const _hoisted_7 = ["innerHTML"];
const _hoisted_8 = { class: "heat__block-1__answers" };
const _hoisted_9 = {
  key: 1,
  class: "heat__block-2 panel"
};
const _hoisted_10 = { class: "headline-20" };
const _hoisted_11 = { class: "heat__block-2__text footnote-12" };
const _hoisted_12 = {
  key: 2,
  class: "heat__block-3 panel"
};
const _hoisted_13 = { class: "headline-20" };
const _hoisted_14 = { class: "heat__block-3__route main-text-16" };
const _hoisted_15 = /* @__PURE__ */ createElementVNode("span", { class: "heat-tasks-route-item__num" }, [
  /* @__PURE__ */ createElementVNode("span", null, "1"),
  /* @__PURE__ */ createElementVNode("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    width: "20",
    height: "20",
    viewBox: "0 0 20 20",
    fill: "none"
  }, [
    /* @__PURE__ */ createElementVNode("path", {
      d: "M2 10.1014L7.48571 15.6667L18 5",
      stroke: "#00B612",
      "stroke-width": "1.6"
    })
  ])
], -1);
const _hoisted_16 = /* @__PURE__ */ createElementVNode("span", { class: "heat-tasks-route-item__num" }, [
  /* @__PURE__ */ createElementVNode("span", null, "2"),
  /* @__PURE__ */ createElementVNode("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    width: "20",
    height: "20",
    viewBox: "0 0 20 20",
    fill: "none"
  }, [
    /* @__PURE__ */ createElementVNode("path", {
      d: "M2 10.1014L7.48571 15.6667L18 5",
      stroke: "#00B612",
      "stroke-width": "1.6"
    })
  ])
], -1);
const _hoisted_17 = /* @__PURE__ */ createElementVNode("span", { class: "heat-tasks-route-item__num" }, [
  /* @__PURE__ */ createElementVNode("span", null, "3"),
  /* @__PURE__ */ createElementVNode("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    width: "20",
    height: "20",
    viewBox: "0 0 20 20",
    fill: "none"
  }, [
    /* @__PURE__ */ createElementVNode("path", {
      d: "M2 10.1014L7.48571 15.6667L18 5",
      stroke: "#00B612",
      "stroke-width": "1.6"
    })
  ])
], -1);
const _hoisted_18 = /* @__PURE__ */ createElementVNode("span", { class: "heat-tasks-route-item__num" }, [
  /* @__PURE__ */ createElementVNode("span", null, "4"),
  /* @__PURE__ */ createElementVNode("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    width: "20",
    height: "20",
    viewBox: "0 0 20 20",
    fill: "none"
  }, [
    /* @__PURE__ */ createElementVNode("path", {
      d: "M2 10.1014L7.48571 15.6667L18 5",
      stroke: "#00B612",
      "stroke-width": "1.6"
    })
  ])
], -1);
const _hoisted_19 = { class: "panel heat-graph-panel" };
const _hoisted_20 = ["src"];
const _hoisted_21 = {
  key: 1,
  class: "heat-graph-panel__main-graph-box"
};
const _hoisted_22 = { class: "_corner-lt headline-24" };
const _hoisted_23 = { class: "_corner-lb headline-24" };
const _hoisted_24 = { class: "_corner-rb headline-24" };
const _hoisted_25 = /* @__PURE__ */ createElementVNode("div", { class: "_axis" }, [
  /* @__PURE__ */ createElementVNode("span", null, "900"),
  /* @__PURE__ */ createElementVNode("span", null, "800"),
  /* @__PURE__ */ createElementVNode("span", null, "700"),
  /* @__PURE__ */ createElementVNode("span", null, "600"),
  /* @__PURE__ */ createElementVNode("span", null, "500")
], -1);
const _hoisted_26 = {
  class: "heat-graph-panel__main-graph",
  height: "409",
  viewBox: "0 0 599 409",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};
const _hoisted_27 = ["stroke"];
const _hoisted_28 = ["stroke"];
const _hoisted_29 = ["stroke"];
const _hoisted_30 = ["stroke"];
const _hoisted_31 = ["stroke"];
const _hoisted_32 = ["stroke"];
const _hoisted_33 = ["stroke"];
const _hoisted_34 = /* @__PURE__ */ createStaticVNode('<path d="M312.468 187.302L303.793 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M303.118 187.302L294.443 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M293.441 192.438L285.422 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M321.577 187.302L312.902 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M330.682 187.302L322.008 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M339.794 187.302L331.119 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M348.901 187.302L340.227 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M358.007 187.302L349.332 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M367.118 187.302L358.443 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M376.223 187.302L367.549 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M385.331 187.302L376.656 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M394.442 187.302L385.768 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M403.55 187.302L394.875 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M412.657 187.302L403.982 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M421.768 187.302L413.094 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M430.876 187.302L422.201 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M439.983 187.302L431.309 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M449.093 187.302L440.418 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M458.2 187.302L449.525 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M467.309 187.302L458.635 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M476.419 187.302L467.744 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M485.526 187.302L476.852 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M494.634 187.302L485.959 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M503.741 187.302L495.066 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M512.848 187.302L504.174 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M521.958 187.302L513.283 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M531.067 187.302L522.393 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M540.175 187.302L531.5 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M549.284 187.302L540.609 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M558.391 187.302L549.717 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M567.499 187.302L558.824 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M576.608 187.302L567.934 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M585.716 187.302L577.041 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M594.825 187.302L586.15 207.302" stroke="#A2ACB1" stroke-width="2"></path><path d="M275.396 207.301H598.727" stroke="#A2ACB1" stroke-width="3"></path><path d="M299.453 187.302H598.726" stroke="#A2ACB1" stroke-width="3"></path><path d="M2.27539 1V406.597C187.709 406.429 598.275 407 598.275 407" stroke="black" stroke-width="4"></path>', 37);
const _hoisted_71 = { key: 0 };
const _hoisted_72 = /* @__PURE__ */ createStaticVNode('<rect x="78" y="262" width="68" height="49" rx="6" fill="white"></rect><path d="M98 296L105.656 279.08H108.248L116 296H112.856L110.792 291.248L112.04 292.088H101.888L103.184 291.248L101.12 296H98ZM106.928 282.392L103.544 290.408L102.92 289.664H110.984L110.48 290.408L107.024 282.392H106.928Z" fill="#75797B"></path><path d="M117.464 296V293.984H119.84V287.936H120.848L117.704 289.64V287.36L120.488 285.848H122.264V293.984H124.304V296H117.464Z" fill="#75797B"></path><rect x="209" y="86" width="55" height="49" rx="6" fill="white"></rect><path d="M230.872 120V117.72L236.488 111.864C237.192 111.128 237.712 110.448 238.048 109.824C238.384 109.2 238.552 108.56 238.552 107.904C238.552 107.088 238.296 106.472 237.784 106.056C237.272 105.64 236.528 105.432 235.552 105.432C234.768 105.432 234.016 105.576 233.296 105.864C232.576 106.136 231.872 106.568 231.184 107.16L230.176 104.856C230.864 104.248 231.712 103.76 232.72 103.392C233.744 103.024 234.816 102.84 235.936 102.84C237.76 102.84 239.16 103.248 240.136 104.064C241.112 104.88 241.6 106.048 241.6 107.568C241.6 108.624 241.352 109.632 240.856 110.592C240.36 111.536 239.592 112.528 238.552 113.568L233.872 118.248V117.432H242.272V120H230.872Z" fill="#75797B"></path><rect x="112" y="44" width="55" height="49" rx="6" fill="white"></rect><path d="M134.664 78V75.432H138.552V63.672H140.04L135.048 66.648V63.72L139.44 61.08H141.648V75.432H145.296V78H134.664Z" fill="#75797B"></path><path d="M41 214L48.656 197.08H51.248L59 214H55.856L53.792 209.248L55.04 210.088H44.888L46.184 209.248L44.12 214H41ZM49.928 200.392L46.544 208.408L45.92 207.664H53.984L53.48 208.408L50.024 200.392H49.928Z" fill="#75797B"></path><path d="M63.488 214.12C62.704 214.12 61.984 214.04 61.328 213.88C60.688 213.704 60.136 213.456 59.672 213.136L60.152 211.384C60.568 211.656 61.04 211.864 61.568 212.008C62.096 212.152 62.656 212.224 63.248 212.224C63.808 212.224 64.248 212.12 64.568 211.912C64.888 211.688 65.048 211.36 65.048 210.928C65.048 210.528 64.896 210.24 64.592 210.064C64.288 209.872 63.88 209.776 63.368 209.776H61.352V208H63.08C63.608 208 64.016 207.896 64.304 207.688C64.592 207.48 64.736 207.176 64.736 206.776C64.736 206.408 64.592 206.128 64.304 205.936C64.032 205.728 63.632 205.624 63.104 205.624C62.608 205.624 62.104 205.712 61.592 205.888C61.096 206.048 60.672 206.264 60.32 206.536L59.792 204.76C60.224 204.44 60.76 204.192 61.4 204.016C62.04 203.84 62.744 203.752 63.512 203.752C64.616 203.752 65.488 203.992 66.128 204.472C66.768 204.952 67.088 205.6 67.088 206.416C67.088 207.072 66.872 207.624 66.44 208.072C66.024 208.504 65.504 208.784 64.88 208.912V208.648C65.632 208.728 66.232 208.984 66.68 209.416C67.128 209.848 67.352 210.44 67.352 211.192C67.352 212.104 66.992 212.824 66.272 213.352C65.552 213.864 64.624 214.12 63.488 214.12Z" fill="#75797B"></path>', 9);
const _hoisted_81 = [
  _hoisted_72
];
const _hoisted_82 = /* @__PURE__ */ createElementVNode("path", {
  d: "M434.22 144V141.86H437.46V132.06H438.7L434.54 134.54V132.1L438.2 129.9H440.04V141.86H443.08V144H434.22Z",
  fill: "#75797B"
}, null, -1);
const _hoisted_83 = /* @__PURE__ */ createElementVNode("path", {
  d: "M241 265.2C239.347 265.2 238.073 264.573 237.18 263.32C236.287 262.053 235.84 260.253 235.84 257.92C235.84 255.56 236.287 253.767 237.18 252.54C238.073 251.313 239.347 250.7 241 250.7C242.667 250.7 243.94 251.313 244.82 252.54C245.713 253.767 246.16 255.553 246.16 257.9C246.16 260.247 245.713 262.053 244.82 263.32C243.94 264.573 242.667 265.2 241 265.2ZM241 263.08C241.907 263.08 242.58 262.667 243.02 261.84C243.46 261 243.68 259.687 243.68 257.9C243.68 256.113 243.46 254.82 243.02 254.02C242.58 253.207 241.907 252.8 241 252.8C240.107 252.8 239.433 253.207 238.98 254.02C238.54 254.82 238.32 256.113 238.32 257.9C238.32 259.687 238.54 261 238.98 261.84C239.433 262.667 240.107 263.08 241 263.08ZM249.292 267.66L248.272 266.86C248.699 266.42 248.992 266.007 249.152 265.62C249.326 265.247 249.412 264.847 249.412 264.42L249.952 265H248.072V262.22H250.872V264.06C250.872 264.74 250.752 265.36 250.512 265.92C250.286 266.493 249.879 267.073 249.292 267.66ZM257.953 265.2C256.3 265.2 255.006 264.847 254.073 264.14C253.153 263.433 252.693 262.453 252.693 261.2C252.693 260.213 252.993 259.393 253.593 258.74C254.193 258.087 254.973 257.7 255.933 257.58V258C255.066 257.813 254.366 257.407 253.833 256.78C253.313 256.153 253.053 255.393 253.053 254.5C253.053 253.313 253.486 252.387 254.353 251.72C255.233 251.04 256.433 250.7 257.953 250.7C259.486 250.7 260.686 251.04 261.553 251.72C262.433 252.387 262.873 253.313 262.873 254.5C262.873 255.393 262.62 256.16 262.113 256.8C261.62 257.44 260.946 257.833 260.093 257.98V257.58C261.04 257.713 261.8 258.107 262.373 258.76C262.946 259.4 263.233 260.213 263.233 261.2C263.233 262.453 262.766 263.433 261.833 264.14C260.913 264.847 259.62 265.2 257.953 265.2ZM257.953 263.2C258.926 263.2 259.653 263.02 260.133 262.66C260.613 262.287 260.853 261.74 260.853 261.02C260.853 260.3 260.613 259.76 260.133 259.4C259.653 259.04 258.926 258.86 257.953 258.86C256.993 258.86 256.266 259.04 255.773 259.4C255.293 259.76 255.053 260.3 255.053 261.02C255.053 261.74 255.293 262.287 255.773 262.66C256.266 263.02 256.993 263.2 257.953 263.2ZM257.953 256.86C258.78 256.86 259.406 256.673 259.833 256.3C260.273 255.927 260.493 255.413 260.493 254.76C260.493 254.12 260.273 253.62 259.833 253.26C259.406 252.887 258.78 252.7 257.953 252.7C257.14 252.7 256.513 252.887 256.073 253.26C255.646 253.62 255.433 254.12 255.433 254.76C255.433 255.413 255.646 255.927 256.073 256.3C256.513 256.673 257.14 256.86 257.953 256.86Z",
  fill: "#75797B"
}, null, -1);
const _hoisted_84 = /* @__PURE__ */ createElementVNode("path", {
  d: "M456.72 265L463.56 252.16V253.06H456.08V250.9H465.94V252.82L459.54 265H456.72ZM468.552 265V263.1L473.232 258.22C473.819 257.607 474.252 257.04 474.532 256.52C474.812 256 474.952 255.467 474.952 254.92C474.952 254.24 474.739 253.727 474.312 253.38C473.886 253.033 473.266 252.86 472.452 252.86C471.799 252.86 471.172 252.98 470.572 253.22C469.972 253.447 469.386 253.807 468.812 254.3L467.972 252.38C468.546 251.873 469.252 251.467 470.092 251.16C470.946 250.853 471.839 250.7 472.772 250.7C474.292 250.7 475.459 251.04 476.272 251.72C477.086 252.4 477.492 253.373 477.492 254.64C477.492 255.52 477.286 256.36 476.872 257.16C476.459 257.947 475.819 258.773 474.952 259.64L471.052 263.54V262.86H478.052V265H468.552ZM480.704 265L487.544 252.16V253.06H480.064V250.9H489.924V252.82L483.524 265H480.704Z",
  fill: "#75797B"
}, null, -1);
const _hoisted_85 = { key: 0 };
const _hoisted_86 = /* @__PURE__ */ createElementVNode("path", {
  d: "M120 404L120 159",
  stroke: "#6C3BF0",
  "stroke-width": "4",
  "stroke-dasharray": "8 8"
}, null, -1);
const _hoisted_87 = /* @__PURE__ */ createElementVNode("path", {
  d: "M34 404L34 114",
  stroke: "#6C3BF0",
  "stroke-width": "4",
  "stroke-dasharray": "8 8"
}, null, -1);
const _hoisted_88 = /* @__PURE__ */ createElementVNode("path", {
  d: "M206 404L206 218",
  stroke: "#6C3BF0",
  "stroke-width": "4",
  "stroke-dasharray": "8 8"
}, null, -1);
const _hoisted_89 = { key: 0 };
const _hoisted_90 = /* @__PURE__ */ createElementVNode("path", {
  d: "M120 402L120 168.5",
  stroke: "#6C3BF0",
  "stroke-width": "4",
  "stroke-dasharray": "8 8"
}, null, -1);
const _hoisted_91 = /* @__PURE__ */ createElementVNode("path", {
  d: "M115.5 173.5L2.5 173.5",
  stroke: "#6C3BF0",
  "stroke-width": "4",
  "stroke-dasharray": "8 8"
}, null, -1);
const _hoisted_92 = [
  _hoisted_90,
  _hoisted_91
];
const _hoisted_93 = { key: 0 };
const _hoisted_94 = /* @__PURE__ */ createElementVNode("path", {
  d: "M179 158L219 118",
  stroke: "#75797B",
  "stroke-width": "2"
}, null, -1);
const _hoisted_95 = {
  key: 0,
  d: "M74.5 128L117 84",
  stroke: "#75797B",
  "stroke-width": "2"
};
const _hoisted_96 = {
  key: 1,
  d: "M77 124L117 84",
  stroke: "#75797B",
  "stroke-width": "2"
};
const _hoisted_97 = {
  key: 2,
  d: "M64 208L112 175",
  stroke: "#75797B",
  "stroke-width": "2"
};
const _hoisted_98 = {
  key: 3,
  d: "M69 202L117 169",
  stroke: "#75797B",
  "stroke-width": "2"
};
const _hoisted_99 = /* @__PURE__ */ createElementVNode("path", {
  d: "M131 272L171 232",
  stroke: "#75797B",
  "stroke-width": "2"
}, null, -1);
const _hoisted_100 = /* @__PURE__ */ createElementVNode("path", {
  d: "M277.5 155L237.5 115",
  stroke: "#75797B",
  "stroke-width": "2"
}, null, -1);
const _hoisted_101 = /* @__PURE__ */ createElementVNode("rect", {
  x: "73",
  y: "263",
  width: "66",
  height: "47",
  rx: "5",
  fill: "white"
}, null, -1);
const _hoisted_102 = /* @__PURE__ */ createElementVNode("path", {
  d: "M92 296L99.656 279.08H102.248L110 296H106.856L104.792 291.248L106.04 292.088H95.888L97.184 291.248L95.12 296H92ZM100.928 282.392L97.544 290.408L96.92 289.664H104.984L104.48 290.408L101.024 282.392H100.928Z",
  fill: "#1D1F20"
}, null, -1);
const _hoisted_103 = /* @__PURE__ */ createElementVNode("path", {
  d: "M111.464 296V293.984H113.84V287.936H114.848L111.704 289.64V287.36L114.488 285.848H116.264V293.984H118.304V296H111.464Z",
  fill: "#1D1F20"
}, null, -1);
const _hoisted_104 = /* @__PURE__ */ createElementVNode("rect", {
  x: "73",
  y: "263",
  width: "66",
  height: "47",
  rx: "5",
  stroke: "#C2AEF7",
  "stroke-width": "2"
}, null, -1);
const _hoisted_105 = /* @__PURE__ */ createElementVNode("rect", {
  x: "204",
  y: "87",
  width: "53",
  height: "47",
  rx: "5",
  fill: "white"
}, null, -1);
const _hoisted_106 = /* @__PURE__ */ createElementVNode("path", {
  d: "M224.872 120V117.72L230.488 111.864C231.192 111.128 231.712 110.448 232.048 109.824C232.384 109.2 232.552 108.56 232.552 107.904C232.552 107.088 232.296 106.472 231.784 106.056C231.272 105.64 230.528 105.432 229.552 105.432C228.768 105.432 228.016 105.576 227.296 105.864C226.576 106.136 225.872 106.568 225.184 107.16L224.176 104.856C224.864 104.248 225.712 103.76 226.72 103.392C227.744 103.024 228.816 102.84 229.936 102.84C231.76 102.84 233.16 103.248 234.136 104.064C235.112 104.88 235.6 106.048 235.6 107.568C235.6 108.624 235.352 109.632 234.856 110.592C234.36 111.536 233.592 112.528 232.552 113.568L227.872 118.248V117.432H236.272V120H224.872Z",
  fill: "#1D1F20"
}, null, -1);
const _hoisted_107 = /* @__PURE__ */ createElementVNode("rect", {
  x: "204",
  y: "87",
  width: "53",
  height: "47",
  rx: "5",
  stroke: "#C2AEF7",
  "stroke-width": "2"
}, null, -1);
const _hoisted_108 = /* @__PURE__ */ createElementVNode("rect", {
  x: "107",
  y: "45",
  width: "53",
  height: "47",
  rx: "5",
  fill: "white"
}, null, -1);
const _hoisted_109 = /* @__PURE__ */ createElementVNode("path", {
  d: "M128.664 78V75.432H132.552V63.672H134.04L129.048 66.648V63.72L133.44 61.08H135.648V75.432H139.296V78H128.664Z",
  fill: "#1D1F20"
}, null, -1);
const _hoisted_110 = /* @__PURE__ */ createElementVNode("rect", {
  x: "107",
  y: "45",
  width: "53",
  height: "47",
  rx: "5",
  stroke: "#C2AEF7",
  "stroke-width": "2"
}, null, -1);
const _hoisted_111 = { key: 4 };
const _hoisted_112 = /* @__PURE__ */ createElementVNode("rect", {
  x: "11",
  y: "195",
  width: "66",
  height: "47",
  rx: "5",
  fill: "white"
}, null, -1);
const _hoisted_113 = /* @__PURE__ */ createElementVNode("path", {
  d: "M30 228L37.656 211.08H40.248L48 228H44.856L42.792 223.248L44.04 224.088H33.888L35.184 223.248L33.12 228H30ZM38.928 214.392L35.544 222.408L34.92 221.664H42.984L42.48 222.408L39.024 214.392H38.928Z",
  fill: "#1D1F20"
}, null, -1);
const _hoisted_114 = /* @__PURE__ */ createElementVNode("rect", {
  x: "11",
  y: "195",
  width: "66",
  height: "47",
  rx: "5",
  stroke: "#C2AEF7",
  "stroke-width": "2"
}, null, -1);
const _hoisted_115 = [
  _hoisted_112,
  _hoisted_113,
  _hoisted_114
];
const _hoisted_116 = { key: 5 };
const _hoisted_117 = /* @__PURE__ */ createElementVNode("rect", {
  x: "11",
  y: "195",
  width: "66",
  height: "47",
  rx: "5",
  fill: "#E7DFFC"
}, null, -1);
const _hoisted_118 = /* @__PURE__ */ createElementVNode("path", {
  d: "M30 228L37.656 211.08H40.248L48 228H44.856L42.792 223.248L44.04 224.088H33.888L35.184 223.248L33.12 228H30ZM38.928 214.392L35.544 222.408L34.92 221.664H42.984L42.48 222.408L39.024 214.392H38.928Z",
  fill: "#080809"
}, null, -1);
const _hoisted_119 = /* @__PURE__ */ createElementVNode("rect", {
  x: "11",
  y: "195",
  width: "66",
  height: "47",
  rx: "5",
  stroke: "#6C3BF0",
  "stroke-width": "2"
}, null, -1);
const _hoisted_120 = [
  _hoisted_117,
  _hoisted_118,
  _hoisted_119
];
const _hoisted_121 = /* @__PURE__ */ createElementVNode("path", {
  d: "M52.488 228.12C51.704 228.12 50.984 228.04 50.328 227.88C49.688 227.704 49.136 227.456 48.672 227.136L49.152 225.384C49.568 225.656 50.04 225.864 50.568 226.008C51.096 226.152 51.656 226.224 52.248 226.224C52.808 226.224 53.248 226.12 53.568 225.912C53.888 225.688 54.048 225.36 54.048 224.928C54.048 224.528 53.896 224.24 53.592 224.064C53.288 223.872 52.88 223.776 52.368 223.776H50.352V222H52.08C52.608 222 53.016 221.896 53.304 221.688C53.592 221.48 53.736 221.176 53.736 220.776C53.736 220.408 53.592 220.128 53.304 219.936C53.032 219.728 52.632 219.624 52.104 219.624C51.608 219.624 51.104 219.712 50.592 219.888C50.096 220.048 49.672 220.264 49.32 220.536L48.792 218.76C49.224 218.44 49.76 218.192 50.4 218.016C51.04 217.84 51.744 217.752 52.512 217.752C53.616 217.752 54.488 217.992 55.128 218.472C55.768 218.952 56.088 219.6 56.088 220.416C56.088 221.072 55.872 221.624 55.44 222.072C55.024 222.504 54.504 222.784 53.88 222.912V222.648C54.632 222.728 55.232 222.984 55.68 223.416C56.128 223.848 56.352 224.44 56.352 225.192C56.352 226.104 55.992 226.824 55.272 227.352C54.552 227.864 53.624 228.12 52.488 228.12Z",
  fill: "#1D1F20"
}, null, -1);
const _hoisted_122 = { key: 0 };
const _hoisted_123 = /* @__PURE__ */ createElementVNode("circle", {
  cx: "120",
  cy: "172",
  r: "12",
  fill: "#F5F1FF",
  stroke: "#6C3BF0",
  "stroke-width": "2"
}, null, -1);
const _hoisted_124 = /* @__PURE__ */ createElementVNode("circle", {
  cx: "120",
  cy: "172",
  r: "5",
  fill: "#6C3BF0",
  stroke: "#6C3BF0",
  "stroke-width": "2"
}, null, -1);
const _hoisted_125 = [
  _hoisted_123,
  _hoisted_124
];
const _hoisted_126 = /* @__PURE__ */ createStaticVNode('<path d="M457 12L476 12" stroke="#A2ACB1"></path><line x1="451" y1="18.5" x2="469" y2="18.5" stroke="#A2ACB1"></line><line x1="392" y1="66.5" x2="413" y2="66.5" stroke="#A2ACB1"></line><line x1="385" y1="72.5" x2="406" y2="72.5" stroke="#A2ACB1"></line><line x1="376" y1="78.5" x2="399" y2="78.5" stroke="#A2ACB1"></line><line x1="369" y1="84.5" x2="392" y2="84.5" stroke="#A2ACB1"></line><line x1="362" y1="90.5" x2="385" y2="90.5" stroke="#A2ACB1"></line><line x1="355" y1="96.5" x2="378" y2="96.5" stroke="#A2ACB1"></line><line x1="348" y1="102.5" x2="371" y2="102.5" stroke="#A2ACB1"></line><line x1="340" y1="108.5" x2="364" y2="108.5" stroke="#A2ACB1"></line><line x1="333" y1="114.5" x2="357" y2="114.5" stroke="#A2ACB1"></line><line x1="325" y1="120.5" x2="349" y2="120.5" stroke="#A2ACB1"></line><line x1="318" y1="126.5" x2="342" y2="126.5" stroke="#A2ACB1"></line><line x1="311" y1="132.5" x2="335" y2="132.5" stroke="#A2ACB1"></line><line x1="302" y1="138.5" x2="328" y2="138.5" stroke="#A2ACB1"></line><line x1="295" y1="144.5" x2="321" y2="144.5" stroke="#A2ACB1"></line><line x1="288" y1="150.5" x2="314" y2="150.5" stroke="#A2ACB1"></line><line x1="281" y1="156.5" x2="307" y2="156.5" stroke="#A2ACB1"></line><line x1="270" y1="162.5" x2="301" y2="162.5" stroke="#A2ACB1"></line><line x1="265" y1="168.5" x2="296" y2="168.5" stroke="#A2ACB1"></line><line x1="258" y1="174.5" x2="289" y2="174.5" stroke="#A2ACB1"></line><path d="M399.5 60L420 60" stroke="#A2ACB1"></path><path d="M407 54L428 54" stroke="#A2ACB1"></path><path d="M413.5 48L434 48" stroke="#A2ACB1"></path><path d="M422 42L441 42" stroke="#A2ACB1"></path><path d="M429.5 36L448 36" stroke="#A2ACB1"></path><line x1="437" y1="30.5" x2="455" y2="30.5" stroke="#A2ACB1"></line><line x1="444" y1="24.5" x2="462" y2="24.5" stroke="#A2ACB1"></line><line x1="21" y1="62.5" x2="39" y2="63.5" stroke="#A2ACB1"></line><path d="M29 68H47" stroke="#A2ACB1"></path><line x1="35" y1="74.5" x2="54" y2="75.5" stroke="#A2ACB1"></line><line x1="43" y1="80.5" x2="62" y2="81.5" stroke="#A2ACB1"></line><line x1="14" y1="55.5" x2="30" y2="56.5" stroke="#A2ACB1"></line><path d="M19.75 62.6084L9.5 75" stroke="#A2ACB1"></path><path d="M24.514 66.2636L14.0002 80.0002" stroke="#A2ACB1"></path><line x1="28.8752" y1="69.608" x2="17.9191" y2="83.8896" stroke="#A2ACB1"></line><line x1="33.3537" y1="73.3043" x2="22.3976" y2="87.5859" stroke="#A2ACB1"></line><line x1="37.3967" y1="77.9127" x2="26.4405" y2="92.1943" stroke="#A2ACB1"></line><line x1="42.1565" y1="81.5651" x2="31.2003" y2="95.8466" stroke="#A2ACB1"></line><line x1="45.7874" y1="86.2883" x2="35.4079" y2="100.994" stroke="#A2ACB1"></line><line x1="50.6858" y1="89.7483" x2="40.3064" y2="104.454" stroke="#A2ACB1"></line><line x1="55.5901" y1="93.2092" x2="45.2107" y2="107.915" stroke="#A2ACB1"></line><line x1="60.4905" y1="96.6672" x2="50.111" y2="111.373" stroke="#A2ACB1"></line><line x1="65.3948" y1="100.127" x2="55.0153" y2="114.833" stroke="#A2ACB1"></line><path d="M70 104.5L59.8382 119.635" stroke="#A2ACB1"></path><line x1="74.2629" y1="107.544" x2="64.4503" y2="122.634" stroke="#A2ACB1"></line><path d="M79.7148 111.086L69.502 126.5" stroke="#A2ACB1"></path><path d="M84 115L73.1762 131.954" stroke="#A2ACB1"></path><path d="M89.0781 118.311L79 133.951" stroke="#A2ACB1"></path><line x1="93.2941" y1="122.187" x2="84.0408" y2="137.626" stroke="#A2ACB1"></line><path d="M99.002 126L88.6196 142.967" stroke="#A2ACB1"></path><path d="M104 129.5L93.7627 146.052" stroke="#A2ACB1"></path><path d="M109 132.5L98.9111 149.136" stroke="#A2ACB1"></path><path d="M113.5 135.999L104.057 152.221" stroke="#A2ACB1"></path><path d="M118.5 138.499L109.433 155.492" stroke="#A2ACB1"></path><path d="M123.162 142.396L114.502 158.5" stroke="#A2ACB1"></path><line x1="128.441" y1="145.235" x2="119.594" y2="161.877" stroke="#A2ACB1"></line><line x1="133.323" y1="148.781" x2="124.895" y2="164.686" stroke="#A2ACB1"></line><line x1="138.623" y1="151.591" x2="130.195" y2="167.496" stroke="#A2ACB1"></line><line x1="143.926" y1="154.399" x2="135.498" y2="170.304" stroke="#A2ACB1"></line><line x1="149.227" y1="157.209" x2="140.799" y2="173.114" stroke="#A2ACB1"></line><line x1="154.53" y1="160.018" x2="146.102" y2="175.923" stroke="#A2ACB1"></line><line x1="159.83" y1="162.828" x2="151.402" y2="178.733" stroke="#A2ACB1"></line><line x1="165.133" y1="165.636" x2="156.705" y2="181.541" stroke="#A2ACB1"></line><line x1="170.434" y1="168.446" x2="162.006" y2="184.351" stroke="#A2ACB1"></line><line x1="175.737" y1="171.256" x2="167.309" y2="187.161" stroke="#A2ACB1"></line><line x1="182.038" y1="173.065" x2="173.61" y2="188.97" stroke="#A2ACB1"></line><line x1="187.34" y1="175.874" x2="178.912" y2="191.779" stroke="#A2ACB1"></line><line x1="191.641" y1="179.683" x2="183.213" y2="195.588" stroke="#A2ACB1"></line><line x1="197.942" y1="181.493" x2="189.514" y2="197.398" stroke="#A2ACB1"></line><line x1="204.245" y1="183.301" x2="195.817" y2="199.206" stroke="#A2ACB1"></line><line x1="209.547" y1="186.111" x2="201.119" y2="202.016" stroke="#A2ACB1"></line><line x1="214.848" y1="188.921" x2="206.42" y2="204.826" stroke="#A2ACB1"></line><line x1="220.151" y1="191.729" x2="211.723" y2="207.634" stroke="#A2ACB1"></line><line x1="225.452" y1="194.539" x2="217.024" y2="210.444" stroke="#A2ACB1"></line><line x1="230.752" y1="197.348" x2="222.324" y2="213.253" stroke="#A2ACB1"></line><line x1="79" y1="112.5" x2="103" y2="113.5" stroke="#A2ACB1"></line><line x1="51" y1="87.5" x2="71" y2="88.5" stroke="#A2ACB1"></line><path d="M90 118L112 118" stroke="#A2ACB1"></path><path d="M58 93H76" stroke="#A2ACB1"></path><line x1="97" y1="124.5" x2="121" y2="125.5" stroke="#A2ACB1"></line><path d="M64.5 99L86 99" stroke="#A2ACB1"></path><line x1="107" y1="130.5" x2="131" y2="131.5" stroke="#A2ACB1"></line><line x1="71" y1="105.5" x2="95" y2="106.5" stroke="#A2ACB1"></line><line x1="114" y1="136.5" x2="138" y2="137.5" stroke="#A2ACB1"></line><line x1="124" y1="142.5" x2="148" y2="143.5" stroke="#A2ACB1"></line><line x1="134" y1="148.5" x2="158" y2="149.5" stroke="#A2ACB1"></line><line x1="145" y1="154.5" x2="169" y2="155.5" stroke="#A2ACB1"></line><line x1="153" y1="160.5" x2="181" y2="161.5" stroke="#A2ACB1"></line><line x1="165" y1="166.5" x2="193" y2="167.5" stroke="#A2ACB1"></line><line x1="178" y1="172.5" x2="206" y2="173.5" stroke="#A2ACB1"></line><line x1="191" y1="178.5" x2="219" y2="179.5" stroke="#A2ACB1"></line><line x1="202" y1="184.5" x2="230" y2="185.5" stroke="#A2ACB1"></line><line x1="250" y1="180.5" x2="281" y2="180.5" stroke="#A2ACB1"></line><line x1="241" y1="186.5" x2="272" y2="186.5" stroke="#A2ACB1"></line><path d="M222.5 192L268 192" stroke="#A2ACB1"></path><rect x="0.5" y="0.5" width="598" height="408" stroke="black"></rect>', 97);
const _hoisted_223 = { class: "_answers-1" };
const _hoisted_224 = { class: "heat-graph-panel__hint" };
const _hoisted_225 = ["innerHTML"];
const _hoisted_226 = { class: "heat-graph-panel__hint__text footnote-12" };
const _hoisted_227 = {
  key: 2,
  class: "heat-graph-panel__last-question"
};
const _hoisted_228 = { class: "_title headline-20" };
const _hoisted_229 = { class: "_text main-text-16" };
const _hoisted_230 = { class: "_answers" };
const _hoisted_231 = { class: "dialog-final" };
const _hoisted_232 = { class: "dialog-final__content" };
const _hoisted_233 = { class: "dialog-final__content__title headline-32" };
const _hoisted_234 = ["innerHTML"];
const _hoisted_235 = { class: "dialog-final__content__btn button-final" };
const _sfc_main$1 = {
  __name: "Heat",
  emits: ["next"],
  setup(__props, { emit }) {
    const $t = inject("translate");
    inject("log");
    const $m = inject("media");
    const isModalOpen = ref(false);
    let step = ref(1);
    let block1Answer = ref(0);
    let lastBlockAnswer = ref(0);
    let block2Stage = ref(1);
    const wrongAnswer = ref(false);
    const graphStep1Answer = ref(0);
    const selectedLine = ref("");
    const setBlock1Answer = (num) => {
      wrongAnswer.value = false;
      block1Answer.value = num;
    };
    const setGraphStep1Answer = (num) => {
      wrongAnswer.value = false;
      graphStep1Answer.value = num;
      if (num === 1)
        passStage();
    };
    const selectLine = (lineName) => {
      if (block2Stage.value < 2)
        return false;
      selectedLine.value = lineName;
      if (lineName === "a3" && block2Stage.value === 2)
        passStage();
    };
    const selectCircle = (circleNum) => {
      if (circleNum !== 2)
        return false;
      else if (block2Stage.value === 3)
        passStage();
    };
    const setLastBlockAnswer = (num) => {
      wrongAnswer.value = false;
      lastBlockAnswer.value = num;
    };
    const confirmStep1 = () => {
      if (block1Answer.value !== 1) {
        wrongAnswer.value = true;
        return false;
      } else
        step.value = 2;
    };
    const confirmLastAnswer = () => {
      if (lastBlockAnswer.value !== 2) {
        wrongAnswer.value = true;
        return false;
      } else
        passStage();
    };
    const passStage = () => {
      block2Stage.value++;
    };
    const openModal = () => {
      isModalOpen.value = true;
    };
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("main", null, [
        createElementVNode("section", _hoisted_1, [
          createElementVNode("div", _hoisted_2, [
            createElementVNode("div", _hoisted_3, toDisplayString(unref($t)("\u0427\u0430\u0441\u0442\u044C 4")), 1),
            createElementVNode("div", _hoisted_4, toDisplayString(unref($t)("\u0422\u0435\u0440\u043C\u043E\u043E\u0431\u0440\u0430\u0431\u043E\u0442\u043A\u0430")), 1)
          ]),
          createElementVNode("div", null, [
            unref(step) === 1 ? (openBlock(), createElementBlock("div", _hoisted_5, [
              createElementVNode("div", _hoisted_6, toDisplayString(unref($t)("\u0421\u043A\u043E\u043B\u044C\u043A\u043E \u0443\u0433\u043B\u0435\u0440\u043E\u0434\u0430 \u0432\xA0\u0441\u0442\u0430\u043B\u0438\xA04\u04255\u041C\u0424\u0421?")), 1),
              createElementVNode("div", {
                class: "heat__block-1__text main-text-16",
                innerHTML: unref($t)(`
            \u0412 \u043C\u0430\u0440\u043A\u0438\u0440\u043E\u0432\u043A\u0435 \u0441\u0442\u0430\u043B\u0438 <strong>\u043F\u0435\u0440\u0432\u0430\u044F \u0446\u0438\u0444\u0440\u0430</strong> \u0433\u043E\u0432\u043E\u0440\u0438\u0442 \u043E \u0442\u043E\u043C,
            \u043A\u0430\u043A\u043E\u0435 \u0432 \u043D\u0435\u0439 \u0441\u043E\u0434\u0435\u0440\u0436\u0438\u0442\u0441\u044F \u043A\u043E\u043B\u0438\u0447\u0435\u0441\u0442\u0432\u043E \u0443\u0433\u043B\u0435\u0440\u043E\u0434\u0430 <strong>\u0432 \u0434\u0435\u0441\u044F\u0442\u044B\u0445 \u0434\u043E\u043B\u044F\u0445</strong>`)
              }, null, 8, _hoisted_7),
              createElementVNode("div", _hoisted_8, [
                createVNode(_sfc_main$5, {
                  label: unref($t)("0,4%"),
                  active: unref(block1Answer) === 1,
                  onButtonClick: _cache[0] || (_cache[0] = ($event) => setBlock1Answer(1))
                }, null, 8, ["label", "active"]),
                createVNode(_sfc_main$5, {
                  label: unref($t)("400%"),
                  active: unref(block1Answer) === 2,
                  wrong: wrongAnswer.value && unref(block1Answer) === 2,
                  onButtonClick: _cache[1] || (_cache[1] = ($event) => setBlock1Answer(2))
                }, null, 8, ["label", "active", "wrong"]),
                createVNode(_sfc_main$5, {
                  label: unref($t)("40%"),
                  active: unref(block1Answer) === 3,
                  wrong: wrongAnswer.value && unref(block1Answer) === 3,
                  onButtonClick: _cache[2] || (_cache[2] = ($event) => setBlock1Answer(3))
                }, null, 8, ["label", "active", "wrong"]),
                createVNode(_sfc_main$5, {
                  label: unref($t)("5%"),
                  active: unref(block1Answer) === 4,
                  wrong: wrongAnswer.value && unref(block1Answer) === 4,
                  onButtonClick: _cache[3] || (_cache[3] = ($event) => setBlock1Answer(4))
                }, null, 8, ["label", "active", "wrong"])
              ]),
              createElementVNode("div", {
                class: "button-simple",
                onClick: confirmStep1
              }, toDisplayString(unref($t)("\u0412\u044B\u0431\u0440\u0430\u0442\u044C")), 1)
            ])) : createCommentVNode("", true),
            unref(step) === 2 ? (openBlock(), createElementBlock("div", _hoisted_9, [
              createElementVNode("div", _hoisted_10, toDisplayString(unref($t)("\u0412\u044B\u0431\u0440\u0430\u0442\u044C \u0442\u0435\u043C\u043F\u0435\u0440\u0430\u0442\u0443\u0440\u0443 \u043E\u0442\u0436\u0438\u0433\u0430")), 1),
              createElementVNode("div", _hoisted_11, toDisplayString(unref($t)(`
            \u041E\u0442\u0436\u0438\u0433 \u2014 \u044D\u0442\u043E \u0432\u0438\u0434 \u0442\u0435\u0440\u043C\u043E\u043E\u0431\u0440\u0430\u0431\u043E\u0442\u043A\u0438. \u0427\u0442\u043E\u0431\u044B \xAB\u043E\u0442\u0436\u0435\u0447\u044C\xBB \u043C\u0435\u0442\u0430\u043B\u043B,
            \u0435\u0433\u043E \u0441\u043D\u043E\u0432\u0430 \u043D\u0443\u0436\u043D\u043E \u043D\u0430\u0433\u0440\u0435\u0442\u044C \u0434\u043E\xA0\u043E\u043F\u0440\u0435\u0434\u0435\u043B\u0451\u043D\u043D\u043E\u0439 \u0442\u0435\u043C\u043F\u0435\u0440\u0430\u0442\u0443\u0440\u044B,
            \u0432\u044B\u0434\u0435\u0440\u0436\u0430\u0442\u044C \u043F\u0440\u0438\u043C\u0435\u0440\u043D\u043E 4-6 \u0447\u0430\u0441\u043E\u0432, \u0430 \u0437\u0430\u0442\u0435\u043C \u0441\u043D\u043E\u0432\u0430 \u043E\u0445\u043B\u0430\u0434\u0438\u0442\u044C.`)), 1)
            ])) : createCommentVNode("", true),
            unref(step) === 2 ? (openBlock(), createElementBlock("div", _hoisted_12, [
              createElementVNode("div", _hoisted_13, toDisplayString(unref($t)("\u041F\u043E\u0440\u044F\u0434\u043E\u043A \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0439")), 1),
              createElementVNode("div", _hoisted_14, [
                createElementVNode("div", {
                  class: normalizeClass({
                    "heat-tasks-route-item": true,
                    "--active": unref(block2Stage) === 1,
                    "--passed": unref(block2Stage) > 1
                  })
                }, [
                  _hoisted_15,
                  createTextVNode(" " + toDisplayString(unref($t)("\u0412\u044B\u0431\u0435\u0440\u0438\u0442\u0435 \u0441\u043E\u0434\u0435\u0440\u0436\u0430\u043D\u0438\u0435 \u0443\u0433\u043B\u0435\u0440\u043E\u0434\u0430 \u043D\u0430 \u0433\u0440\u0430\u0444\u0438\u043A\u0435")), 1)
                ], 2),
                createElementVNode("div", {
                  class: normalizeClass({
                    "heat-tasks-route-item": true,
                    "--active": unref(block2Stage) === 2,
                    "--passed": unref(block2Stage) > 2
                  })
                }, [
                  _hoisted_16,
                  createTextVNode(toDisplayString(unref($t)("\u0412\u044B\u0431\u0435\u0440\u0438\u0442\u0435 \u043B\u0438\u043D\u0438\u044E \u04103")), 1)
                ], 2),
                createElementVNode("div", {
                  class: normalizeClass({
                    "heat-tasks-route-item": true,
                    "--active": unref(block2Stage) === 3,
                    "--passed": unref(block2Stage) > 3
                  })
                }, [
                  _hoisted_17,
                  createTextVNode(toDisplayString(unref($t)("\u041D\u0430\u0439\u0434\u0438\u0442\u0435 \u043F\u0435\u0440\u0435\u0441\u0435\u0447\u0435\u043D\u0438\u0435 \u043B\u0438\u043D\u0438\u0439 \u04103 \u0441 \u043F\u043E\u043A\u0430\u0437\u0430\u0442\u0435\u043B\u0435\u043C \u0443\u0433\u043B\u0435\u0433\u0440\u043E\u0434\u0430")), 1)
                ], 2),
                createElementVNode("div", {
                  class: normalizeClass({
                    "heat-tasks-route-item": true,
                    "--active": unref(block2Stage) === 4,
                    "--passed": unref(block2Stage) > 4
                  })
                }, [
                  _hoisted_18,
                  createTextVNode(toDisplayString(unref($t)("\u041D\u0430\u0439\u0434\u0438\u0442\u0435 \u0442\u0435\u043C\u043F\u0435\u0440\u0430\u0442\u0443\u0440\u0443 \u044D\u0442\u043E\u0439 \u0442\u043E\u0447\u043A\u0438 \u0438 \u043E\u043F\u0440\u0435\u0434\u0435\u043B\u0438\u0442\u0435\xA0\xB0\u0421 \u043E\u0442\u0436\u0438\u0433\u0430")), 1)
                ], 2)
              ])
            ])) : createCommentVNode("", true)
          ]),
          createElementVNode("div", _hoisted_19, [
            unref(step) === 1 ? (openBlock(), createElementBlock("img", {
              key: 0,
              src: unref($m)("/images/heat-blank-graph.svg"),
              alt: ""
            }, null, 8, _hoisted_20)) : createCommentVNode("", true),
            unref(step) === 2 ? (openBlock(), createElementBlock("div", _hoisted_21, [
              createElementVNode("div", _hoisted_22, toDisplayString(unref($t)("T,\u2103")), 1),
              createElementVNode("div", _hoisted_23, toDisplayString(unref($t)("Fe")), 1),
              createElementVNode("div", _hoisted_24, toDisplayString(unref($t)("C,%")), 1),
              _hoisted_25,
              (openBlock(), createElementBlock("svg", _hoisted_26, [
                createElementVNode("path", {
                  onClick: _cache[4] || (_cache[4] = ($event) => selectLine("x6")),
                  stroke: selectedLine.value === "x6" ? "#6C3BF0" : "#A2ACB1",
                  d: "M4.78906 68.3018C25.8759 90.3018 67.5847 131.276 109.219 157.302C155.143 186.009 221.692 211.802 244.275 220.802",
                  "stroke-width": "4"
                }, null, 8, _hoisted_27),
                createElementVNode("path", {
                  onClick: _cache[5] || (_cache[5] = ($event) => selectLine("x5")),
                  stroke: selectedLine.value === "x5" ? "#6C3BF0" : "#A2ACB1",
                  d: "M9.30638 60.7987C94.6587 140.299 147.928 175.088 244.775 210.302L481.75 8.80129",
                  "stroke-width": "4"
                }, null, 8, _hoisted_28),
                createElementVNode("path", {
                  onClick: _cache[6] || (_cache[6] = ($event) => selectLine("x4")),
                  stroke: selectedLine.value === "x4" ? "#6C3BF0" : "#A2ACB1",
                  d: "M12.7715 54.3018C45.4938 85.8257 129.302 156.302 234.234 197.802",
                  "stroke-width": "4"
                }, null, 8, _hoisted_29),
                createElementVNode("path", {
                  onClick: _cache[7] || (_cache[7] = ($event) => selectLine("x3")),
                  stroke: selectedLine.value === "x3" ? "#6C3BF0" : "#A2ACB1",
                  d: "M17.3441 45.6121C121.278 131.252 181.702 166.778 241.294 186.746L460.166 9.80185",
                  "stroke-width": "4"
                }, null, 8, _hoisted_30),
                createElementVNode("path", {
                  onClick: _cache[8] || (_cache[8] = ($event) => selectLine("x2")),
                  stroke: selectedLine.value === "x2" ? "#6C3BF0" : "#A2ACB1",
                  d: "M2.27539 86.3018L21.8725 229.802M21.8725 229.802L2.27539 382.302M21.8725 229.802H597.726",
                  "stroke-width": "4"
                }, null, 8, _hoisted_31),
                createElementVNode("path", {
                  onClick: _cache[9] || (_cache[9] = ($event) => selectLine("x1")),
                  stroke: selectedLine.value === "x1" ? "#6C3BF0" : "#A2ACB1",
                  d: "M249 230C296.733 189.971 323.508 167.529 371.24 127.5L511 13",
                  "stroke-width": "4"
                }, null, 8, _hoisted_32),
                createElementVNode("path", {
                  class: "_a3",
                  onClick: _cache[10] || (_cache[10] = ($event) => selectLine("a3")),
                  d: "M5 78C40.5729 125.5 95.6858 180.5 249 229",
                  stroke: selectedLine.value === "a3" ? "#6C3BF0" : "#A2ACB1",
                  "stroke-width": "4"
                }, null, 8, _hoisted_33),
                _hoisted_34,
                unref(block2Stage) === 1 ? (openBlock(), createElementBlock("g", _hoisted_71, _hoisted_81)) : createCommentVNode("", true),
                _hoisted_82,
                _hoisted_83,
                _hoisted_84,
                createVNode(Transition, { name: "fade" }, {
                  default: withCtx(() => [
                    unref(block2Stage) === 3 ? (openBlock(), createElementBlock("g", _hoisted_85, [
                      _hoisted_86,
                      createElementVNode("circle", {
                        onClick: _cache[11] || (_cache[11] = ($event) => selectCircle(2)),
                        style: { "cursor": "pointer" },
                        cx: "120",
                        cy: "167",
                        r: "12",
                        fill: "#F7FEFE",
                        stroke: "#6C3BF0",
                        "stroke-width": "2"
                      }),
                      _hoisted_87,
                      createElementVNode("circle", {
                        onClick: _cache[12] || (_cache[12] = ($event) => selectCircle(1)),
                        style: { "cursor": "pointer" },
                        cx: "34",
                        cy: "115",
                        r: "12",
                        fill: "#F7FEFE",
                        stroke: "#6C3BF0",
                        "stroke-width": "2"
                      }),
                      _hoisted_88,
                      createElementVNode("circle", {
                        onClick: _cache[13] || (_cache[13] = ($event) => selectCircle(3)),
                        style: { "cursor": "pointer" },
                        cx: "206",
                        cy: "218",
                        r: "12",
                        fill: "#F7FEFE",
                        stroke: "#6C3BF0",
                        "stroke-width": "2"
                      })
                    ])) : createCommentVNode("", true)
                  ]),
                  _: 1
                }),
                createVNode(Transition, { name: "fade" }, {
                  default: withCtx(() => [
                    unref(block2Stage) === 4 ? (openBlock(), createElementBlock("g", _hoisted_89, _hoisted_92)) : createCommentVNode("", true)
                  ]),
                  _: 1
                }),
                createVNode(Transition, { name: "fade" }, {
                  default: withCtx(() => [
                    unref(block2Stage) >= 2 ? (openBlock(), createElementBlock("g", _hoisted_93, [
                      _hoisted_94,
                      unref(block2Stage) === 2 ? (openBlock(), createElementBlock("path", _hoisted_95)) : (openBlock(), createElementBlock("path", _hoisted_96)),
                      unref(block2Stage) < 4 ? (openBlock(), createElementBlock("path", _hoisted_97)) : (openBlock(), createElementBlock("path", _hoisted_98)),
                      _hoisted_99,
                      _hoisted_100,
                      _hoisted_101,
                      _hoisted_102,
                      _hoisted_103,
                      _hoisted_104,
                      _hoisted_105,
                      _hoisted_106,
                      _hoisted_107,
                      _hoisted_108,
                      _hoisted_109,
                      _hoisted_110,
                      unref(block2Stage) === 2 ? (openBlock(), createElementBlock("g", _hoisted_111, _hoisted_115)) : (openBlock(), createElementBlock("g", _hoisted_116, _hoisted_120)),
                      _hoisted_121
                    ])) : createCommentVNode("", true)
                  ]),
                  _: 1
                }),
                createVNode(Transition, { name: "fade" }, {
                  default: withCtx(() => [
                    unref(block2Stage) === 4 ? (openBlock(), createElementBlock("g", _hoisted_122, _hoisted_125)) : createCommentVNode("", true)
                  ]),
                  _: 1
                }),
                _hoisted_126
              ])),
              createElementVNode("div", _hoisted_223, [
                createElementVNode("div", {
                  class: normalizeClass({ "--active": graphStep1Answer.value === 1 }),
                  onClick: _cache[14] || (_cache[14] = ($event) => setGraphStep1Answer(1))
                }, "0,4", 2),
                createElementVNode("div", {
                  class: normalizeClass({ "--disabled": graphStep1Answer.value > 0 && graphStep1Answer.value === 1 })
                }, "0,5", 2),
                createElementVNode("div", {
                  class: normalizeClass({ "--disabled": graphStep1Answer.value > 0 && graphStep1Answer.value === 1 })
                }, "1", 2),
                createElementVNode("div", {
                  class: normalizeClass({ "--disabled": graphStep1Answer.value > 0 && graphStep1Answer.value === 1 })
                }, "1,5", 2)
              ])
            ])) : createCommentVNode("", true),
            createElementVNode("div", _hoisted_224, [
              createElementVNode("div", {
                class: "heat-graph-panel__hint__title",
                innerHTML: unref($t)("\u0414\u0438\u0430\u0433\u0440\u0430\u043C\u043C\u0430<br>\u0436\u0435\u043B\u0435\u0437\u043E-\u0443\u0433\u043B\u0435\u0440\u043E\u0434")
              }, null, 8, _hoisted_225),
              createElementVNode("div", _hoisted_226, toDisplayString(unref($t)("\u041E\u0442\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u0438\u0435 \u0441\u0442\u0440\u0443\u043A\u0442\u0443\u0440\u044B \u0441\u043F\u043B\u0430\u0432\u043E\u0432, \u043A\u043E\u0442\u043E\u0440\u043E\u0435 \u043F\u043E\u0437\u0432\u043E\u043B\u044F\u0435\u0442 \u043F\u043E\u043D\u044F\u0442\u044C \u043F\u0440\u043E\u0446\u0435\u0441\u0441\u044B, \u043F\u0440\u043E\u0438\u0441\u0445\u043E\u0434\u044F\u0449\u0438\u0435 \u043F\u0440\u0438\xA0\u0442\u0435\u0440\u043C\u043E\u043E\u0431\u0440\u0430\u0431\u043E\u0442\u043A\u0435 \u0441\u0442\u0430\u043B\u0438.")), 1)
            ]),
            unref(block2Stage) > 3 ? (openBlock(), createElementBlock("div", _hoisted_227, [
              createElementVNode("div", _hoisted_228, toDisplayString(unref($t)("\u041E\u043F\u0440\u0435\u0434\u0435\u043B\u0438\u0442\u0435 \xB0\u0421 \u043E\u0442\u0436\u0438\u0433\u0430 \u0441\u0442\u0430\u043B\u0438 \u043F\u043E \u0434\u0438\u0430\u0433\u0440\u0430\u043C\u043C\u0435 \u0436\u0435\u043B\u0435\u0437\u043E-\u0443\u0433\u043B\u0435\u0440\u043E\u0434")), 1),
              createElementVNode("div", _hoisted_229, toDisplayString(unref($t)("\u041A \u043D\u0430\u0439\u0434\u0435\u043D\u043D\u043E\u0439 \u0442\u0435\u043C\u043F\u0435\u0440\u0430\u0442\u0443\u0440\u0435 \u043F\u0440\u0438\u0431\u0430\u0432\u043B\u044F\u0435\u043C 40 \xB0\u0421")), 1),
              createElementVNode("div", _hoisted_230, [
                createVNode(_sfc_main$5, {
                  label: unref($t)("900 \xB0\u0421"),
                  active: unref(lastBlockAnswer) === 1,
                  wrong: wrongAnswer.value && unref(lastBlockAnswer) === 1,
                  onButtonClick: _cache[15] || (_cache[15] = ($event) => setLastBlockAnswer(1)),
                  class: normalizeClass({ "--disabled": unref(block2Stage) > 4 })
                }, null, 8, ["label", "active", "wrong", "class"]),
                createVNode(_sfc_main$5, {
                  label: unref($t)("820 \xB0\u0421"),
                  active: unref(lastBlockAnswer) === 2,
                  onButtonClick: _cache[16] || (_cache[16] = ($event) => setLastBlockAnswer(2)),
                  class: normalizeClass({ "--disabled": unref(block2Stage) > 4 })
                }, null, 8, ["label", "active", "class"]),
                createVNode(_sfc_main$5, {
                  label: unref($t)("860 \xB0\u0421"),
                  active: unref(lastBlockAnswer) === 3,
                  wrong: wrongAnswer.value && unref(lastBlockAnswer) === 3,
                  onButtonClick: _cache[17] || (_cache[17] = ($event) => setLastBlockAnswer(3)),
                  class: normalizeClass({ "--disabled": unref(block2Stage) > 4 })
                }, null, 8, ["label", "active", "wrong", "class"]),
                createVNode(_sfc_main$5, {
                  label: unref($t)("700 \xB0\u0421"),
                  active: unref(lastBlockAnswer) === 4,
                  wrong: wrongAnswer.value && unref(lastBlockAnswer) === 4,
                  onButtonClick: _cache[18] || (_cache[18] = ($event) => setLastBlockAnswer(4)),
                  class: normalizeClass({ "--disabled": unref(block2Stage) > 4 })
                }, null, 8, ["label", "active", "wrong", "class"])
              ]),
              createElementVNode("div", {
                class: normalizeClass({ "button-simple": true, "--disabled": unref(block2Stage) > 4 }),
                onClick: confirmLastAnswer
              }, toDisplayString(unref($t)("\u0412\u044B\u0431\u0440\u0430\u0442\u044C")), 3)
            ])) : createCommentVNode("", true)
          ]),
          createVNode(Transition, { name: "fade" }, {
            default: withCtx(() => [
              isModalOpen.value ? (openBlock(), createBlock(_sfc_main$b, {
                key: 0,
                width: 880
              }, {
                default: withCtx(() => [
                  createElementVNode("div", _hoisted_231, [
                    createElementVNode("div", _hoisted_232, [
                      createElementVNode("div", _hoisted_233, toDisplayString(unref($t)("\u041F\u043E\u0437\u0434\u0440\u0430\u0432\u043B\u044F\u0435\u043C!")), 1),
                      createElementVNode("div", {
                        class: "dialog-final__content__text main-text-16",
                        innerHTML: unref($t)(`
              <p>
                  \u0412\u0430\u0448\u0430 \u0441\u0442\u0430\u043B\u044C \u043F\u043E\u043B\u0443\u0447\u0438\u043B\u0430\u0441\u044C \u0447\u0442\u043E \u043D\u0430\u0434\u043E. \u041F\u0440\u043E\u0431\u044B \u043C\u0435\u0442\u0430\u043B\u043B\u0430 \u043F\u043E\u043A\u0430\u0437\u0430\u043B\u0438, \u0447\u0442\u043E \u0444\u043B\u043E\u043A\u0435\u043D\u043E\u0432 \u0438 \u0434\u0440\u0443\u0433\u0438\u0445 \u0434\u0435\u0444\u0435\u043A\u0442\u043E\u0432 \u043D\u0435 \u043D\u0430\u0431\u043B\u044E\u0434\u0430\u0435\u0442\u0441\u044F. \u041F\u0440\u043E\u0447\u043D\u043E\u0441\u0442\u044C \u0438 \u0442\u0432\u0451\u0440\u0434\u043E\u0441\u0442\u044C \u2014 \u0438\u0434\u0435\u0430\u043B\u044C\u043D\u044B\u0435, \u0430 \u043A\u0440\u0438\u0432\u0438\u0437\u043D\u0430 \u0438 \u0432\u043E\u0432\u0441\u0435 \u043E\u0442\u0441\u0443\u0442\u0441\u0442\u0432\u0443\u0435\u0442. \u041F\u0440\u044F\u043C\u043E \u043A\u0430\u043A \u043D\u0443\u0436\u043D\u043E \u0437\u0430\u043A\u0430\u0437\u0447\u0438\u043A\u0443!
                </p>
                <p>
                  \u041D\u043E \u0441\u0430\u043C\u043E\u0435 \u0433\u043B\u0430\u0432\u043D\u043E\u0435, \u0447\u0442\u043E \u0438\u043C\u0435\u043D\u043D\u043E \u0431\u043B\u0430\u0433\u043E\u0434\u0430\u0440\u044F \u0432\u0430\u0448\u0435\u0439 \u0440\u0430\u0431\u043E\u0442\u0435 \u0432\u044B, \u0432\u0430\u0448\u0430 \u0441\u0435\u043C\u044C\u044F \u0438 \u043B\u044E\u0434\u0438 \u043F\u043E \u0432\u0441\u0435\u0439 \u0441\u0442\u0440\u0430\u043D\u0435 \u043C\u043E\u0433\u0443\u0442 \u0436\u0438\u0442\u044C \u0441 \u043A\u043E\u043C\u0444\u043E\u0440\u0442\u043E\u043C: \u0435\u0437\u0434\u0438\u0442\u044C \u043D\u0430 \u0430\u0432\u0442\u043E\u043C\u043E\u0431\u0438\u043B\u044F\u0445 \u0438 \u0442\u0440\u0430\u043C\u0432\u0430\u044F\u0445, \u043F\u0440\u043E\u0441\u044B\u043F\u0430\u0442\u044C\u0441\u044F \u0432\xA0\u0443\u0441\u0442\u043E\u0439\u0447\u0438\u0432\u044B\u0445 \u043C\u043D\u043E\u0433\u043E\u044D\u0442\u0430\u0436\u043A\u0430\u0445, \u043B\u0435\u0442\u0430\u0442\u044C \u043D\u0430 \u0441\u0430\u043C\u043E\u043B\u0435\u0442\u0430\u0445 \u0438 \u043C\u043D\u043E\u0433\u043E\u0435 \u0434\u0440\u0443\u0433\u043E\u0435. \u0412\u0435\u0434\u044C \u043A\u043E\u043D\u0441\u0442\u0440\u0443\u043A\u0446\u0438\u0438 \u0438 \u043C\u0435\u0445\u0430\u043D\u0438\u0437\u043C\u044B \u0438\u0437 \u043C\u0435\u0442\u0430\u043B\u043B\u0430 \u043E\u043A\u0440\u0443\u0436\u0430\u044E\u0442 \u043D\u0430\u0441 \u043F\u043E\u0432\u0441\u044E\u0434\u0443.
                </p>
              `)
                      }, null, 8, _hoisted_234),
                      createElementVNode("span", _hoisted_235, toDisplayString(unref($t)("\u0414\u0430\u043B\u0435\u0435")), 1)
                    ]),
                    createElementVNode("div", {
                      class: "dialog-final__art",
                      style: normalizeStyle(`background: url(${unref($m)(
                        "/images/final-modal.png"
                      )}) center/cover no-repeat;`)
                    }, null, 4)
                  ])
                ]),
                _: 1
              })) : createCommentVNode("", true)
            ]),
            _: 1
          })
        ]),
        unref(block2Stage) > 4 ? (openBlock(), createBlock(_sfc_main$8, {
          key: 0,
          title: unref($t)("\u041E\u0442\u043B\u0438\u0447\u043D\u043E, \u0432\u044B \u0441\u043F\u0440\u0430\u0432\u0438\u043B\u0438\u0441\u044C \u0441 \u0437\u0430\u0434\u0430\u043D\u0438\u0435\u043C!"),
          text: unref($t)("\u042D\u0442\u043E \u0431\u044B\u043B\u043E \u043D\u0435\u043F\u0440\u043E\u0441\u0442\u043E, \u0437\u0430\u0442\u043E \u0442\u0435\u043F\u0435\u0440\u044C \u0432\u0430\u0448 \u043C\u0435\u0442\u0430\u043B\u043B \u0438\u0434\u0435\u0430\u043B\u0435\u043D. \u0422\u0430\u043A\u043E\u043C\u0443 \u043F\u043E\u0437\u0430\u0432\u0438\u0434\u0443\u0435\u0442 \u0441\u0430\u043C \u0413\u0440\u043E\u043C\u043E\u0437\u0435\u043A\u0430!"),
          label: unref($t)("\u0414\u0430\u043B\u0435\u0435"),
          onLabelClick: openModal
        }, null, 8, ["title", "text", "label"])) : createCommentVNode("", true)
      ]);
    };
  }
};
const _sfc_main = {
  __name: "Body",
  setup(__props) {
    inject("translate");
    const $l = inject("log");
    inject("media");
    let currentScreen = ref(0);
    let metalurgBlock = parseInt(window.localStorage.getItem("metalurg_block"));
    if (metalurgBlock) {
      currentScreen = ref(metalurgBlock);
    }
    const screens = ref([
      markRaw(_sfc_main$a),
      markRaw(_sfc_main$6),
      markRaw(_sfc_main$3),
      markRaw(_sfc_main$2),
      markRaw(_sfc_main$1)
    ]);
    onMounted(() => {
      $l("SYSTEM", "session_created");
    });
    return (_ctx, _cache) => {
      return openBlock(), createBlock(resolveDynamicComponent(screens.value[unref(currentScreen)]), {
        id: "metalurg",
        onNext: _cache[0] || (_cache[0] = ($event) => isRef(currentScreen) ? currentScreen.value++ : currentScreen++)
      }, null, 32);
    };
  }
};
export { _sfc_main as default };
