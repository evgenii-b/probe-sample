import { createApp } from 'vue'
import App from './App.vue'
import Probe from './lib/install'
import './styles/tailwind.scss'
import './styles/main.scss'

createApp(App).use(Probe).mount('#app')
